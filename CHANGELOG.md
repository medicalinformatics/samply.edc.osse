# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## 1.4.3 - 2017-10-26
### Added
- Every input element now has an attribute called ```data-mdrid```, containing the data element's mdr id.

### Changed
- Caching of database queries used to speed up user and roles lists in admin section.
- List of user roles in frontend now sorted alphabetically.

### Fixed
- Sorting of patients by name or pseudonym was not always working correctly.
- Some minor translation issues.

## 1.4.2 - 2017-09-04
### Fixed
- One rather prominent string always showed in english.

## 1.4.1 - 2017-08-29
### Added
- Forms show title and description texts as well as rich text items in the language set in EDC, given that they are available in that language. (requires Form Editor 1.5.0 or higher)
- Patients' pseudonyms can now be copied to the clipboard easily.

### Changed
- Loading patients and locations now incorporates caching methods to speed things up.
- Patients' pseudonyms now displayed in a different font which makes it easier to distinguish some characters.
- Changed design of language select box.
- jQuery imported by mainzelliste-client is ignored from now on.
- Changed several dependencies to the latest component versions (edc, store-osse, mdrclient).
- Flag icons replaced by [flag-icon-css](http://flag-icon-css.lip.is/).

### Fixed
- "Password Complexity" setting appeared twice in admin view.
- Some minor i18n-related string corrections.
- Timeout when connecting to samply.auth prevents possible freeze on startup.

## 1.4.0 - 2017-06-01
### Added
- Additional config parameters editable for admin.
- OSSE can now act as a FAIR data point.
- New select box to change ui language (currently available: English and German).
- The user's current language setting gets saved.
- Added ability to import entire last episode.
- Use gzip compression in data transfer for some uncompressed formats.

### Changed
- Filters of patient list repositioned.
- Config panels rearranged to keep things clear in admin view.
- Records' titles are now fetched dynamically from the MDR.
- Forms now show title and description in header.
- Layout works better on higher resolutions now.
- Version number in login screen now shows build version.

### Fixed
- Fixed a bug occurring when adding a new patient.
- Users cannot be added without a role anymore.
- Special characters in utf-8 now display correctly.

## 1.3.3 - 2016-12-15
### Fixed
- Disabled failure on deserialisation on unknown or unignorable properties in Jersey (needed for new AUTH version).
- Some CSS fixes.

## 1.3.2 - 2016-04-21
### Added
- Patient list can now be filtered.
- Edit a new patient's medical data directly after creating the patient.
- Added the possibility to create another patient right after creating a new one.

## 1.3.1 - 2016-03-17
###Fixed
- Proxy usage for mdrfaces fixed.

## 1.3.0 - 2015-12-21
### Added
- New mainzelliste.noIdat setting in osseconfig. Will automatically be set to false, if it doesn't exist yet or isn't a boolean.
- New postgres functions for samply.store.

### Changed
- MDR rest interface updated to version v3.
- Incorporated changes in form-data-storage (all forms will be recreated).
- Changes the timestamps in all episodes to the timestamp version of the name.

## 1.2.0 - 2015-11-06
### Added
- New RESTLocalhostToMainzelliste configuration value for local HTTP connection to Mainzelliste.

### Fixed
- TeilerURL and port settings fixed.