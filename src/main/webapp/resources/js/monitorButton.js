function monitorButton(data) {
  var loading = document.getElementById("saveButtonWorking");
  var saveButton = document.getElementById("save");
  var resetButton = document.getElementById("reset");
  if(data.status == "begin"){
    loading.style.display = "inline";
    saveButton.style.display = "none";
    if(resetButton != null)
    	resetButton.style.display = "none";
  }
  else if(data.status == "success"){
    loading.style.display = "none";
    saveButton.style.display = "inline";
    if(resetButton != null)
    	resetButton.style.display = "inline";
  }
}