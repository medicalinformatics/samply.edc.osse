/**
 * Activates the drag functionality in .sortableItemPanel
 * and .draggable
 */
function activateDrag() {

	$(".sortableItemPanelEpisode").sortable({
		start: function(e, ui ){
            ui.placeholder.height(ui.helper.outerHeight());
        },
		stop : function(event, ui) {
			refreshItems();
			refreshItemsEpisode();
			$("#refreshItems").click();
			$("#refreshItemsEpisode").click();
		}
	});
	
	$(".sortableItemPanel").sortable({
		start: function(e, ui ){
            ui.placeholder.height(ui.helper.outerHeight());
        },
		stop : function(event, ui) {
			refreshItems();
			refreshItemsEpisode();
			$("#refreshItems").click();
			$("#refreshItemsEpisode").click();
		}
	});
	
	$(".draggable").draggable({
		connectToSortable : ".sortableItemPanel,.sortableItemPanelEpisode",
		revert : "invalid",
		helper: "clone",
		stop : function(event, ui) {
			patformColors();
		}
	});

	$(".dragableInternal").draggable({
		connectToSortable : ".sortableItemPanel,.sortableItemPanelEpisode",
		revert : "invalid",
	});
	
	patformColors();
}

/**
 * Refreshes the items, using the .formItems
 */

function refreshItems() {
	var data = "";

	if ($(".sortableItemPanel").find(".formItem").length > 0) {
		var newElements = new Array();
		var newElementsTemp = new Array();
		
		$(".sortableItemPanel").find(".formItem").each(function(i, el) {
			var urn = $(el).find(".urn").html();
			var formname = $(el).find(".designation").html();
			var split = urn.split("-");
			
			if(!isInEpisodeForms(split[0])) {
				if(newElementsTemp.indexOf(split[0]) == -1) {
					newElements.push(urn);
					newElementsTemp.push(split[0]);
				} else {
					$.growl({
						title: '<strong>Error:</strong><br /> ',
						message: 'The form '+formname+' (in any version) is already used in your basic data forms.'
					},{
						type: 'info',
						animate: {
							enter: 'animated pulse',
							exit: 'animated zoomOut'
						}
					});
				}
			}
			else {
				$.growl({
					title: '<strong>Error:</strong><br /> ',
					message: 'The form '+formname+' (in any version) is already used in your longitudinal data forms.'
				},{
					type: 'info',
					animate: {
						enter: 'animated pulse',
						exit: 'animated zoomOut'
					}
				});
			}
		});

		$("#items").val(newElements.join(","));
		$("#itemsTemp").val(newElementsTemp.join(","));

		$("#dragInfo").hide();
	} else {
		$("#items").val("");
		$("#itemsTemp").val("");
		$("#dragInfo").show();
	}
	
	patformColors();
}

function isInCaseForms(searchID) {
	var all = $("#itemsTemp").val().split(",");
	if(all.indexOf(searchID) == -1)
		return false;
	else
		return true;
}

function isInEpisodeForms(searchID) {
	var all = $("#itemsEpisodeTemp").val().split(",");
	if(all.indexOf(searchID) == -1)
		return false;
	else
		return true;
}

function refreshItemsEpisode() {
	var data = "";

	if ($(".sortableItemPanelEpisode").find(".formItem").length > 0) {
		var newElements = new Array();
		var newElementsTemp = new Array();
		
		$(".sortableItemPanelEpisode").find(".formItem").each(function(i, el) {
			var urn = $(el).find(".urn").html();
			var formname = $(el).find(".designation").html();
			var split = urn.split("-");
			
			if(!isInCaseForms(split[0])) {
				if(newElementsTemp.indexOf(split[0]) == -1) {
					newElements.push(urn);
					newElementsTemp.push(split[0]);
				}
				else
					{
						$.growl({
							title: '<strong>Error:</strong><br /> ',
							message: 'The form '+formname+' (in any version) is already used in your longitudinal data forms.'
						},{
							type: 'info',
							animate: {
								enter: 'animated pulse',
								exit: 'animated zoomOut'
							}
						});
					}
			}
			else {
				$.growl({
					title: '<strong>Error:</strong><br /> ',
					message: 'The form '+formname+' (in any version) is already used in your basic data forms.'
				},{
					type: 'info',
					animate: {
						enter: 'animated pulse',
						exit: 'animated zoomOut'
					}
				});
			}
		});

		$("#itemsEpisode").val(newElements.join(","));
		$("#itemsEpisodeTemp").val(newElementsTemp.join(","));
		
		$("#dragInfoEpisode").hide();
	} else {
		$("#itemsEpisode").val("");
		$("#itemsEpisodeTemp").val("");
		$("#dragInfoEpisode").show();
	}
	
	patformColors();
}

/**
 * Called when a "remove" button has been clicken
 * @param element
 */
function removeItem(element) {
	//var urn = $(element).parent().parent().parent().remove();
	var urn = $(element).parent().parent().remove();
	refreshItems();
	$("#refreshItems").click();
}

function removeItemEpisode(element) {
	var urn = $(element).parent().parent().remove();
	refreshItemsEpisode();
	$("#refreshItemsEpisode").click();
}


function showResultTab() {
	$("#searchTabs a[href='#searchResultTab']").tab("show");
}

function toggleItem(element) {
	var urn = $(element).parent().parent().find(".urn").html();
	$(".inputStarItemUrn").val(urn);
	$(".inputStarItemBtn").click();
	
	
	var others = $(".urn").filter(function() { return $(this).html() == urn; });
	var i = $(element).find("i");
	
	if(i.hasClass("text-muted")) {
		others.each(function(index) {
			$(this).parent().parent().parent().find(".starItem").switchClass("text-muted", "text-primary");
		});
	} else {
		others.each(function(index) {
			$(this).parent().parent().parent().find(".starItem").switchClass("text-primary", "text-muted");
		});
	}
}

function collapseItem(element) {
	var divp = $(element).parent().parent().parent();
	var urn = divp.find(".urn").html();
	var d = divp.find("#details").first();
	var draggable = divp.find(".formItem").first().hasClass("ui-draggable");
	
	if(! d.hasClass("in")) {
		d.load("shortDetail.xhtml?draggable=" + draggable + "&urn=" + urn + " #detailView",
				function() {
			$(this).collapse('toggle');
			$(element).find("i").switchClass("fa-angle-down", "fa-angle-up");
			activateDrag();
		});
	} else {
		d.collapse('toggle');
		$(element).find("i").switchClass("fa-angle-up", "fa-angle-down");
	}
}

// Role Edit, hide/display permissions on rolename entry
function displayPermissions() {
	var feld = document.getElementById("rolename");
	var hiddenDiv = document.getElementById("predefinedPermissions");

    hiddenDiv.style.display = (feld.value == "") ? "none":"block";
}

function patformColors() {
    $("input[id*='patFormToggle']").each(function (i, el) {
	    if(el.checked)
	        el.parentElement.parentElement.parentElement.style.backgroundColor = "palegreen";
	    else
	        el.parentElement.parentElement.parentElement.style.background = "rgba(234, 239, 245, 0.71) none repeat scroll 0 0";

    });
}

function updateColor(val) {
    if(val.checked)
        val.parentElement.parentElement.parentElement.style.backgroundColor = "palegreen";
    else
        val.parentElement.parentElement.parentElement.style.background = "rgba(234, 239, 245, 0.71) none repeat scroll 0 0";
}



$(document).ready(function(){
    patformColors();
});
