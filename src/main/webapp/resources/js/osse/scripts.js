$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });

  $('#list_patients').click(function(e) {
    e.preventDefault();
    $('#patient').hide();
  });

  $('#patients tr').click(function(e){
    e.preventDefault();
    $('#patient').show();
  })

  $('#episode-slider').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    centerMode: true
  });
});
