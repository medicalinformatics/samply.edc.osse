$(document).ready(function() {
		$(".tooltipInput").tooltip({ placement: 'right', trigger: 'focus'});

		applyChangeHandlersEditMode();
		applyChangeHandlersSave();
});

function applyChangeHandlersEditMode() {
	$('.implicitEditMode .iseditable').one('input keyup change paste', ":input, select", function() {
		$("#editmode").click();
	});
}

function applyChangeHandlersSave() {
	$('#leftFlagList').on('click', '#editFlagSaveButton', function() {
		$("#save").click();
	});
	$('#leftFlagList').on('click', '#editFlagCancelButton', function() {
		$("#cancel").click();
	});
	$('#leftFlagList').on('click', '#editFlagLeaveEditModeButton', function() {
		$("#editmodefinish").click();
	});
	$('#leftFlagList').on('click', '#editFlagEditModeButton', function() {
		$("#editmode").click();
	});
	

	$('#leftFlagList').on('mouseenter', '#editFlagCancelButton, #editFlagSaveButton, #editFlagLeaveEditModeButton', function() {
		$(this).focus();
	});
}
	
function applyEditMode() {
	$(".iseditable").addClass("editMode");
	$(".ui-cell-editor-output").removeClass("ui-cell-editor-output").addClass("ui-cell-editor-input");
}

function removeEditMode() {
	$(".iseditable").removeClass("editMode");	
	$(".ui-cell-editor-input").removeClass("ui-cell-editor-input").addClass("ui-cell-editor-output");
}

function setConfirmUnload(on) {
	if (on) {
		$('.navbar-fixed-top').find('.btn').addClass('disabled');
	} else {
		$('.navbar-fixed-top').find('.btn').removeClass('disabled');
	}
	
    var message = "You have unsaved data. Are you sure to leave the page?";
    window.onbeforeunload = (on) ? function() { return message; } : false;
}