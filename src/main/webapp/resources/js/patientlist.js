/**
 * Sorting plug-in for DataTables "yesnoidat" 
 * sorts fields that have no 'data-subject="vorname' defined to the end of the table
 * 
 * @name: patientlist no-idat sorter
 * @summary: sorts fields that have no 'data-subject="vorname' defined to the end of the table
 * @example
 *   $('#mytable').dataTable({
 *      columnDefs: [
 *        { targets: 0, type: 'yesnoidat' }
 *      ]
 *   });
 */

jQuery.extend(jQuery.fn.dataTableExt.oSort, {
    "yesnoidat-asc": function (str1, str2) {
        var tempElement1 = document.createElement("div");
        tempElement1.innerHTML = str1;
        var tempElement2 = document.createElement("div");
        tempElement2.innerHTML = str2;
        var newStr1 = (tempElement1.textContent || tempElement1.innerText).trim();
        var newStr2 = (tempElement2.textContent || tempElement2.innerText).trim();

        if (newStr1.startsWith("(")) {
            newStr1 = "ZZZZZZZZZZ".concat(newStr1);
        }
        if (newStr2.startsWith("(")) {
            newStr2 = "ZZZZZZZZZZ".concat(newStr2);
        }

        return newStr1.localeCompare(newStr2);
    },
    "yesnoidat-desc": function (str1, str2) {
        var tempElement1 = document.createElement("div");
        tempElement1.innerHTML = str1;
        var tempElement2 = document.createElement("div");
        tempElement2.innerHTML = str2;
        var newStr1 = (tempElement1.textContent || tempElement1.innerText).trim();
        var newStr2 = (tempElement2.textContent || tempElement2.innerText).trim();

        if (newStr1.startsWith("(")) {
            newStr1 = "0000000000".concat(newStr1);
        }
        if (newStr2.startsWith("(")) {
            newStr2 = "0000000000".concat(newStr2);
        }

        return newStr2.localeCompare(newStr1);
    }
});




//clipboard function

$(document).ready(function()  {

    function getCookie(name) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length == 2) return parts.pop().split(";").shift();
    }

    var str;

    if (getCookie("lang") == "en") str = "Copied !";
    if (getCookie("lang") =="de") str = "Kopiert !";

    var clipboard = new Clipboard('.clipboard', {
        text: function(trigger) {
            return $(trigger).parents("tr").find("[data-subject='pid']").text();
        }
    });

    // $('.clipboard').tooltip({
    //     trigger: 'click',
    //     placement: 'bottom',
		// delay:{show:4000, hide:4000},
    // });


    function setTooltip(btn, message) {

        $(btn).tooltip('hide')
            .attr('data-original-title', message)
            .tooltip('show');
    }

    function hideTooltip(btn) {
        setTimeout(function() {
            $(btn).tooltip('hide');
        }, 1000);
    }

    clipboard.on('success', function(e) {

        $($(e.trigger).parents("tr").find("[data-subject='pid']")).tooltip({
            trigger: 'click',
            placement: 'bottom',
            // delay:{show:4000, hide:4000},
        });

        setTooltip($(e.trigger).parents("tr").find("[data-subject='pid']"), str);
        hideTooltip($(e.trigger).parents("tr").find("[data-subject='pid']"));
    });



})