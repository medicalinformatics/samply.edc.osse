/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.osse.control;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import de.pseudonymisierung.mainzelliste.client.AddPatientToken;
import de.pseudonymisierung.mainzelliste.client.EditPatientToken;
import de.pseudonymisierung.mainzelliste.client.ID;
import de.pseudonymisierung.mainzelliste.client.InvalidSessionException;
import de.pseudonymisierung.mainzelliste.client.MainzellisteNetworkException;
import de.pseudonymisierung.mainzelliste.client.Session;
import de.samply.edc.control.AbstractViewBean;
import de.samply.edc.model.Entity;
import de.samply.edc.osse.model.Case;
import de.samply.edc.osse.model.Form;
import de.samply.edc.osse.model.Location;
import de.samply.edc.osse.model.Patient;
import de.samply.edc.osse.model.TempTokenExchangeStorage;
import de.samply.edc.osse.utils.OSSEUtils;
import de.samply.edc.utils.Utils;
import de.samply.store.Resource;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.store.query.Criteria;
import de.samply.store.query.ResourceQuery;

/**
 * View scoped bean for patient data management.
 */
@ManagedBean
@ViewScoped
public class PatientBean extends AbstractViewBean {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * The patient token ID
     */
    private String patientTokenID = null;

    /** The list of patients. */
    private ArrayList<HashMap<String, Object>> patients;

    /** The locations select items. */
    private List<SelectItem> locationsSelectItems;

    /**  The amount of deleted cases. */
    private Integer amountDeletedCases;

    /**  Formvar if or not to show deleted patients/cases. */
    private String showDeleted;

    /** Var by with patientlist shall be filtered */
    private String filterPatientsBy = null;

    /**
     * Post-construct init.
     *
     * @see de.samply.edc.control.AbstractViewBean#init()
     */
    @Override
    public void init() {
        patients = null;
        amountDeletedCases = 0;
        filterPatientsBy = null;
        super.init();
    }

    /**
     * Load patients.
     */
    private void loadPatients() {
        loadPatients(false);
    }

    /**
     * Loads the patients.
     *
     * @param deleted
     *            If or not to load deleted patients or current patients
     */
    private void loadPatients(Boolean deleted) {
        // check if the GET param is set
        if ("1".equalsIgnoreCase(showDeleted))
            deleted = true;

        sessionBean.getDatabase().startCaching();

        amountDeletedCases = 0;
        patients = new ArrayList<HashMap<String, Object>>();

        ResourceQuery queryPatients = new ResourceQuery(OSSEVocabulary.Type.Patient);
        List<Resource> resultPatients = new ArrayList<>();

        if (((SessionBean) Utils.getSB()).showQueryResult()) {
            try {
                resultPatients = ((Database) getSessionBean().getDatabase())
                        .getQueryResult(((SessionBean) getSessionBean()).getQueryResultId());
            } catch (DatabaseException e) {
                Utils.getLogger().debug("ID = " + ((SessionBean) getSessionBean()).getQueryResultId());
                // TODO Auto-generated catch block
                Utils.addContextMessage("Could not load Patients",
                        "The provided query result did not contain any patients.");
                e.printStackTrace();
            }
        } else {
//            queryPatients.addFetch(OSSEVocabulary.Type.Case);
//            queryPatients.addFetch(OSSEVocabulary.Type.Location);
            resultPatients = getSessionBean().getDatabase().getResources(queryPatients);
        }

        ////////fetch list of possible locations///////////////
        ResourceQuery queryLocations = new ResourceQuery(OSSEVocabulary.Type.Location);
        queryLocations.setFetchAdjacentResources(false);
        List<Resource> resultLocations = getSessionBean().getDatabase().getResources(queryLocations);


        for (Resource patientResource : resultPatients) {
            Patient patient = new Patient(getSessionBean().getDatabase(), patientResource);

            // get the location of the current user (= my location)
            Location myLocation = null;
            String myLocationName = "";
            if (((SessionBean) getSessionBean()).getCurrentRole() != null) {
                myLocation = ((SessionBean) getSessionBean()).getCurrentRole().getLocation();
                myLocationName = myLocation.getName();
            }

            //Loading location for each case of each patient is a very costly. We want to do case loop only when a
            //patient hast more then one case, because then it's possible that this patient has more then one location.
            if (((Resource) patientResource).getProperties("cases").size() > 1) {
                patient.load();

                // fetch all cases
                patient.loadChildren(OSSEVocabulary.Type.Case, true);

                if (patient.getPatientID() == null)
                    continue;



                for (Entity aCase : patient.getCases()) {
                    if (!deleted && aCase.isDeleted()) {
                        // count the deleted cases of my location
                        if (((Case) aCase).getLocation().getName().equals(myLocationName))
                            amountDeletedCases++;
                        continue;
                    }

                    // only display deleted?
                    if (deleted && !aCase.isDeleted())
                        continue;

                    // only display deleted cases of my location
                    if (deleted && !((Case) aCase).getLocation().getName().equals(myLocationName))
                        continue;

                    if (deleted && ((Case) aCase).getLocation().getName().equals(myLocationName))
                        amountDeletedCases++;

                    if (filterPatientsBy != null) {
                        // we want a filtered patient list
                        Boolean thisPatientHasFilteredForm = false;

                        // 1) check forms if any is in status X
                        for (Entity aForm : ((Case) aCase).getForms()) {
                            if (filterPatientsBy.equalsIgnoreCase(((Form) aForm).getFormState())) {
                                thisPatientHasFilteredForm = true;
                            }
                        }

                        if (!thisPatientHasFilteredForm) {
                            // 2) check episodes for such forms
                            for (Entity anEpisode : ((Case) aCase).getEpisodes()) {
                                anEpisode.load();
                                for (Entity aForm : ((Case) aCase).getForms()) {
                                    if (filterPatientsBy.equalsIgnoreCase(((Form) aForm).getFormState())) {
                                        thisPatientHasFilteredForm = true;
                                    }
                                }
                            }
                        }

                        // this patient has no forms in that state, cut it away
                        if (!thisPatientHasFilteredForm)
                            continue;
                    }

                    dataObject = new HashMap<String, Object>();
                    dataObject.put("pseudonym", patient.getPseudonym());
                    dataObject.put("patientID", patient.getPatientID());

//                System.out.println("Loading patient "+patient.getResourceURI()+" .. "+patient.getPseudonym()+" .. "+((Case) aCase).getResourceURI());
//
//                for(String moo:aCase.getProperties().keySet()) {
//                    System.out.println("CASE PROP "+moo+" VALUE "+aCase.getProperty(moo));
//                }

                    dataObject.put("location", ((Case) aCase).getLocation().getName());
                    dataObject.put("isDeleted", aCase.isDeleted());
                    dataObject.put("isImported", patient.isImported());

                    if (((SessionBean) getSessionBean()).getMayAlwaysSeeIdat()) {
                        dataObject.put("showIdat", true);
                    } else {
                        if (((Case) aCase).getLocation().getName().equals(myLocationName)
                                && ((SessionBean) getSessionBean()).getMaySeeIdat()) {
                            dataObject.put("showIdat", true);
                        }
                    }

                    // User may only edit a patient, if the case's and the patient's
                    // location
                    // is equal to his role's location
                    if (!((Case) aCase).getLocation().getName().equals(myLocationName)) {
                        dataObject.put("mayWrite", false);
                    } else {
                        if (((SessionBean) getSessionBean()).mayWrite(patient.getLocation().getName(),
                                OSSEVocabulary.Type.Patient)) {
                            // may only (un)delete the case if its of my location
                            if (!deleted)
                                dataObject.put("mayDeleteCase", true);
                            else
                                dataObject.put("mayUnDeleteCase", true);
                        }
                        dataObject.put("mayWrite", ((SessionBean) getSessionBean())
                                .mayWrite(patient.getLocation().getName(), OSSEVocabulary.Type.Patient));
                    }
                    dataObject.put("patientResource", patientResource);
                    dataObject.put("caseResource", aCase.getResource());
                    patients.add(dataObject);
                }




            }

            //else: if patient only has one case
            else {

                patient.loadQuick();

                String patientID = ((Resource) patientResource).getProperty("patientID").getValue();
                String patientLocationsQuery = ((Resource) patientResource).getProperty("locations").getValue();

                String patientLocation = "";

                //get actual location string from resultLocations  based on the location query (e.g: "location:1" -> "Munich")
                for (int x= 0; x < resultLocations.size(); x++) {
                    if (resultLocations.get(x).getValue().equals(patientLocationsQuery)) {
                        patientLocation = resultLocations.get(x).getProperty("name").getValue();
                        break;
                    }
                }

//                Boolean caseLocationIsMyLocation  = ((Case) aCase).getLocation().getName().equals(myLocationName);
//                Boolean caseLocationIsMyLocation = patient.getLocation().getName().equals(myLocationName);
                Boolean caseLocationIsMyLocation = patientLocation.equals(myLocationName);



                // fetch all cases of the patient
                patient.loadChildren(OSSEVocabulary.Type.Case, true);

                if (patientID == null)
                    continue;

                for (Entity aCase : patient.getCases()) {


                    if (!deleted && aCase.isDeleted()) {
                        // count the deleted cases of my location
                        if (caseLocationIsMyLocation)
                            amountDeletedCases++;
                        continue;
                    }

                    // only display deleted?
                    if (deleted && !aCase.isDeleted())
                        continue;

                    // only display deleted cases of my location
                    if (deleted && !caseLocationIsMyLocation)
                        continue;

                    if (deleted && caseLocationIsMyLocation)
                        amountDeletedCases++;

                    if (filterPatientsBy != null) {
                        // we want a filtered patient list
                        Boolean thisPatientHasFilteredForm = false;

                        // 1) check forms if any is in status X
                        for (Entity aForm : ((Case) aCase).getForms()) {
                            if (filterPatientsBy.equalsIgnoreCase(((Form) aForm).getFormState())) {
                                thisPatientHasFilteredForm = true;
                            }
                        }

                        if (!thisPatientHasFilteredForm) {
                            // 2) check episodes for such forms
                            for (Entity anEpisode : ((Case) aCase).getEpisodes()) {
                                anEpisode.load();
                                for (Entity aForm : ((Case) aCase).getForms()) {
                                    if (filterPatientsBy.equalsIgnoreCase(((Form) aForm).getFormState())) {
                                        thisPatientHasFilteredForm = true;
                                    }
                                }
                            }
                        }

                        // this patient has no forms in that state, cut it away
                        if (!thisPatientHasFilteredForm)
                            continue;
                    }




                    dataObject = new HashMap<String, Object>();
                    dataObject.put("pseudonym", patient.getPseudonym());
                    dataObject.put("patientID", patient.getPatientID());
                    dataObject.put("location", patientLocation);
                    dataObject.put("isDeleted", aCase.isDeleted());
                    dataObject.put("isImported", patient.isImported());

                    if (((SessionBean) getSessionBean()).getMayAlwaysSeeIdat()) {
                        dataObject.put("showIdat", true);
                    } else {
                        if ( caseLocationIsMyLocation
                                && ((SessionBean) getSessionBean()).getMaySeeIdat()) {
                            dataObject.put("showIdat", true);
                        }
                    }

                    // User may only edit a patient, if the case's and the patient's
                    // location
                    // is equal to his role's location
                    if (! (caseLocationIsMyLocation)) {
                        dataObject.put("mayWrite", false);
                    } else {
                        if (((SessionBean) getSessionBean()).mayWrite(patientLocation,
                                OSSEVocabulary.Type.Patient)) {
                            // may only (un)delete the case if its of my location
                            if (!deleted)
                                dataObject.put("mayDeleteCase", true);
                            else
                                dataObject.put("mayUnDeleteCase", true);
                        }
                        dataObject.put("mayWrite", ((SessionBean) getSessionBean())
                                .mayWrite(patientLocation, OSSEVocabulary.Type.Patient));
                    }
                    dataObject.put("patientResource", patientResource);
                    dataObject.put("caseResource", aCase.getResource());
                    patients.add(dataObject);
                }




            }

        }
        dataObject = null;

        sessionBean.getDatabase().stopCaching();
    }

    /**
     * Method to save a created patient and directly go back to Mainzelliste to
     * add another one
     * 
     * @return for JSF navigation
     * @throws MainzellisteNetworkException
     * @throws InvalidSessionException
     * @throws IOException
     */
    public String saveAndMore() throws MainzellisteNetworkException, InvalidSessionException, IOException {
        save(false);
        goAddPatient();
        return null;
    }

    /**
     * @return the string
     * @see de.samply.edc.control.AbstractViewBean#save()
     */
    @Override
    public String save() {
        return save(true);
    }

    /**
     * Saves patient data. Called by create and edit patient.
     * 
     * @param goBackToPatientlist
     *            If or not to jump to patientlist after done
     * @return for JSF navigation
     * @see #save()
     */
    public String save(Boolean goBackToPatientlist) {
        String locationName = (String) dataObject.get("location");

        Patient patient = new Patient(getSessionBean().getDatabase());

        // if patientResource is set, then we are currently editing an existing
        // patient
        Resource patientResource = (Resource) dataObject.get("patientResource");
        if (patientResource != null) {
            patient.setResource(patientResource);
        }

        // get the location
        Location patientLocation = new Location(getSessionBean().getDatabase());
        Resource patientLocationResource = patientLocation.entityExistsByProperty(OSSEVocabulary.Location.Name,
                locationName);
        if (patientLocationResource == null) {
            getSessionBean().getDatabase().rollback();
            throw new Error("The provided location was not found.");
        }
        patientLocation.setResource(patientLocationResource);

        patient.setProperty(OSSEVocabulary.Patient.Locations, patientLocationResource);

        String patientID = null;

        getSessionBean().getDatabase().startCaching();

        /**
         * Cache the locations
         */
        ResourceQuery locationQuery = new ResourceQuery(OSSEVocabulary.Type.Location);
        locationQuery.setFetchAdjacentResources(false);
        getSessionBean().getDatabase().getResources(locationQuery);

        getSessionBean().getDatabase().beginTransaction();

        try {
            if (patientResource == null) {
                // We are creating a new patient

                patientID = getCreatedPatientID();
                patient.setProperty("patientID", patientID);

                // Check if a patient of that ID already exists in the selected
                // location
                if (patient.existsInLocation(patientID, locationName)) {
                    // the patient exists in the selected location, rollback and
                    // refuse creation
                    getSessionBean().getDatabase().rollback();
                    Utils.addContextMessage(Utils.getResourceBundleString("summary_adduserfailed"),
                            Utils.getResourceBundleString("newpatientfailure"));

                    goPatientList();
                    return null;
                }

                // Now get the patient, if he exists anywhere else
                Resource thisPatient = patient.entityExistsByProperty("patientID", patientID);

                if (thisPatient != null) {
                    // we found a patient with a case in another location, so we use
                    // this one here
                    patient.setResource(thisPatient);
                } else {
                    // no such patient yet, so we create one now
                    patient.saveOrUpdate();
                }

                // and we add the case for this location
                Case myCase = new Case(getSessionBean().getDatabase());
                myCase.setParent(patient);
                myCase.setLocation(patientLocation);
                myCase.saveOrUpdate();

                getSessionBean().getDatabase().commit();
                ((SessionBean) Utils.getSB()).clearCurrentObject("tokenId");

                if (goBackToPatientlist) {
                    Utils.addContextMessage("Success", "Your patient has been added.");
                    // and switch into that patient
                    ((SessionBean) sessionBean).switchPatient(patient, myCase.getResource());
                    return null;
                }
            } else {
                // we are editing an existing patient
                try {
                    Session mlSession = ((SessionBean) Utils.getSB()).getMainzellisteSession();
                    ID myId = new ID("psn", patientResource.getProperty("patientID").getValue());
                    mlSession.removeTempId(myId);
                } catch (MainzellisteNetworkException | InvalidSessionException e) {
                    e.printStackTrace();
                }

                patient.saveOrUpdate();
                if (goBackToPatientlist)
                    Utils.addContextMessage("Success", "The patient data have been changed.");

                getSessionBean().getDatabase().commit();
                ((SessionBean) Utils.getSB()).clearCurrentObject("tokenId");

                // redirect user to the list of patients
                if (goBackToPatientlist)
                    goPatientList();
                return null;
            }
        } finally {
            getSessionBean().getDatabase().stopCaching();
        }
        return null;
    }

    /**
     * JSF Button: edit user.
     *
     * @param PatientData
     *            the patient data
     * @return the string JSF outcome
     * @throws MainzellisteNetworkException
     *             if a network error occurs while making the request to the
     *             Mainzelliste
     * @throws InvalidSessionException
     *             if the Mainzelliste session has expired
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @SuppressWarnings("unchecked")
    public String editPatientAction(Object PatientData)
            throws MainzellisteNetworkException, InvalidSessionException, IOException {
        String patID = (String) ((HashMap<String, Object>) PatientData).get("patientID");
        String path = Utils.generateUrlToWebservice();

        ID myId = new ID("psn", patID);
        EditPatientToken t = new EditPatientToken(myId);

        // There are two configurations of registries. One with IDAT and one
        // without, which only demands a local patient ID
        Boolean doesMainzellisteWantIdat = ((ApplicationBean) Utils.getAB()).doesMainzellisteWantIdat();
        if(doesMainzellisteWantIdat)
            t.setFieldsToEdit(Arrays.asList("vorname", "nachname", "geburtstag", "geburtsmonat", "geburtsjahr", "ort", "geburtsname", "plz"));
        else
            t.setFieldsToEdit(Arrays.asList("patientId"));

        t.redirect(path + "/patient_edit.xhtml");
        // .addIdType("psn");

        Session mlSession = ((SessionBean) Utils.getSB()).getMainzellisteSession();
        String tokenId = mlSession.getToken(t);
        String mlURL = OSSEUtils.getMainzellisteRESTURL();
        String lang = ((SessionBean) Utils.getSB()).getLanguage();
        Utils.redirectToPage(mlURL + "/html/editPatient?language="+ lang +"&tokenId=" + tokenId);
        getSessionBean().setTempObject("dataObject", PatientData);
        return "editPatient";
    }

    /**
     * JSF Button: add user.
     *
     * @return the string
     * @throws MainzellisteNetworkException
     *             if a network error occurs while making the request to the
     *             Mainzelliste
     * @throws InvalidSessionException
     *             if the Mainzelliste session has expired
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public String goAddPatient() throws MainzellisteNetworkException, InvalidSessionException, IOException {
        String path = Utils.generateUrlToWebservice();
        String pathCallback = OSSEUtils.getCallbackURLToRegistry();

        AddPatientToken t = new AddPatientToken();

        String locationName = getLocationList().get(0).getLabel();
        Location location = new Location(getSessionBean().getDatabase());
        Resource locationResource = location.entityExistsByProperty(OSSEVocabulary.Location.Name, locationName);
        t.addField("location", locationResource.getId() + "");

        t.callback(new URL(pathCallback + "/api/mainzelliste/patients/")).redirect(path + "/patient_create.xhtml")
                .addIdType("psn");

        Session mlSession = ((SessionBean) Utils.getSB()).getMainzellisteSession();
        String tokenId = mlSession.getToken(t);
        Utils.getSB().setCurrentObject("tokenId", tokenId);

        String mlURL = OSSEUtils.getMainzellisteRESTURL();
        String lang = ((SessionBean) Utils.getSB()).getLanguage();
        Utils.redirectToPage(mlURL + "/html/createPatient?language="+lang+"&tokenId=" + tokenId);
        return null;
    }

    // must be there for Primefaces bug/feature reason
    public void setPatientTokenPresent(Boolean nothing) {
    }

    /**
     * Checks if we have a patientTokenID
     * 
     * @return true|false
     */
    public Boolean getPatientTokenPresent() {
        return !"".equals(getPatientTokenID());
    }

    /**
     * Gets a tempID for a patient from the Mainzelliste.
     * 
     * @return the patient token id (=tempID)
     */
    @SuppressWarnings("unchecked")
    public String getPatientTokenID() {
        if (patientTokenID != null)
            return patientTokenID;

        String mainzellisteTempID = "";
        try {
            Session mlSession = ((SessionBean) Utils.getSB()).getMainzellisteSession();

            String patientID = getCreatedPatientID();
            if (patientID == null) {
                if (getSessionBean().getTempObject("dataObject") != null
                        && getSessionBean().getTempObject("dataObject") instanceof HashMap) {
                    patientID = (String) ((HashMap<String, Object>) getSessionBean().getTempObject("dataObject"))
                            .get("patientID");
                }
            }

            if (patientID == null)
                return "";

            ID myId = new ID("psn", patientID);

            List<String> fieldsToShow = null;

            if (((ApplicationBean) Utils.getAB()).doesMainzellisteWantIdat())
                fieldsToShow = Arrays.asList("vorname", "nachname", "geburtstag", "geburtsmonat", "geburtsjahr");
            else
                fieldsToShow = Arrays.asList("patientId");

            List<String> idsToShow = Arrays.asList("pid");
            mainzellisteTempID = mlSession.getTempId(myId, fieldsToShow, idsToShow);
        } catch (MainzellisteNetworkException | InvalidSessionException e) {
            // TODO: Dieser Fall sollte vernünfitg abgefangen werden, z.B.
            // versuchen, eine neue Session
            // zu erzeugen oder Fehlermeldung, wenn dies nicht gelingt.
            e.printStackTrace();
        }

        patientTokenID = mainzellisteTempID;

        return mainzellisteTempID;
    }

    /**
     * JSF Command method to initiate a loadPatients with filterPatientsBy set
     * 
     * @param filterStatus
     *            all|open|reported
     * @return
     */
    public String filterPatientsByFormStatus(String filterStatus) {
        if (!"open".equalsIgnoreCase(filterStatus) && !"reported".equalsIgnoreCase(filterStatus)) {
            filterPatientsBy = null;
            loadPatients();
            return null;
        }

        filterPatientsBy = filterStatus;
        loadPatients();
        return null;
    }

    /**
     * filterPatientsByFormStatusChanged
     */
    public void filterPatientsByFormStatusChanged(ValueChangeEvent event) {
        filterPatientsByFormStatus(event.getNewValue().toString());
    }



    /**
     * Gets the Mainzelliste ID of the newly created patient (over the
     * tokenExchangeStorage due to it being delivered via REST interface
     * callback).
     *
     * @return the created patient id
     */
    public String getCreatedPatientID() {
        if (Utils.getSB().getCurrentObject("tokenId") == null) {
            return null;
        }
        String myCode = (String) Utils.getSB().getCurrentObject("tokenId");
        if (TempTokenExchangeStorage.instance.getCurrentObject(myCode) == null)
            return null;
        else {
            return (String) TempTokenExchangeStorage.instance.getCurrentObject(myCode);
        }
    }

    /**
     * Gets the token id.
     *
     * @return the token id
     */
    public String getTokenID() {
        if (Utils.getSB().getCurrentObject("tokenId") == null) {
            return "ERROR NO MYCODE IN SESSIONBEAN";
        }

        String myCode = (String) Utils.getSB().getCurrentObject("tokenId");
        return myCode;
    }

    /**
     * JSF Button: delete case.
     *
     * @param toDeletePatient            the case to be deleted
     */
    public void setToDeletePatient(Object toDeletePatient) {
        getSessionBean().setTempObject("dataObject", toDeletePatient);
    }

    /**
     * JSF Button: undelete case.
     *
     * @param toUnDeletePatient the new to un delete patient
     */
    public void setToUnDeletePatient(Object toUnDeletePatient) {
        getSessionBean().setTempObject("dataObject", toUnDeletePatient);
    }

    /**
     * Method to delete or undelete a case of a patient.
     *
     * @param delete            true: delete, false: undelete
     * @return outcome
     */
    @SuppressWarnings("unchecked")
    public String toggleCase(Boolean delete) {
        dataObject = (HashMap<String, Object>) Utils.getSB().clearTempObject("dataObject");
        if (dataObject == null)
            return "";

        ResourceQuery query = new ResourceQuery(OSSEVocabulary.Type.Patient);
        query.add(Criteria.Equal(OSSEVocabulary.Type.Patient, "patientID", (String) dataObject.get("patientID")));

        ArrayList<Resource> found = getSessionBean().getDatabase().getResources(query);
        for (Resource patientResource : found) {
            Patient patient = new Patient(getSessionBean().getDatabase(), patientResource);

            Case myCase = new Case(getSessionBean().getDatabase(), (Resource) dataObject.get("caseResource"));

            if (delete)
                patient.deleteCaseOfPatient(myCase);
            else
                patient.unDeleteCaseOfPatient(myCase);
        }
        if (delete)
            Utils.displayContextMessage("delete_successful_title", "delete_case_successful");
        else
            Utils.displayContextMessage("undelete_successful_title", "undelete_case_successful");

        loadPatients();

        return "success";
    }

    /**
     * JSF Button: go Patient List.
     */
    public void goPatientList() {
        try {
            Utils.redirectToPage("patientlist.xhtml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the list of patients.
     *
     * @return the patients
     */
    public ArrayList<HashMap<String, Object>> getPatients() {
        if (patients == null)
            loadPatients();
        return patients;
    }

    /**
     * Gets the list of deleted patients.
     *
     * @return the deleted patients
     */
    public ArrayList<HashMap<String, Object>> getDeletedPatients() {
        if (patients == null)
            loadPatients(true);
        return patients;
    }

    /**
     * Determines if we have any (undeleted) patients
     * If showDeleted is set to "1" it will always return false
     * @return
     */
    public Boolean hasPatients() {
        if("1".equalsIgnoreCase(showDeleted))
            return false;
        
        if(getPatients().isEmpty())
            return false;
        
        return true;
    }
    
    /**
     * Gets the location list of the users permission locations as select items.
     *
     * @return the location list
     */
    public List<SelectItem> getLocationList() {
        if (locationsSelectItems == null) {
            locationsSelectItems = new ArrayList<SelectItem>();

            Set<String> myLocations = ((SessionBean) getSessionBean()).getMyPermissions().keySet();

            for (String locationName : myLocations) {
                SelectItem item = new SelectItem();

                item.setLabel(locationName);
                item.setValue(locationName);
                locationsSelectItems.add(item);
            }
        }
        return locationsSelectItems;
    }

    /**
     * Gets the amount deleted patients.
     *
     * @return the amountDeletedPatients
     */
    public Integer getAmountDeletedPatients() {
        return amountDeletedCases;
    }

    /**
     * Gets the show deleted.
     *
     * @return the showDeleted
     */
    public String getShowDeleted() {
        return showDeleted;
    }

    /**
     * Sets the show deleted.
     *
     * @param showDeleted            the showDeleted to set
     */
    public void setShowDeleted(String showDeleted) {
        this.showDeleted = showDeleted;
    }

    public String getFilterPatientsBy() {
        return filterPatientsBy;
    }

    public void setFilterPatientsBy(String filterPatientsBy) {
        this.filterPatientsBy = filterPatientsBy;
    }

}
