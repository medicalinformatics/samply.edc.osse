/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.osse.control;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import de.samply.auth.client.jwt.JWTException;
import de.samply.auth.rest.AccessTokenDTO;
import de.samply.auth.rest.LoginDTO;
import de.samply.common.http.HttpConnector43;
import de.samply.common.http.HttpConnectorException;
import de.samply.common.mdrclient.MdrClient;
import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.control.AbstractApplicationBean;
import de.samply.edc.osse.auth.Auth;
import de.samply.edc.osse.catalog.EpisodePatterns;
import de.samply.edc.osse.dto.teiler.TeilerAccountDTO;
import de.samply.edc.osse.dto.teiler.TeilerDTO;
import de.samply.edc.osse.model.InitDBModel;
import de.samply.edc.osse.upgrade.UpgradeExecution;
import de.samply.edc.osse.upgrade.UpgradeExecutionException;
import de.samply.edc.osse.upgrade.UpgradeExecutionReconfigure;
import de.samply.edc.osse.upgrade.dto.Upgrades;
import de.samply.edc.osse.upgrade.dto.Upgrades.Upgrade;
import de.samply.edc.osse.utils.FormRecreationHelper;
import de.samply.edc.osse.utils.OSSEUtils;
import de.samply.edc.utils.Utils;
import de.samply.edc.utils.VersionNumber;
import de.samply.edc.validator.PasswordComplexityValidator;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.Value;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.exceptions.TablesNotFoundException;
import de.samply.store.osse.OSSEModel;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.store.query.ResourceQuery;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The application scoped Bean.
 */
@ManagedBean(name = "applicationBean", eager = true)
@ApplicationScoped
public class ApplicationBean extends AbstractApplicationBean {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Internal var to force a recreation of forms. only used for DEVELOPMENT
     * purposes
     */
    private static final Boolean redoForms = false;

    /**
     * This number defines how many upgrades shall be executed, so when there is
     * a new upgrade to be executed on application start, upgrade this number to
     * the necessary internal Revision.
     */
    private static final Integer requiredVersion = 16;

    /**  Contains the configuration of the OSSE Register in a JSONResource format. */
    private JSONResource OSSEConfig;

    /** Contains the xhtml of all forms (old and current) ever created for this registry. */
    private JSONResource OSSEFormStorage;

    /** Contains all import XSDs used in the registry. */
    private JSONResource OSSEXSDStorage;

    /** Contains additional information about the build. */
    private Properties osseProperties = new Properties();

    /** A HashMap of all status a form may have and which resource Id it is linked to. */
    private HashMap<Integer, String> formStatus = new HashMap<>();

    /**  A reverse of the map above. */
    private HashMap<String, Integer> formStatusReverse = new HashMap<>();

    /**  The standard status number of a form. */
    private String formStandardStatusNumber;

    /**
     * A map of status and the valid status they may transition to e.g.
     * "reported": ["validated","open"]
     */
    private HashMap<String, ArrayList<String>> statusTransitions;

    /** The formulars available in the registry by language code The LinkedHashMap: key = formname, value = form title. */
    private HashMap<String, LinkedHashMap<String, String>> formulars = new HashMap<String, LinkedHashMap<String, String>>();

    /**
     * The archived formulars (old versions of formulars). The LinkedHashMap:
     * key = formname, value = form title
     */
    private HashMap<String, LinkedHashMap<String, String>> archivedFormulars = new HashMap<String, LinkedHashMap<String, String>>();

    /** The episode formulars. */
    private HashMap<String, LinkedHashMap<String, String>> visitFormulars = new HashMap<String, LinkedHashMap<String, String>>();

    /** The archived episode formulars. */
    private HashMap<String, LinkedHashMap<String, String>> archivedVisitFormulars = new HashMap<String, LinkedHashMap<String, String>>();

    /** The patient forms. */
    private ArrayList<String> patientForms = new ArrayList<String>();
    
    /**  A map storing which mdr entity is in which form. */
    private HashMap<String, List<String>> mdrEntityIsInForm = new HashMap<>();

    /**  List of MDR entities in a record. */
    private HashMap<String, List<String>> recordHasMdrEntities = new HashMap<>();

    /**  Name of the main patientuser case form. */
    private String patientUserPatientMainPage;
    
    /**  Name of the main patientuser episode form. */
    private String patientUserEpisodeMainPage;

    /**
     * Reads the config and form storage and stores it in the JSONResources
     * above. It also copies the Apache configuration data into the OSSEconfig
     * JSONResource.
     *
     * @param model
     *            the backend database model
     * @throws DatabaseException
     *             if a database error occurs
     */
    public void readConfig(OSSEModel model) throws DatabaseException {
        OSSEXSDStorage = model.getConfig("osse.xsd.storage");
        if (OSSEXSDStorage == null)
            OSSEXSDStorage = new JSONResource();

        OSSEFormStorage = model.getConfig("osse.form.storage");
        if (OSSEFormStorage == null)
            OSSEFormStorage = new JSONResource();

        OSSEConfig = model.getConfig("osse");
        if (OSSEConfig == null)
            OSSEConfig = new JSONResource();

        try {
            osseProperties.load(this.getClass().getClassLoader().getResourceAsStream("osse.properties"));
        } catch (Exception e) {
            Utils.getLogger().error("Could not load osse.properties file.", e);
        }

        String[] proxySettings = { Vocabulary.Config.Standard.proxyHTTPHost,
                Vocabulary.Config.Standard.proxyHTTPSHost,
                Vocabulary.Config.Standard.proxyHTTPPort,
                Vocabulary.Config.Standard.proxyHTTPSPort,
                Vocabulary.Config.Standard.proxyHTTPUsername,
                Vocabulary.Config.Standard.proxyHTTPPassword,
                Vocabulary.Config.Standard.proxyHTTPSUsername,
                Vocabulary.Config.Standard.proxyHTTPSPassword,
                Vocabulary.Config.Standard.proxyRealm };

        // inject proxy settings, so they dont get overloaded in the next step
        for (String pSet : proxySettings) {
            OSSEConfig.setProperty(pSet, config.getString(pSet));
        }

        // inject OSSEConfig into Config, so both ways of configuration are
        // returning the same values
        for (String key : OSSEConfig.getDefinedProperties()) {
            config.setProperty(key, OSSEConfig.getProperty(key).getValue());
        }

        // Get formstatus und transitions
        formStatus = new HashMap<>();
        formStatusReverse = new HashMap<>();

        // for now the standard status of a form has the number 1
        setFormStandardStatusNumber("1");

        // Get statuses
        ResourceQuery statusQuery = new ResourceQuery(OSSEVocabulary.Type.Status);
        statusQuery.setFetchAdjacentResources(false);

        ArrayList<Resource> statusResources = model
                .getResources(statusQuery);
        for (Resource statusResource : statusResources) {
            formStatus
                    .put(statusResource.getProperty(OSSEVocabulary.ID)
                            .asInteger(), statusResource.getProperty("name")
                            .getValue());
            formStatusReverse.put(
                    statusResource.getProperty("name").getValue(),
                    statusResource.getProperty(OSSEVocabulary.ID).asInteger());
        }

        if (formStatus.isEmpty())
            System.err
                    .println("*** There were no formStatus defined! At least one has to be!");

        // Transitions are yet only predefined
        HashMap<String, ArrayList<String>> statusTransitions = new HashMap<String, ArrayList<String>>();
        ArrayList<String> temp = new ArrayList<String>();
        temp.add("reported");
        statusTransitions.put("open", temp);

        temp.clear();

        temp.add("open");
        temp.add("validated");
        statusTransitions.put("reported", temp);

        temp.clear();
        temp.add("open");
        statusTransitions.put("validated", temp);
    }

    /**
     * Post-construct initialization. Reads in the configuration, but also
     * initiates the installation if the registry has not yet completely been
     * installed
     *
     * @see de.samply.edc.control.AbstractApplicationBean#init()
     */
    @Override
    public void init() {
        super.init();

        OSSEModel model = null;
        if (OSSEConfig == null)
            OSSEConfig = new JSONResource();

        String configFile = Utils.findConfigurationFile("backend.xml");
        Utils.getLogger().debug("USING BACKEND FILE : " + configFile);
        Boolean configWasRead = false;

        try {
            model = new OSSEModel(configFile);
            model.testTables();

            model.injectLogin("admin");
            readConfig(model);
            model.logout();
            model.close();
            configWasRead = true;
        } catch (TablesNotFoundException e) {
            // This means, the registry was freshly installed, but the database
            // was not created yet, so we create it now, and inject the
            // necessary
            // base users and roles necessary for first administration

            Utils.getLogger().debug(
                    "===========================================");
            Utils.getLogger().error("Installing Database");
            Utils.getLogger().debug(
                    "===========================================");

            try {
                if (model != null)
                    model.close();
                InitDBModel tmodel = new InitDBModel(configFile);
                tmodel.installDatabase();
                tmodel.insertAdminAndDev();
                tmodel.insertStatuses();
                tmodel.insertOSSEConfig(this.config);

                tmodel.logout();
                tmodel.close();
            } catch (Exception ee) {
                ee.printStackTrace();
            }
        } catch (DatabaseException e) {
            e.printStackTrace();
            throw new Error("Database Exception prevents start of OSSE.");
        }

        if (!configWasRead) {
            try {
                model = new OSSEModel(configFile);
                model.injectLogin("admin");
                readConfig(model);
                model.logout();
                model.close();
            } catch (DatabaseException e) {
                e.printStackTrace();
            }
        }

        // Initialise the List of Formulars
        initFormLists();

        
        HttpConnector43 httpConnector = null;
        try {
            httpConnector = new HttpConnector43(getConfig());
        } catch (HttpConnectorException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Init MdrFacesClient
        MdrClient mdrClient = null;
        
        if (httpConnector == null) {
            Utils.getLogger().error("HTTPclient could not be started. MDR Client in context (used by MDRFaces) will not support proxy connection!");
            mdrClient = new MdrClient(OSSEConfig.getProperty(
                    Vocabulary.Config.MDR.REST).getValue());
        } else {
            Client mdrJerseyClient = httpConnector.getJerseyClient(OSSEConfig.getProperty(
                    Vocabulary.Config.MDR.REST).getValue(), false);
            
            mdrClient = new MdrClient(OSSEConfig.getProperty(
                    Vocabulary.Config.MDR.REST).getValue(), mdrJerseyClient);
        }

        de.samply.web.mdrFaces.MdrContext.getMdrContext().init(mdrClient);

        // Pre caching mdr fields, so forms don't take ages to load on first
        // access
        // TODO: needs to check for records to load them "different" etc.
        // TODO: needs more mdrclient/faces support as this currently does not
        // function properly in it,
        // so I commented this out for now

        // for(String key: mdrEntityIsInForm.keySet()) {
        // Utils.getLogger().debug("MDR clienting for key "+key);
        // Definition dataElementDefinition;
        // try {
        // dataElementDefinition = mdrClient.getDataElementDefinition(key,
        // Utils.getLanguage());
        // Utils.getLogger().debug("DataDefinition: "+dataElementDefinition);
        // } catch (MdrConnectionException | MdrInvalidResponseException
        // | ExecutionException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // }
        // }

        // Check if there are any upgrades to be executed
        checkForUpgrades(configFile);

        // initialize password complexity validator
        initPasswordComplexityValidator();
    }

    /**
     * Save config.
     *
     * @param name the name
     * @param config the config
     * @throws DatabaseException the database exception
     */
    private void saveConfig(String name, JSONResource config) throws DatabaseException {
        String configFile = Utils.findConfigurationFile("backend.xml");
        InitDBModel model = new InitDBModel(configFile);
        model.saveOSSEConfig(name, config);
        model.logout();
        model.close();
    }

    /**
     * Inits the password complexity validator.
     */
    private void initPasswordComplexityValidator() {
        // Minimum length of a password (minimal 4 is accepted, standard is 8)
        Integer minPasswordLength, maxPasswordLength, minLowerAlphaChars, minUpperAlphaChars, minSpecialChars, minNumericalChars;
        minPasswordLength = getOSSEConfigAsInteger(Vocabulary.Config.Password.minLength);
        if (minPasswordLength == null) {
            // not initialized yet, so we do it here to standard settings
            setOSSEConfig(Vocabulary.Config.Password.minLength, 8);
            setOSSEConfig(Vocabulary.Config.Password.maxLength, 255);
            setOSSEConfig(Vocabulary.Config.Password.minLowercase, 1);
            setOSSEConfig(Vocabulary.Config.Password.minUppercase, 1);
            setOSSEConfig(Vocabulary.Config.Password.minSpecial, 1);
            setOSSEConfig(Vocabulary.Config.Password.minNumerical, 1);
            try {
                saveConfig("osse", OSSEConfig);
            } catch (DatabaseException e) {
                e.printStackTrace();
            }
            minPasswordLength = getOSSEConfigAsInteger(Vocabulary.Config.Password.minLength);
        }

        if (minPasswordLength < 4)
            minPasswordLength = 4;
        // Maximum length of a password (if 0 then it can be huge = 255 chars
        // long)
        maxPasswordLength = getOSSEConfigAsInteger(Vocabulary.Config.Password.maxLength);
        if (maxPasswordLength == null)
            maxPasswordLength = 255;
        if (maxPasswordLength < 1)
            maxPasswordLength = 255;
        // Minimum amount of lowercase alpha characters in the password
        minLowerAlphaChars = getOSSEConfigAsInteger(Vocabulary.Config.Password.minLowercase);
        if (minLowerAlphaChars == null)
            minLowerAlphaChars = 1;
        // Minimum amount of uppercase alpha characters in the password
        minUpperAlphaChars = getOSSEConfigAsInteger(Vocabulary.Config.Password.minUppercase);
        // Minimum amount of special characters in the password
        minSpecialChars = getOSSEConfigAsInteger(Vocabulary.Config.Password.minSpecial);
        // Minimum amount of numerical characters in the password
        minNumericalChars = getOSSEConfigAsInteger(Vocabulary.Config.Password.minNumerical);

        // The following configurations are yet not changeable by the
        // adminstrator and are mostly unused in the registry

        // Allow extended ascii characters in the password?
        boolean allowExtendedAsciiSymbols = false;
        // The password must differ by X amount of characters compared to the
        // last one
        int lastPasswordDifferInChars = 4;
        // Validate we haven't used this password in this many iterations (we
        // use the list of old pw's you pass in for this)
        int passwordHistoryLen = 4;
        // Allow phone numbers in the password?
        boolean allowPhoneNumbers = false;
        // Allow dates in the password?
        boolean allowDates = false;
        // Deny dictionary words within the password?
        boolean restrictedByDictionary = false;
        // Default Bloom Filter settings
        float dictionaryAccuracy = 17;
        // Default dictionary word length. Anything smaller and you'll get lots
        // of hits
        int dictionaryMinWordLength = 4;

        PasswordComplexityValidator.configure(minPasswordLength,
                maxPasswordLength, minLowerAlphaChars, minUpperAlphaChars,
                minSpecialChars, minNumericalChars, allowExtendedAsciiSymbols,
                lastPasswordDifferInChars, passwordHistoryLen,
                allowPhoneNumbers, allowDates, restrictedByDictionary,
                dictionaryAccuracy, dictionaryMinWordLength);
    }

    /**
     * Check for upgrades and executes them.
     *
     * @param configFile            the config file
     */
    @SuppressWarnings("unchecked")
    private void checkForUpgrades(String configFile) {
        Utils.getLogger()
                .debug("--------------------\n --- Upgrades starting\n--------------------");

        String upgradeFileName = "theOneUpgrade.xml";
        String upgradeFile = Utils.findConfigurationFile(upgradeFileName);
        Upgrades upgrades = null;

        if (upgradeFile == null) {
            Utils.getLogger()
                    .error("Cannot execute any upgrade: There was no upgrades file present!");
            return;
        }

        JAXBContext jaxbContext;
        try {
            jaxbContext = JAXBContext.newInstance(Upgrades.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            upgrades = (Upgrades) jaxbUnmarshaller.unmarshal(new File(
                    upgradeFile));
        } catch (JAXBException e1) {
            e1.printStackTrace();
        }

        if (upgrades == null) {
            Utils.getLogger()
                    .error("Cannot execute any upgrade: The upgrades file could not be read as proper XML!");
            return;
        }

        // Check if the upgradeUUID in the upgrade.xml is the same as in our DB,
        // then we already done this upgrade
        // and can assume nothing has to be done.
        if (OSSEConfig.getProperty(Vocabulary.Config.executedUpgradeUUID) != null
                && upgrades.getUpdateUUID().equalsIgnoreCase(
                        OSSEConfig.getProperty(
                                Vocabulary.Config.executedUpgradeUUID)
                                .getValue())) {
            Utils.getLogger().debug(
                    "The upgrade with UUID " + upgrades.getUpdateUUID()
                            + " was already done.");
            return;
        }

        // Put Upgrades into a HashMap<key, upgrade> to reference a wanted one
        HashMap<String, Upgrade> upgradeMap = new HashMap<>();
        for (Upgrade entry : upgrades.getUpgrade()) {
            String internalRevision = entry.getVersion().getInternalRevision();
            upgradeMap.put(internalRevision, entry);
        }

        Boolean didUpgrade = false;

        // run through the upgrades until the preset requiredVersion internal
        // revision number defined above
        for (int i = 1; i <= requiredVersion; i++) {
            Utils.getLogger().debug("Running UpgradeExecution" + i + ".java");
            Class<? extends UpgradeExecution> c = null;
            String javaFile = "de.samply.edc."
                    + getConfig().getString("instance.project")
                    + ".upgrade.UpgradeExecution" + i;

            try {
                c = (Class<? extends UpgradeExecution>) Class.forName(javaFile);
            } catch (ClassNotFoundException e) {
                System.err.println("Did not find class " + javaFile
                        + ", but there should be one! I will check in osse as instance project name as fallback.");

                try {
                    javaFile = "de.samply.edc.osse.upgrade.UpgradeExecution" + i;
                    c = (Class<? extends UpgradeExecution>) Class.forName(javaFile);
                } catch(ClassNotFoundException ee) {
                    System.err.println("Did not find class " + javaFile
                            + ", but there should be one!");
                    throw new Error("There is no UpgradeExecution" + i
                            + ".java available!");
                }
            }

            if (c != null) {
                try {
                    UpgradeExecution upgradeExecution = null;
                    if (upgradeMap.get("" + i) != null) {
                        Constructor<? extends UpgradeExecution> cons = c
                                .getConstructor(Upgrade.class,
                                        JSONResource.class, ApplicationBean.class);
                        upgradeExecution = cons.newInstance(
                                upgradeMap.get("" + i), OSSEConfig, this);
                    } else {
                        Constructor<? extends UpgradeExecution> cons = c
                                .getConstructor(JSONResource.class, ApplicationBean.class);
                        upgradeExecution = cons.newInstance(OSSEConfig, this);
                    }

                    Boolean executedUpgrade;
                    try {
                        executedUpgrade = upgradeExecution.doUpgrade();
                    } catch (UpgradeExecutionException e) {
                        Utils.getLogger()
                                .debug("An upgrade could not be finished successfully. Bailing out of the complete set of upgrading!");
                        e.printStackTrace();
                        return;
                    }

                    didUpgrade = didUpgrade | executedUpgrade;

                } catch (NoSuchMethodException | SecurityException
                        | InstantiationException | IllegalAccessException
                        | IllegalArgumentException | InvocationTargetException e) {
                    e.printStackTrace();
                    System.err.println("Something went wrong");
                }
            }
        }

        // Insert the reconfigure settings
        UpgradeExecution upgradeExecution = new UpgradeExecutionReconfigure(upgradeMap.get("reconfigure"), OSSEConfig,
                this);
        Boolean executedUpgrade;
        try {
            executedUpgrade = upgradeExecution.doUpgrade();
        } catch (UpgradeExecutionException e) {
            Utils.getLogger()
                    .debug("An reconfigure upgrade could not be finished successfully. Bailing out of the complete set of upgrading!");
            e.printStackTrace();
            return;
        }

        didUpgrade = didUpgrade | executedUpgrade;

        // After upgrades were done, the config etc needs to be reloaded.
        // the easiest way to make sure all is reloaded is to call init again.
        if (didUpgrade) {
            Utils.getLogger().debug("Any upgrade was done, so calling init");

            OSSEConfig.setProperty(Vocabulary.Config.executedUpgradeUUID,
                    upgrades.getUpdateUUID());
            try {
                InitDBModel tmodel = new InitDBModel(configFile);
                tmodel.saveOSSEConfig("osse", OSSEConfig);
                tmodel.logout();
                tmodel.close();
            } catch (DatabaseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            init();
        }

        Utils.getLogger()
                .debug("--------------------\n --- Upgrades finished\n--------------------");
    }

    /**
     * Copies the current config to the factory standard.
     *
     * @param model
     *            the backend database model
     * @throws DatabaseException
     *             if a database error occurs
     */
    public void copyConfigToStandard(OSSEModel model) throws DatabaseException {
        OSSEConfig = model.getConfig("osse");
        model.saveConfig("osse.factory.settings", OSSEConfig);
    }

    /**
     * Gets the forms of this registry TODO: "Formulars" ist kein englisches
     * Wort. Besser in "getForms" umbenennen, folgende Methoden entsprechend.
     *
     * @param locale
     *            the locale
     * @return the forms.
     */
    public LinkedHashMap<String, String> getFormulars(Locale locale) {
        if (!isASupportedLocale(locale)) {
            System.err
                    .println("System tried to get formulars with locale "
                            + locale.getLanguage()
                            + " which is not supported! Giving back standard language.");
            return formulars.get(FacesContext.getCurrentInstance()
                    .getApplication().getDefaultLocale().getLanguage());
        }

        return formulars.get(locale.getLanguage());
    }

    /**
     * Gets the amount of visit forms (visit was renamed episode, so here for
     * compatibility reasons).
     *
     * @return the amount visit forms
     */
    @Deprecated
    public Integer getAmountVisitFormulars() {
        if (visitFormulars == null || visitFormulars.isEmpty())
            return 0;

        return visitFormulars.get(supportedLocales.get(0)).size();
    }

    /**
     * Gets the amount of episode forms.
     *
     * @return the amount episode forms
     */
    public Integer getAmountEpisodeFormulars() {
        if (visitFormulars == null || visitFormulars.isEmpty())
            return 0;

        return visitFormulars.get(supportedLocales.get(0)).size();
    }

    /**
     * Gets the visit forms for a certain locale. If the locale is not
     * supported, the default locale will be used.
     *
     * @param locale
     *            the locale
     * @return the visit formulars
     */
    public LinkedHashMap<String, String> getVisitFormulars(Locale locale) {
        if (!isASupportedLocale(locale)) {
            System.err
                    .println("System tried to get formulars with locale "
                            + locale.getLanguage()
                            + " which is not supported!  Giving back standard language.");
            return formulars.get(FacesContext.getCurrentInstance()
                    .getApplication().getDefaultLocale().getLanguage());
        }
        return visitFormulars.get(locale.getLanguage());
    }

    /**
     * Checks if a form is a case form (previously called "unique" form).
     *
     * @param formName            the form name
     * @return true|false
     */
    public Boolean isUniqueForm(String formName) {
        if (formulars.get(
                FacesContext.getCurrentInstance().getApplication()
                        .getDefaultLocale().getLanguage())
                .containsKey(formName)
                || archivedFormulars.get(
                        FacesContext.getCurrentInstance().getApplication()
                                .getDefaultLocale().getLanguage()).containsKey(
                        formName))
            return true;

        return false;
    }

    /**
     * Checks if a form is a visit form.
     *
     * @param formName
     *            the form name
     * @return true|false
     */
    @Deprecated
    public Boolean isVisitForm(String formName) {
        if (visitFormulars.get(
                FacesContext.getCurrentInstance().getApplication()
                        .getDefaultLocale().getLanguage())
                .containsKey(formName)
                || archivedVisitFormulars.get(
                        FacesContext.getCurrentInstance().getApplication()
                                .getDefaultLocale().getLanguage()).containsKey(
                        formName))
            return true;

        return false;
    }

    /**
     * Checks if a form is an episode form.
     *
     * @param formName
     *            the form name
     * @return true|false
     */
    public Boolean isEpisodeForm(String formName) {
        if (visitFormulars.get(
                FacesContext.getCurrentInstance().getApplication()
                        .getDefaultLocale().getLanguage())
                .containsKey(formName)
                || archivedVisitFormulars.get(
                        FacesContext.getCurrentInstance().getApplication()
                                .getDefaultLocale().getLanguage()).containsKey(
                        formName))
            return true;

        return false;
    }

    /**
     * Checks if a form is a medical form (either a case or an episode form).
     *
     * @param formName
     *            the form name
     * @return true|false
     */
    public Boolean isMedicalForm(String formName) {
        return (isUniqueForm(formName) || isEpisodeForm(formName) || isPatientForm(formName));
    }

    /**
     * Loads the form lists of all supported languages.
     */
    public void initFormLists() {
        clearPatientUserForms();
        
        mdrEntityIsInForm = new HashMap<>();
        JSONResource mdrEntityIsInFormJSON = new JSONResource();
        if (OSSEConfig.getProperty(Vocabulary.Config.mdrEntityIsInForm) != null) {
            mdrEntityIsInFormJSON = OSSEConfig.getProperty(Vocabulary.Config.mdrEntityIsInForm).asJSONResource();
            if (mdrEntityIsInFormJSON != null) {
                for (String key : mdrEntityIsInFormJSON.getDefinedProperties()) {
                    List<Value> moo = mdrEntityIsInFormJSON.getProperties(key);
                    List<String> forms = new ArrayList<String>();
                    for (Value me : moo) {
                        forms.add(me.getValue());
                    }
                    mdrEntityIsInForm.put(key, forms);
                }
            }
        }

        recordHasMdrEntities = new HashMap<>();
        JSONResource recordHasMdrEntitiesJSON = new JSONResource();
        if (OSSEConfig.getProperty(Vocabulary.Config.recordHasMdrEntities) != null) {
            recordHasMdrEntitiesJSON = OSSEConfig.getProperty(Vocabulary.Config.recordHasMdrEntities).asJSONResource();
            if (recordHasMdrEntitiesJSON != null) {
                for (String key : recordHasMdrEntitiesJSON.getDefinedProperties()) {
                    List<Value> moo = recordHasMdrEntitiesJSON.getProperties(key);
                    List<String> forms = new ArrayList<String>();
                    for (Value me : moo) {
                        forms.add(me.getValue());
                    }
                    recordHasMdrEntities.put(key, forms);
                }
            }
        }

        for (String locale : supportedLocales) {
            Locale theLocale = new Locale(locale);

            if (OSSEConfig.getProperty(Vocabulary.Config.Form.patient) == null) {
                break;
            }

            LinkedHashMap<String, String> forms;
            LinkedHashMap<String, String> archivedForms = new LinkedHashMap<>();
            LinkedHashMap<String, String> visitForms = new LinkedHashMap<>();
            LinkedHashMap<String, String> archivedVisitForms = new LinkedHashMap<>();

            forms = getPageTitleNamesByLocale(theLocale, OSSEConfig.getProperty(Vocabulary.Config.Form.patient).getValue());

            if (OSSEConfig.getProperty(Vocabulary.Config.Form.archivedPatient) != null) {
                archivedForms = getPageTitleNamesByLocale(theLocale, OSSEConfig.getProperty(Vocabulary.Config.Form.archivedPatient).getValue());
            }

            if (OSSEConfig.getProperty(Vocabulary.Config.Form.visit) != null) {
                visitForms = getPageTitleNamesByLocale(theLocale, OSSEConfig.getProperty(Vocabulary.Config.Form.visit).getValue());

                if (OSSEConfig.getProperty(Vocabulary.Config.Form.archivedVisit) != null) {
                    archivedVisitForms = getPageTitleNamesByLocale(theLocale, OSSEConfig.getProperty(Vocabulary.Config.Form.archivedVisit).getValue());
                }
            }

            formulars.put(locale, forms);
            archivedFormulars.put(locale, archivedForms);
            visitFormulars.put(locale, visitForms);
            archivedVisitFormulars.put(locale, archivedVisitForms);
        }

        // fill list of patientforms
        patientForms = new ArrayList<>();

        String patientFormList = OSSEConfig.getString(Vocabulary.Config.PatientForm.list);
        String patientFormArchivedList = OSSEConfig.getString(Vocabulary.Config.PatientForm.archivedList);
        patientFormList = (patientFormList == null)?"":patientFormList;
        patientFormArchivedList = (patientFormArchivedList == null)?"":patientFormArchivedList;
        String concatString = (!patientFormList.isEmpty() && !patientFormArchivedList.isEmpty())?",":"";
        patientForms = new ArrayList<>(Arrays.asList(patientFormList.concat(concatString).concat(patientFormArchivedList).split(",")));

        // In case of a redeploy the xhtml files were deleted, so we have to
        // recover them from the DB
        recoveryCheck();

        // DEVELOPMENT PURPOSES ONLY: remake all forms from scratch
        if (redoForms)
            remakeForms();

        getPatientUserEpisodeMainPage();
        getPatientUserPatientMainPage();
    }

    /**
     * Takes a csv-type string of form ids and creates a map of the form's titles.
     *
     * @param locale The language in which the form titles should be.
     * @param formsString A comma separated string of form ids.
     * @return A map containing form titles assigned to the form's ids.
     */
    private LinkedHashMap<String, String> getPageTitleNamesByLocale(Locale locale, String formsString) {
        LinkedHashMap<String, String> result = new LinkedHashMap<>();

        if (formsString instanceof String) {
            for (String formString : formsString.split(",")) {
                if (formString != null && !formString.isEmpty()) {
                    result.put(formString, getPageTitleName(formString, locale));
                }
            }
        }

        return result;
    }

    /**
     * Clear patient user forms.
     */
    public void clearPatientUserForms() {
        patientUserPatientMainPage = null;
        patientUserEpisodeMainPage = null;
    }
    
    /**
     * Gets the patient user patient main page.
     *
     * @return the patient user patient main page
     */
    public String getPatientUserPatientMainPage() {
        if(patientUserPatientMainPage != null)
            return patientUserPatientMainPage;
        
        LinkedHashMap<String, String> list = getFormulars(Utils.getDefaultLocale());
        
        patientUserPatientMainPage = "noform";

        if(list != null && !list.isEmpty()) {
            for(String formName:list.keySet()) {
                if(isPatientForm(formName)) {
                    patientUserPatientMainPage = formName;
                }
            }
        }
        
        return patientUserPatientMainPage;
    }

    /**
     * Gets the patient user episode main page.
     *
     * @return the patient user episode main page
     */
    public String getPatientUserEpisodeMainPage() {
        if(patientUserEpisodeMainPage != null)
            return patientUserEpisodeMainPage;
        
        LinkedHashMap<String, String> list = getVisitFormulars(Utils.getDefaultLocale());
        
        patientUserEpisodeMainPage = "noform";

        if(list != null && !list.isEmpty()) {
            for(String formName:list.keySet()) {
                if(isPatientForm(formName)) {
                    patientUserEpisodeMainPage = formName;
                }
            }
        }
        
        return patientUserEpisodeMainPage;
    }
    
    /**
     * DEVELOPMENT PURPOSES ONLY Recreates all forms from scratch.
     */
    private void remakeForms() {
        FormRecreationHelper rch = new FormRecreationHelper(this);
        try {
            rch.recreateForms();
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }

    /**
     * Checks if the form file exists of a given form name.
     *
     * @param formName the form name
     * @return the boolean
     */
    private Boolean doesFormFileExist(String formName) {
        String saveFileName = "/forms/" + formName + ".xhtml";
        saveFileName = Utils.getRealPath(saveFileName);
        File file = new File(saveFileName);
        if (file.exists()) {
            Utils.getLogger().debug(
                    "Form " + formName + " exists as form in " + saveFileName
                            + ". No recovery necessary.");
            return true;
        }

        return false;
    }

    /**
     * Check and recover lost xhtml form files.
     */
    private void recoveryCheck() {
        // Check that the files of the forms are available
        // After redeploying they might have been deleted, so we have to recover
        // them

        // Do we have a storage?
        if (OSSEFormStorage.getProperty(Vocabulary.Config.Form.storage) == null)
            return;

        JSONResource formStorage = OSSEFormStorage.getProperty(
                Vocabulary.Config.Form.storage).asJSONResource();
        if (formStorage == null)
            return;

        // if any form is missing, we will just remake all forms right away
        for (String formName : archivedFormulars.get("en").keySet()) {
            if (!doesFormFileExist(formName)) {
                Utils.getLogger()
                        .debug("RecoveryCheck: Form " + formName + " is not existing, remaking all forms now.");
                remakeForms();
                return;
            }
        }

        for (String formName : formulars.get("en").keySet()) {
            if (!doesFormFileExist(formName)) {
                Utils.getLogger()
                        .debug("RecoveryCheck: Form " + formName + " is not existing, remaking all forms now.");
                remakeForms();
                return;
            }
        }

        for (String formName : archivedVisitFormulars.get("en").keySet()) {
            if (!doesFormFileExist(formName)) {
                Utils.getLogger()
                        .debug("RecoveryCheck: Form " + formName + " is not existing, remaking all forms now.");
                remakeForms();
                return;
            }
        }

        for (String formName : visitFormulars.get("en").keySet()) {
            if (!doesFormFileExist(formName)) {
                Utils.getLogger()
                        .debug("RecoveryCheck: Form " + formName + " is not existing, remaking all forms now.");
                remakeForms();
                return;
            }
        }
    }

    /**
     * Gets the form title of a form.
     *
     * @param formName
     *            the form name
     * @return the form title
     */
    public String getFormTitle(String formName) {
        String result = "";

        if (OSSEFormStorage instanceof JSONResource) {
            Value formNameMatrix = OSSEFormStorage.getProperty(Vocabulary.Config.Form.formNameMatrix);
            if (formNameMatrix instanceof Value) {
                Value formNamePropertyLocalized = formNameMatrix.asJSONResource().getProperty(formName + "_" + ((SessionBean) Utils.getSB()).getCurrentLanguage());
                if (!(formNamePropertyLocalized instanceof Value)) {
                    Value formNameProperty = formNameMatrix.asJSONResource().getProperty(formName);
                    if (formNameProperty instanceof Value) {
                        result = formNameProperty.getValue();
                    }
                }
            }
        }

        return result;
    }

    /**
     * Creates an account in a registry of registers (RoR) to be used by the
     * "Teiler"/Samply.Share
     *
     * @param url
     *            the URL to the RoR
     * @param username
     *            the username
     * @param password
     *            the password
     * @return created|updated|failed
     * @throws HttpConnectorException
     *             the http connector exception
     */
    public String createTeilerAccount(String url, String username,
            String password) throws HttpConnectorException {
        HttpConnector43 hc = new HttpConnector43(getConfig());
        String teilerURL = getConfig().getString(Vocabulary.Config.Teiler.REST);
        Client client = hc.getJerseyClient(teilerURL, false);

        TeilerAccountDTO accountData = new TeilerAccountDTO();
        accountData.setAddress(url);
        accountData.setUsername(username);
        accountData.setPassword(password);

        ClientResponse response = client.resource(teilerURL + "/rest/osse-ror")
                .accept(MediaType.APPLICATION_JSON)
                .type(MediaType.APPLICATION_JSON)
                .put(ClientResponse.class, accountData);

        if (response.getStatus() == 201) {
            return "created";
        }

        if (response.getStatus() == 204) {
            return "updated";
        }

        return "failed";
    }

    /**
     * Redirects the user to the Teiler and makes sure the user is logged in.
     *
     * @return the string
     * @throws HttpConnectorException
     *             the http connector exception
     */
    public String goTeiler() throws HttpConnectorException {
        return goTeiler(false);
    }

    /**
     * Redirects the user to the Teiler Exporter and makes sure the user is
     * logged in.
     *
     * @return the string
     * @throws HttpConnectorException
     *             the http connector exception
     */
    public String goTeilerExport() throws HttpConnectorException {
        return goTeiler(true);
    }

    /**
     * Redirects the user to the Teiler and makes sure the user is logged in.
     *
     * @param goExport
     *            Redirect to data export or to teiler main page
     * @return the string
     * @throws HttpConnectorException
     *             the http connector exception
     */
    private String goTeiler(Boolean goExport) throws HttpConnectorException {
        HttpConnector43 hc = new HttpConnector43(getConfig());

        // get configured values
        String teilerURL = OSSEUtils.getTeilerRESTURL();
        String teilerInternalPort = getConfig().getString(Vocabulary.Config.Teiler.internalTeilerPort);

        // if an internal port is defined, we go via localhost
        String internalURL = teilerURL;
        if (teilerInternalPort != null && !teilerInternalPort.equals("")) {
            internalURL = "http://localhost:" + teilerInternalPort + "/osse-share";
        }

        Client client = hc.getJerseyClient(internalURL, false);
        String apiKey = null;
        if (goExport)
            apiKey = getConfig().getString(Vocabulary.Config.Teiler.apiKeyExport);
        else
            apiKey = getConfig().getString(Vocabulary.Config.Teiler.apiKey);

        ClientResponse response = client.resource(internalURL + "/rest/tokens")
                .header("apiKey", apiKey).accept(MediaType.APPLICATION_JSON)
                .type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
        if (response.getStatus() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
        }

        TeilerDTO theToken = response.getEntity(TeilerDTO.class);
        client.destroy();
        hc.closeClients();
        try {
            Utils.redirectToPage(teilerURL + "/index.xhtml?signinToken="
                    + theToken.getSigninToken());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    /**
     * Redirects the user to the form repository and provides a Samply.AUTH
     * login code.
     *
     * @return the string
     */
    public String goFormRepository() {
        try {
            String feClientId = getConfig().getString(
                    Vocabulary.Config.FormEditor.formEditorClientId);


            LoginDTO loginDTO = Auth.getClientCommunicationCode(feClientId, getConfig());
            String feURL = getConfig().getString(
                    Vocabulary.Config.FormEditor.formEditorBASE)
                    + "/samplyLogin.xhtml";
            feURL += "?code=" + loginDTO.getCode();
            Utils.redirectToPage(feURL);
        } catch (InvalidKeyException | NoSuchAlgorithmException
                | SignatureException | JWTException | IOException e) {
            e.printStackTrace();
        }

        return "";
    }


    /**
     * Gets the OSSE form storage, i.e. the xhtml of all forms (old and current)
     * ever created for this registry.
     *
     * @return the OSSE form storage
     */
    public JSONResource getOSSEFormStorage() {
        return OSSEFormStorage;
    }

    /**
     * Gets the OSSE XSD storage, i.e. the import XSD ever created for this
     * registry.
     *
     * @return the OSSE XSD storage
     */
    public JSONResource getOSSEXSDStorage() {
        return OSSEXSDStorage;
    }

    /**
     * Gets the OSSE config.
     *
     * @return the OSSE config
     */
    public JSONResource getOSSEConfig() {
        return OSSEConfig;
    }

    /**
     * Gets the name of the patient main page.
     *
     * @return the patient main page name
     */
    public String getPatientMainPage() {
        return OSSEConfig.getString(Vocabulary.Config.Form.patientStandard);
    }

    /**
     * Gets the title of a page in a certain locale.
     *
     * @param form the form
     * @param locale the locale
     * @return the page title name
     */
    @Override
    public String getPageTitleName(String form, Locale locale) {
        String title = "";

        JSONResource formNames = OSSEConfig.getProperty(Vocabulary.Config.formNames).asJSONResource();
        if (formNames != null && formNames.getProperty(form) != null) {
            Value titleProperty = formNames.getProperty(form + "_" + locale.getLanguage());
            if (titleProperty instanceof Value) {
                title = titleProperty.getValue();
            }
            if (title == null || title.trim().isEmpty()) {
                title = formNames.getProperty(form).getValue();
            }
        }

        if (title == null || "".equals(title)) {
            if (OSSEConfig.getProperty(Vocabulary.Config.archivedFormNames) != null) {
                // check in archived Vocabulary.Config.archivedFormNames
                formNames = OSSEConfig.getProperty(
                        Vocabulary.Config.archivedFormNames).asJSONResource();
                if (formNames != null && formNames.getProperty(form) != null)
                    title = formNames.getProperty(form).getValue();
            }

            if (title == null || "".equals(title)) {
                // Fallback to messagebundle
                try {
                    title = Utils.getResourceBundleString(
                            "de.samply.edc.osse.messages.formnames", form);
                } catch (MissingResourceException e) {
                    System.err.println("No entry in formnames bundle for form "
                            + form);
                }

                if (title == null || "".equals(title)) {
                    System.err.println("Form " + form + " has no title");
                    return "No Title";
                }
            }
        }

        return title;
    }

    /**
     * Gets the archived forms, HashMap of laguagecode, and a LinkedHashMap
     * containing: key = formname, value = form title.
     *
     * @return the archived forms
     */
    public HashMap<String, LinkedHashMap<String, String>> getArchivedFormulars() {
        return archivedFormulars;
    }

    /**
     * Sets the archived forms, HashMap of laguagecode, and a LinkedHashMap
     * containing: key = formname, value = form title.
     *
     * @param archivedFormulars            the archived forms
     */
    public void setArchivedFormulars(
            HashMap<String, LinkedHashMap<String, String>> archivedFormulars) {
        this.archivedFormulars = archivedFormulars;
    }

    /**
     * Gets the archived episode forms, HashMap of laguagecode, and a
     * LinkedHashMap containing: key = formname, value = form title.
     *
     * @return the archived visit forms
     */
    public HashMap<String, LinkedHashMap<String, String>> getArchivedVisitFormulars() {
        return archivedVisitFormulars;
    }

    /**
     * Gets the archived episode forms, HashMap of laguagecode, and a
     * LinkedHashMap containing: key = formname, value = form title.
     *
     * @param archivedVisitFormulars            the archived visit forms
     */
    public void setArchivedVisitFormulars(
            HashMap<String, LinkedHashMap<String, String>> archivedVisitFormulars) {
        this.archivedVisitFormulars = archivedVisitFormulars;
    }

    /**
     * Gets the OSSE version.
     *
     * @return the OSSE version
     */
    public String getOSSEVersion() {
        if (OSSEConfig.getProperty(Vocabulary.Config.version) == null)
            return "1.0.0";

        return OSSEConfig.getProperty(Vocabulary.Config.version).getValue();
    }

    /**
     * Gets the OSSE version.
     *
     * @return the OSSE version
     */
    public String getOSSEBuildVersion() {
        return osseProperties.getProperty("build.version");
    }

    /**
     * Gets the executed upgrade uuid as stored in the DB.
     *
     * @return the executed upgrade uuid
     */
    public String getExecutedUpgradeUUID() {
        if (OSSEConfig.getProperty(Vocabulary.Config.executedUpgradeUUID) == null)
            return "";

        return OSSEConfig.getProperty(Vocabulary.Config.executedUpgradeUUID)
                .getValue();
    }

    /**
     * This is the port that the Mainzelliste shall use in its callback to
     * access a registry via "localhost" on a special port. If you do not set a
     * port here, the URL is calculated, by what you have called your registry
     * in the browser
     *
     * @return the internal mainzelliste port
     */
    public String getInternalMainzellistePort() {
        if (OSSEConfig
                .getProperty(Vocabulary.Config.Mainzelliste.RESTInternalPort) == null)
            return null;
        else
            return OSSEConfig.getProperty(
                    Vocabulary.Config.Mainzelliste.RESTInternalPort).getValue();
    }

    /**
     * Gets the form status.
     *
     * @return the form status
     */
    public HashMap<Integer, String> getFormStatus() {
        return formStatus;
    }

    /**
     * Gets the form status reverse.
     *
     * @return the form status reverse
     */
    public HashMap<String, Integer> getFormStatusReverse() {
        return formStatusReverse;
    }

    /**
     * Determines if we have any mdr entities in our registry yet
     * @return
     */
    public Boolean hasMdrEntities() {
        if(mdrEntityIsInForm == null || mdrEntityIsInForm.isEmpty())
            return false;
        
        return true;
    }
    
    /**
     * Gets the mdr entity is in form.
     *
     * @return the mdr entity is in form
     */
    public HashMap<String, List<String>> getMdrEntityIsInForm() {
        return mdrEntityIsInForm;
    }

    /**
     * Sets the mdr entity is in form.
     *
     * @param mdrEntityIsInForm
     *            the mdr entity is in form
     */
    public void setMdrEntityIsInForm(
            HashMap<String, List<String>> mdrEntityIsInForm) {
        this.mdrEntityIsInForm = mdrEntityIsInForm;
    }

    /**
     * Gets the mdr entities of a record.
     *
     * @return the list of mdr entities of a record
     */
    public HashMap<String, List<String>> getRecordHasMdrEntities() {
        return recordHasMdrEntities;
    }

    /**
     * Sets the mdr entities of a record.
     *
     * @param recordHasMdrEntities
     *            the recordHasMdrEntities to set for a record
     */
    public void setRecordHasMdrEntities(
            HashMap<String, List<String>> recordHasMdrEntities) {
        this.recordHasMdrEntities = recordHasMdrEntities;
    }

    /**
     * Gets the case forms, HashMap of laguagecode, and a LinkedHashMap
     * containing: key = formname, value = form title.
     *
     * @return the forms
     */
    public HashMap<String, LinkedHashMap<String, String>> getFormulars() {
        return formulars;
    }

    /**
     * Gets the case forms, HashMap of laguagecode, and a LinkedHashMap
     * containing: key = formname, value = form title.
     *
     * @param formulars the formulars
     */
    public void setFormulars(
            HashMap<String, LinkedHashMap<String, String>> formulars) {
        this.formulars = formulars;
    }

    /**
     * Gets the episode forms, HashMap of laguagecode, and a LinkedHashMap
     * containing: key = formname, value = form title.
     *
     * @return the episode forms
     */
    public HashMap<String, LinkedHashMap<String, String>> getVisitFormulars() {
        return visitFormulars;
    }

    /**
     * Sets the episode forms, HashMap of laguagecode, and a LinkedHashMap
     * containing: key = formname, value = form title.
     *
     * @param visitFormulars            the visit forms
     */
    public void setVisitFormulars(
            HashMap<String, LinkedHashMap<String, String>> visitFormulars) {
        this.visitFormulars = visitFormulars;
    }

    /**
     * Gets the OSSE config as integer.
     *
     * @param key the key
     * @return the OSSE config as integer
     */
    public Integer getOSSEConfigAsInteger(String key) {
        if (OSSEConfig.getProperty(key) != null)
            return OSSEConfig.getProperty(key).asInteger();
        else
            return null;
    }

    /**
     * Sets the osse config.
     *
     * @param key the key
     * @param value the value
     */
    public void setOSSEConfig(String key, Integer value) {
        OSSEConfig.setProperty(key, value);
    }

    /**
     * Checks if a registry has case forms defined.
     *
     * @return the boolean
     */
    public Boolean hasCaseForms() {
        String formlist = config.getString(Vocabulary.Config.Form.patient);
        if (formlist == null || "".equals(formlist))
            return false;
        else
            return true;
    }

    /**
     * Checks if a registry has episode forms defined.
     *
     * @return the boolean
     */
    public Boolean hasEpisodeForms() {
        String formlist = config.getString(Vocabulary.Config.Form.visit);
        if (formlist == null || "".equals(formlist))
            return false;
        else
            return true;
    }

    /**
     * Gets the status transitions.
     *
     * @return the statusTransitions
     */
    public HashMap<String, ArrayList<String>> getStatusTransitions() {
        return statusTransitions;
    }

    /**
     * Sets the status transitions.
     *
     * @param statusTransitions            the statusTransitions to set
     */
    public void setStatusTransitions(HashMap<String, ArrayList<String>> statusTransitions) {
        this.statusTransitions = statusTransitions;
    }

    /**
     * Gets the form standard status number.
     *
     * @return the formStandardStatusNumber
     */
    public String getFormStandardStatusNumber() {
        return formStandardStatusNumber;
    }

    /**
     * Sets the form standard status number.
     *
     * @param formStandardStatusNumber            the formStandardStatusNumber to set
     */
    public void setFormStandardStatusNumber(String formStandardStatusNumber) {
        this.formStandardStatusNumber = formStandardStatusNumber;
    }

    /**
     * Check if this application is a Bridgehead of OSSE.
     *
     * @return boolean
     */
    public Boolean isBridgehead() {
        return Utils.isBridgehead(OSSEConfig);
    }

    /**
     * Checks if this app is larger than the given version.
     *
     * @param version            the version to check for
     * @return the boolean
     */
    public Boolean largerThanVersion(String version) {
        if (version == null || version.equals(""))
            return false;

        VersionNumber checkVersion = new VersionNumber(version);
        VersionNumber currentVersion = new VersionNumber(getOSSEVersion());

        if (currentVersion.compareTo(checkVersion) >= 0) {
            return true;
        }
        return false;
    }

    /**
     * Checks if the registry has registered in the Samply.AUTH service.
     *
     * @return the boolean
     */
    public Boolean isInstallationAUTHDone() {
        if (hasCaseForms())
            return true;

        if (!doWeHaveAuthUserId())
            return false;

        AccessTokenDTO accessToken = null;
        try {
            // check if we can get an auth token, if not then we probably have
            // the account not yet activated
            HttpConnector43 hc = new HttpConnector43(getConfig());
            Client client = hc.getJerseyClientForHTTPS(false);
            accessToken = Auth.getAccessToken(client, getConfig());
        } catch (InvalidKeyException | NoSuchAlgorithmException
                | SignatureException | JWTException e) {
            e.printStackTrace();
        } catch (HttpConnectorException e) {
            e.printStackTrace();
        }
        if (accessToken == null)
            return false;

        return true;
    }

    /**
     * Checks if we have the User ID of the Samply.AUTH service saved.
     *
     * @return the boolean
     */
    public Boolean doWeHaveAuthUserId() {
        Value authUserID = getOSSEConfig().getProperty(Vocabulary.Config.Auth.UserId);

        if (authUserID != null && !authUserID.getValue().equals(""))
            return true;

        return false;
    }

    /**
     * Checks if an email address has been saved for the registry.
     *
     * @return the boolean
     */
    public Boolean isInstallationEmailDone() {
        Value email = getOSSEConfig().getProperty(Vocabulary.Config.registryEmail);

        if (email != null && !email.getValue().equals(""))
            return true;

        return false;
    }

    /**
     * Checks if the URL for the form repository has been saved.
     *
     * @return the boolean
     */
    public Boolean isInstallationFormRepositoryDone() {
        Value formRepositoryURL = getOSSEConfig()
                .getProperty(Vocabulary.Config.FormEditor.formEditorBASE);

        if (formRepositoryURL != null
                && !formRepositoryURL.getValue().equals(""))
            return true;

        return false;
    }

    /**
     * Checks if we yet have case forms defined in the registry.
     *
     * @return the boolean
     */
    public Boolean hasForms() {
        HashMap<String, String> formulars = getFormulars().get("en");

        if (formulars == null || formulars.isEmpty()) {
            return false;
        }

        return true;
    }

    /**
     * Does mainzelliste want idat.
     *
     * @return the boolean
     */
    public Boolean doesMainzellisteWantIdat() {
        return !Utils.getBooleanOfJSONResource(OSSEConfig, Vocabulary.Config.Mainzelliste.noIdat);
    }
    
    /**
     * Gets the episode pattern name.
     *
     * @return the episode pattern name
     */
    public String getEpisodePatternName() {
        return Utils.getStringOfJSONResource(OSSEConfig, Vocabulary.Config.Episode.pattern);
    }

    /**
     * Gets the episode pattern upper.
     *
     * @return the episode pattern upper
     */
    public String getEpisodePatternUpper() {
        return EpisodePatterns.patterns.get(getEpisodePatternName()).getPattern().toUpperCase();
    }

    /**
     * Gets the episode pattern label.
     *
     * @return the episode pattern label
     */
    public String getEpisodePatternLabel() {
        return EpisodePatterns.patterns.get(getEpisodePatternName()).getLabel();
    }

    /**
     * Gets the episode pattern.
     *
     * @return the episode pattern
     */
    public String getEpisodePattern() {
        return EpisodePatterns.patterns.get(getEpisodePatternName()).getPattern();
    }

    /**
     * Gets the episode pattern j query.
     *
     * @return the episode pattern j query
     */
    public String getEpisodePatternJQuery() {
        return EpisodePatterns.patterns.get(getEpisodePatternName()).getPatternJQuery();
    }

    /**
     * Checks if is episode date pattern.
     *
     * @return the boolean
     */
    public Boolean isEpisodeDatePattern() {
        return EpisodePatterns.patterns.get(getEpisodePatternName()).getIsDatePattern();
    }

    /**
     * Gets the url to web service.
     *
     * @return the url to web service
     */
    public String getUrlToWebService() {
        return Utils.generateUrlToWebservice();
    }
    
    /**
     * Checks for patient forms.
     *
     * @return the boolean
     */
    public Boolean hasPatientForms() {
        if(patientForms == null || patientForms.isEmpty())
            return false;
        
        return true;
    }
    
    /**
     * Checks for patient user episode form.
     *
     * @return the boolean
     */
    public Boolean hasPatientUserEpisodeForm() {
        if("noform".equals(patientUserEpisodeMainPage))
            return false;
        
        return true;
    }
    
    /**
     * Checks for patient user case form.
     *
     * @return the boolean
     */
    public Boolean hasPatientUserCaseForm() {
        if("noform".equals(patientUserPatientMainPage))
            return false;
        
        return true;
    }
    
    /**
     * Checks if a form name is a patientform or a default form.
     *
     * @param name  full form name in pattern "form_NAME_ver-VERSION" or "NAME-VERSION"
     * @return the boolean
     */
    public Boolean isPatientForm(String name) {
        if(!hasPatientForms())
            return false;
        
        Pattern pattern = Pattern.compile("^form_([^-]+)_ver-([^-]+)$");
        Matcher matcher = pattern.matcher(name);
        if (!matcher.find()) {
            // maybe it is already in the goal pattern NAME-VERSION, so check that
            return patientForms.contains(name);
        }
        
        // convert to NAME-VERSION pattern
        name = matcher.group(1)+"-"+matcher.group(2);
        return patientForms.contains(name);
    }
}
