/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.osse.control;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import com.sun.jersey.api.client.Client;

import de.samply.auth.client.jwt.JWTException;
import de.samply.auth.rest.ClientDescriptionDTO;
import de.samply.auth.rest.OAuth2Key;
import de.samply.auth.rest.OAuth2Keys;
import de.samply.auth.rest.Usertype;
import de.samply.common.http.HttpConnector43;
import de.samply.common.http.HttpConnectorException;
import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.control.AbstractViewBean;
import de.samply.edc.osse.auth.Auth;
import de.samply.edc.osse.catalog.EpisodePatterns;
import de.samply.edc.osse.utils.OSSEUtils;
import de.samply.edc.osse.validator.FAIRValidator;
import de.samply.edc.utils.Utils;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.Value;

import java.text.*;
import java.util.Calendar;
/**
 * Session scoped bean for configuration pages.
 */
@ManagedBean
@ViewScoped
public class ConfigurationBean extends AbstractViewBean {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** Is the URL to the Samply.AUTH service ok? */
    private Boolean authURLisOk = false;

    /** Map of all clients known to the Samply.AUTH service */
    private HashMap<String, ClientDescriptionDTO> allClients;

    /**
     * A SelectItem list of all form repositories defined in Samply.AUTH service
     */
    private List<SelectItem> formRepositorySelectItems;

    /** A SelectItem list of all MDRs defined in the Samply.AUTH service */
    private List<SelectItem> mdrSelectItems;

    /**
     * Postconstruct init, loads the current configuration.
     */
    @Override
    public void init() {
        super.init();
        loadConfiguration();
    }

    /**
     * reads the configuration from the application scoped bean and puts it into
     * the form param map.
     */
    private void loadConfiguration() {
        JSONResource osseConfig = ((ApplicationBean) Utils.getAB())
                .getOSSEConfig();
        for (String key : osseConfig.getDefinedProperties()) {
            if (!(osseConfig.getProperty(key) instanceof JSONResource))
                setFormParam(key, osseConfig.getProperty(key).getValue());
        }

        String authREST = osseConfig.getProperty(Vocabulary.Config.Auth.REST)
                .getValue();
        if (authREST != null && !"".equals(authREST))
            authURLisOk = true;
        else
            authURLisOk = false;
    }

    /**
     * JSF action method of the admin registry page.
     *
     * @return the string
     */
    public String saveRegister() {
        saveConfiguration(false);
        if (((ApplicationBean) Utils.getAB()).isInstallationEmailDone()) {
            registerAuth();
        }

        return "";
    }

    /**
     * Saves the form param to the configuration.
     *
     * @return the string
     */
    public String saveConfiguration() {
        return saveConfiguration(true);
    }

    /**
     * Save the form param to the configuration.
     *
     * @param showHappy
     *            display a save successful message
     * @return JSF outcome string (unused)
     */
    public String saveConfiguration(Boolean showHappy) {
        JSONResource osseConfig = ((ApplicationBean) Utils.getAB())
                .getOSSEConfig();

        for (String key : getFormParam().keySet()) {
            if (getFormParam(key) == null) {
                // never store null!
            } else if (osseConfig.getProperty(key) == null) {
                osseConfig.setProperty(key, getFormParam(key));
            } else if (!osseConfig.getProperty(key).getValue()
                    .equalsIgnoreCase(getFormParam(key))) {
                osseConfig.setProperty(key, getFormParam(key));
            }
        }

        // save the osse config, and reload it in the application scoped bean
        getSessionBean().getDatabase().saveConfig("osse", osseConfig);
        Utils.getAB().init();
        loadConfiguration();

        if (showHappy)
            Utils.addContextMessage("Saving successful",
                    "Your changes have been saved.");
        return "";
    }

    /**
     * Gets the name of the current form.
     *
     * @return the form
     */
    public String getForm() {
        if (Utils.getSB().getCurrentFormName() == null
                || Utils.getSB().getCurrentFormName().equals("")) {

            if (((ApplicationBean) Utils.getAB()).isInstallationEmailDone()) {
                if (((ApplicationBean) Utils.getAB()).isInstallationAUTHDone()) {
                    if (!((ApplicationBean) Utils.getAB()).isBridgehead()) {
                        Utils.getSB().setForm("forms");
                    }
                }
            }

            else Utils.getSB().setForm("register");
        }

        return Utils.getSB().getCurrentFormName();
    }

    /**
     * Cancel button action.
     *
     * @return JSF outcome string
     */
    public String cancel() {
        Utils.goAdminForm(getForm());
        Utils.addContextMessage("Changes reverted",
                "All your changes have been reverted to the current state.");
        return "cancel";
    }

    /**
     * Checks if a certain form is the currently active form. Used to call the
     * css class in the xhtml
     *
     * @param formName
     *            the form name
     * @return active|""
     */
    public String isActiveForm(String formName) {
        String currentFormName = Utils.getSB().getForm();

        if (currentFormName != null
                && currentFormName.equalsIgnoreCase(formName)) {
            return "active";
        }

        if (currentFormName == null && formName.equalsIgnoreCase("register"))
            return "active";

        return "";
    }

    /**
     * Map of all clients known to the Samply.AUTH service
     *
     * @return the all clients
     */
    public HashMap<String, ClientDescriptionDTO> getAllClients() {
        if (allClients == null) {
            try {
                HttpConnector43 hc = new HttpConnector43(Utils.getAB().getConfig());
                Client client = hc.getJerseyClientForHTTPS(false);
                allClients = Auth.loadAllClients(client, Utils.getAB().getConfig());
            } catch (HttpConnectorException e) {
                e.printStackTrace();
                allClients = new HashMap<>();
            }
        }

        return allClients;
    }

    /**
     * Gets the form repository SelectItem list.
     *
     * @return the form repository select items
     */
    public List<SelectItem> getFormRepositorySelectItems() {
        if (formRepositorySelectItems == null) {
            formRepositorySelectItems = new ArrayList<SelectItem>();

            SelectItem item;

            for (String clientID : getAllClients().keySet()) {
                ClientDescriptionDTO clientDDTO = getAllClients().get(clientID);

                // TODO: Check for type FormRepository, yet Samply.AUTH does not
                // have client-Types
                String redirectURL = clientDDTO.getRedirectUrl();
                redirectURL = Utils.removeFinalTrailFromURL(redirectURL);

                item = new SelectItem();
                item.setLabel(redirectURL);
                item.setValue(redirectURL);
                formRepositorySelectItems.add(item);
            }
        }
        return formRepositorySelectItems;
    }

    /**
     * Gets the mdr SelectItem list.
     *
     * @return the mdr select items
     */
    public List<SelectItem> getMdrSelectItems() {
        if (mdrSelectItems == null) {
            mdrSelectItems = new ArrayList<SelectItem>();

            SelectItem item;

            for (String clientID : getAllClients().keySet()) {
                ClientDescriptionDTO clientDDTO = getAllClients().get(clientID);

                // TODO: Check for type MDR, yet Samply.AUTH does not have
                // client-types
                item = new SelectItem();
                item.setLabel(clientDDTO.getRedirectUrl());
                item.setValue(clientDDTO.getRedirectUrl());
                mdrSelectItems.add(item);
            }
        }
        return mdrSelectItems;
    }

    /**
     * Saves the FormRepository settings.
     */
    public void saveFR() {
        saveConfiguration();
        Utils.refreshView();
    }

    /**
     * Checks if a Samply.AUTH URL actually points to a Samply.AUTH service, and
     * saves the value.
     */
    public void checkAuthURL() {
        String url = getFormParam(Vocabulary.Config.Auth.REST);

        if (url == null || "".equals(url))
            return;

        try {
            HttpConnector43 hc = new HttpConnector43(Utils.getAB().getConfig());
            Client client = hc.getJerseyClientForHTTPS(false);
            OAuth2Keys keys = client.resource(url + "/oauth2/certs").get(
                    OAuth2Keys.class);

            if (keys != null && keys.getKeys() != null
                    && !keys.getKeys().isEmpty()) {
                OAuth2Key key = keys.getKeys().get(0);
                authURLisOk = true;

                String oldEntry = getFormParam(Vocabulary.Config.Auth.ServerPubKey);

                if (oldEntry == null || "".equals(oldEntry)) {
                    // there was no AUTH registered yet
                    saveConfiguration();
                } else {
                    if (oldEntry.equalsIgnoreCase(key.getDerFormat())) {
                        // same entry, just save it in case the AUTH URL changed
                        saveConfiguration();
                    } else {
                        // new AUTH, delete user ID, key ID, and serverpubkey,
                        // but keep own priv/pub key
                        setFormParam(Vocabulary.Config.Auth.UserId, "");
                        setFormParam(Vocabulary.Config.Auth.KeyId, "");
                        setFormParam(Vocabulary.Config.Auth.ServerPubKey, "");
                        saveConfiguration();
                    }
                }
            } else {
                authURLisOk = false;
            }
        } catch (Exception e) {
            authURLisOk = false;
            e.printStackTrace();
        }
    }

    /**
     * Was the authURL correct and saved?.
     *
     * @return the boolean
     */
    public Boolean isAuthURLcorrect() {
        return authURLisOk;
    }

    /**
     * Registers the registry to the Samply.AUTH service.
     */
    public void registerAuth() {
        JSONResource OSSEConfig = ((ApplicationBean) Utils.getAB())
                .getOSSEConfig();

        HashMap<String, String> authstuff = new HashMap<>();
        String pubkey = null;

        if (OSSEConfig.getProperty(Vocabulary.Config.Auth.MyPubKey) != null)
            pubkey = OSSEConfig.getProperty(Vocabulary.Config.Auth.MyPubKey)
                    .getValue();
        if (pubkey != null && !"".equalsIgnoreCase(pubkey)) {
            authstuff.put(Vocabulary.Config.Auth.MyPubKey, pubkey);
        } else {
            try {
                authstuff = Auth.generateKeyPair();
                pubkey = authstuff.get(Vocabulary.Config.Auth.MyPubKey);
                OSSEConfig.setProperty(Vocabulary.Config.Auth.MyPrivKey,
                        authstuff.get(Vocabulary.Config.Auth.MyPrivKey));
                OSSEConfig.setProperty(Vocabulary.Config.Auth.MyPubKey, pubkey);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }

        String email = OSSEConfig.getProperty(Vocabulary.Config.registryEmail)
                .getValue();
        String registryName = OSSEConfig.getProperty(
                Vocabulary.Config.registryName).getValue();

        if (((ApplicationBean) Utils.getAB()).isBridgehead())
            authstuff = Auth.registerAuth(pubkey, email, registryName, Utils.getAB().getConfig(), Usertype.BRIDGEHEAD);
        else
            authstuff = Auth.registerAuth(pubkey, email, registryName, Utils.getAB().getConfig(),
                    Usertype.OSSE_REGISTRY);

        if (authstuff == null) {
            Utils.addContextMessage(
                    "Authentication registration failed",
                    "Something went wrong in registring your OSSE to the authentication service.",
                    FacesMessage.SEVERITY_FATAL);
            return;
        }

        if (authstuff.containsKey("hostunreachable")) {
            Utils.addContextMessage(
                    "AUTH Host could not be reached",
                    "The AUTH service could not be reached. This could be caused if your server has no access to the internet or if the proxy settings are incorrect.",
                    FacesMessage.SEVERITY_ERROR);
            return;
        }

        if (authstuff.containsKey("conflict")) {
            Utils.addContextMessage(
                    "Email already in use",
                    "The email you provided is already in use and cannot be used to create a new registry. Please change to the Registry setting and enter a different email adress.",
                    FacesMessage.SEVERITY_ERROR);
            return;
        }

        OSSEConfig.setProperty(Vocabulary.Config.Auth.KeyId,
                authstuff.get(Vocabulary.Config.Auth.KeyId));
        OSSEConfig.setProperty(Vocabulary.Config.Auth.UserId,
                authstuff.get(Vocabulary.Config.Auth.UserId));
        OSSEConfig.setProperty(Vocabulary.Config.Auth.ServerPubKey,
                authstuff.get(Vocabulary.Config.Auth.ServerPubKey));

        getSessionBean().getDatabase().saveConfig("osse", OSSEConfig);
        getSessionBean().getDatabase().saveConfig("osse.factory.settings", OSSEConfig);
        Utils.getAB().init();
        loadConfiguration();

        Utils.addContextMessage("Authentication registration successful",
                "Your account has been created in the authentication service.");

        try {
            OSSEUtils.mdrFacesClientStuff();
        } catch (InvalidKeyException | NoSuchAlgorithmException
                | SignatureException | JWTException e) {
            e.printStackTrace();
        }
    }

    /**
     * Do we have locations in the registry?.
     *
     * @return the boolean
     */
    public Boolean hasLocations() {
        List<Resource> resultLocations = ((Database) getSessionBean().getDatabase()).getLocations();

        return !resultLocations.isEmpty();
    }

    /**
     * Gets the episode pattern possibilities.
     *
     * @return the episode pattern possibilities
     */
    public List<SelectItem> getEpisodePatternPossibilities() {
        List<SelectItem> locationsSelectItems = new ArrayList<>();

        SelectItem item;

        for (String patternName : EpisodePatterns.patterns.keySet()) {
            item = new SelectItem();

            item.setLabel(EpisodePatterns.patterns.get(patternName).getLabel()+" : "+EpisodePatterns.patterns.get(patternName).getPattern());
            item.setValue(patternName);
            locationsSelectItems.add(item);
        }

        return locationsSelectItems;
    }

    public void CheckFAIRData(){


        JSONResource OsseConfig = ((ApplicationBean) Utils.getAB())
                .getOSSEConfig();

        for (String key : getFormParam().keySet()) {
            if (getFormParam(key) == null) {
                // never store null!
                //test
            } else if (OsseConfig.getProperty(key) == null) {
                OsseConfig.setProperty(key, getFormParam(key));
            } else if (!OsseConfig.getProperty(key).getValue()
                    .equalsIgnoreCase(getFormParam(key))){
                OsseConfig.setProperty(key, getFormParam(key));
            }
        }

        //teste, ob Issued gefüllt ist, wenn nicht, dann ändere es

        String timeStamp = new SimpleDateFormat("yyyy-MM-dd'T' HH:mm:ss").format(Calendar.getInstance().getTime());
        Value property = OsseConfig.getProperty("fair.fdpmetadata.issued");
        if(property == null){


            OsseConfig.setProperty("fair.fdpmetadata.issued",timeStamp);
            OsseConfig.setProperty("fair.catalog.issued",timeStamp);
            OsseConfig.setProperty("fair.dataset.issued",timeStamp);
            OsseConfig.setProperty("fair.distribution.issued",timeStamp);
       }


        OsseConfig.setProperty("fair.fdpmetadata.modified",timeStamp);
        OsseConfig.setProperty("fair.catalog.modified",timeStamp);
        OsseConfig.setProperty("fair.dataset.modified",timeStamp);
        OsseConfig.setProperty("fair.distribution.modified",timeStamp);

        FAIRValidator fairValidator = new FAIRValidator();
        boolean testFDP = fairValidator.CheckFDPMetaData(OsseConfig);
        boolean testCatalog = fairValidator.CheckCatalogMetaData(OsseConfig);
        boolean testDataset = fairValidator.CheckDataSetMetaData(OsseConfig);
        boolean testDistribution = fairValidator.CheckDistributionMetaData(OsseConfig);

        if(testFDP == false || testCatalog ==false || testDataset ==false || testDistribution ==false ){

            Utils.addContextMessage("Saving failed, please fill out the necassary fields", "Your changes have not been saved",FacesMessage.SEVERITY_ERROR);

        }

        else{

            saveFairConfiguration(OsseConfig,true);
        }


    }

    /***
     * Need extra method for Save FAIR Configuration
     * @param osseConfig
     * @param showHappy
     * @return
     */

    public String saveFairConfiguration(JSONResource osseConfig, boolean showHappy) {


        // save the osse config, and reload it in the application scoped bean
        getSessionBean().getDatabase().saveConfig("osse", osseConfig);
        Utils.getAB().init();
        loadConfiguration();

        if (showHappy)
            Utils.addContextMessage("Saving successful",
                    "Your changes have been saved.");
        return "";
    }

}
