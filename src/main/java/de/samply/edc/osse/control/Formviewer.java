/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.osse.control;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.control.AbstractViewBean;
import de.samply.edc.exceptions.FormException;
import de.samply.edc.model.ContextMenuEntry;
import de.samply.edc.model.DatatableRow;
import de.samply.edc.model.DatatableRows;
import de.samply.edc.model.Entity;
import de.samply.edc.osse.dto.formeditor.FormDetails;
import de.samply.edc.osse.model.*;
import de.samply.edc.osse.utils.FormUtils;
import de.samply.edc.utils.Utils;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.Value;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.osse.OSSEVocabulary;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;
import java.util.Map.Entry;

//import org.primefaces.model.chart.CartesianChartModel;
//import org.primefaces.model.chart.LineChartSeries;

/**
 * the viewscoped bean formviewer, managing forms.
 */
@ManagedBean(name = "formviewer")
@ViewScoped
public class Formviewer extends AbstractViewBean {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**  Is the current form an episode form?. */
    private Boolean isEpisodeForm = false;

    /**  The form entries/data. */
    private MyMap<String, Object> entries;

    /** The form we're looking at. */
    private Form form = null;

    /** The visit we're in. */
    private Episode episode = null;

    /** The case we're in. */
    private Case currentCase = null;

    /** If all forms are validated it counts as if the visit was validated. */
    private Boolean episodeIsValidated = false;

    /** If all forms are reported it counts as if the visit was reported. */
    private Boolean episodeIsReported = false;

    /**  Linear model used to display form data as a chart. */
    // private CartesianChartModel linearModel;

    /** defines the new status on a status change */
    private String newStatus;

    /**  a selectitem list of accessible new status. */
    private List<SelectItem> possibleNewStatus;

    /**  A list of comments. */
    private TreeSet<FormComment> formComments;

    /**
     * Postconstruct init. If the current form has a name, try to load its
     * values.
     *
     * @see AbstractViewBean#init()
     */
    @Override
    public void init() {
        // If we got an episode selected, we are trying to call an episode form
        isEpisodeForm = (((SessionBean) getSessionBean()).getCurrentEpisode() != null);
        
        super.init();

        // And load our form
        if (getSessionBean().getCurrentFormName() != null) {
            loadValues();
        }
    }

    /**
     * Method to init the form and load the data.
     */
    private void loadValues() {
        // no patient selected, so this cannot be a medical form, bail out
        if (((SessionBean) getSessionBean()).getCurrentPatient() == null) {
            return;
        }

        // no medical form, bail out
        if (!((SessionBean) getSessionBean()).isCurrentFormMedical())
            return;

        // Init the form in case we didn't do it yet
        if (form == null) {
            initializeForm(false);

            if (form == null) {
                // init form failed or no medical form is displayed, bail out
                return;
            }

            form.load();

            // prepare comments
            formComments = new TreeSet<FormComment>();
            if (form.hasProperty(Vocabulary.Form.formComments)) {
                JSONResource commentsResource = (JSONResource) form.getProperty(Vocabulary.Form.formComments);

                List<Value> allComments = commentsResource.getProperties(Vocabulary.Form.formComment);
                for (Value aComment : allComments) {
                    FormComment fc = new FormComment((JSONResource) aComment);
                    formComments.add(fc);
                }
            }

            entries = new MyMap<String, Object>(form);
            if (episode != null) {
                episodeIsValidated = episode.allFormsAreValidated();
                episodeIsReported = episode.allFormsAreReported();
            } else {
                episodeIsValidated = false;
                episodeIsReported = false;
            }

            newStatus = form.getFormState();
            preparePossibleStatusList();
        }
    }

    /**
     * Prepare possible status list.
     */
    private void preparePossibleStatusList() {
        possibleNewStatus = new ArrayList<SelectItem>();
        SelectItem item = null;

        if (form == null || form.getFormState().equalsIgnoreCase("open")) {
            item = new SelectItem();
            item.setLabel("open");
            item.setValue("open");

            possibleNewStatus.add(item);

            item = new SelectItem();
            item.setLabel("reported");
            item.setValue("reported");

            possibleNewStatus.add(item);

        } else if (form.getFormState().equalsIgnoreCase("reported")) {
            item = new SelectItem();
            item.setLabel("reported");
            item.setValue("reported");

            possibleNewStatus.add(item);

            item = new SelectItem();
            item.setLabel("open");
            item.setValue("open");

            possibleNewStatus.add(item);

            item = new SelectItem();
            item.setLabel("validated");
            item.setValue("validated");

            possibleNewStatus.add(item);
        } else if (form.getFormState().equalsIgnoreCase("validated")) {
            item = new SelectItem();
            item.setLabel("validated");
            item.setValue("validated");

            possibleNewStatus.add(item);

            item = new SelectItem();
            item.setLabel("open");
            item.setValue("open");

            possibleNewStatus.add(item);
        }

    }

    /*
    * Gets the previous episode based on episode`s name.
    *
    *  @return the last Episode
    */
    private Episode getTheLastEpisodeByName() {

        String errorNoData = Utils.getResourceBundleString("error_formimportnodata");
        String summary = Utils.getResourceBundleString("summary_importfailed");

        // Get the previous episode (based on the name), we use a treemap for this
        TreeMap<Long, Episode> myEpisodes = new TreeMap<Long, Episode>(Collections.reverseOrder());

        // reload the current case and episode data, in case meanwhile someone else changed values
        if (!((SessionBean) getSessionBean()).reinitCaseAndEpisode()) {
            System.err.println("FV.importLastYearData: sb.reinitCaseAndEpisode returned false.");
            return null;
        }

        currentCase = (Case) ((SessionBean) getSessionBean()).getCurrentCase();

        // fill the tree
        for (Entity theEpisode : currentCase.getChildren(OSSEVocabulary.Type.Episode)) {

            Date theDate = Episode.getDateByPattern(((Episode) theEpisode).getName(), null);
            if (theDate != null) {
                Long compareKey = theDate.getTime();
                myEpisodes.put(compareKey, (Episode) theEpisode);
            }
        }

        Entry<Long, Episode> theLast = null;
        Date myDate = Episode
                .getDateByPattern(((Episode) ((SessionBean) getSessionBean()).getCurrentEpisode()).getName(), null);
        if (myDate != null) {
            theLast = myEpisodes.higherEntry(myDate.getTime());
        }

        Episode theLastEpisode = null;

        // If there are no data to be imported go away
        if (theLast == null) {
            return null;
        } else {
           return theLastEpisode = theLast.getValue();
        }
    }

    /**
     * Imports the data of a/all previous episode's form(s) (historically called last
     * year) that is/are not in a "reported"- or "validated"-State.
     *
     * For now we say that data may not be imported into forms that are not "open"
     *
     *@param allOpenForms: if true, all forms (if false,just the current form )
     *                     of the current episode will be overwritten.
     *
     * @return JSF outcome
     */
    public String importLastYearData(Boolean allOpenForms) {
        Boolean imported = false;

        String summary_failed= Utils.getResourceBundleString("summary_importfailed");
        String summary_succeeded= Utils.getResourceBundleString("summary_importsuccess");
        String errorNotImported = Utils.getResourceBundleString("error_noimport");
        String successImport = Utils.getResourceBundleString("success_import");
        String errorNotOpen = Utils.getResourceBundleString("error_formnotopen");
        String errorNoData = Utils.getResourceBundleString("error_formimportnodata");

        if (episode == null) {
            throw new Error("formviewer.importLastYearData: " +
                             "Cannot proceed without an episode");
        }

        // 1) Get the previous episode (based on it's name)
        Episode theLastEpisode = getTheLastEpisodeByName();
        if (theLastEpisode == null) {
            Utils.addContextMessage(summary_failed, errorNoData);
            return "";
        }

        // 2) Get the previous form
        theLastEpisode.reloadResource();
        theLastEpisode.load();
        theLastEpisode.loadEpisodeForms();

        // just the data of the current Form should to be imported
        if (!allOpenForms) {
            if (form == null) {
                throw new Error("formviewer.importLastYearData: Cannot proceed without a form");
            }

            // if the current Form isn't in "open"-State inform the user; return nothing
            if (!form.isOpen()) {
                Utils.addContextMessage(summary_failed, errorNotOpen);
                return "";
            }

            Form theLastForm = theLastEpisode.getForm(getSessionBean().getCurrentFormName());
            if (theLastForm == null) {
                theLastForm = findLowerVersionForm(getSessionBean().getCurrentFormName(), false, null, theLastEpisode);
            }

            // if the last Form doesn't exist inform the user; return nothing
            if (theLastForm == null) {
                Utils.addContextMessage(summary_failed, errorNoData);
                return "";
            }

            //overwrite all data values of the current Form
            form.importPreviousFormData((EpisodeForm) theLastForm);

            //inform the user that the import was successful
            Utils.addContextMessage(summary_succeeded, successImport);

            // point the entries to the newly filled form data
            entries = new MyMap<String, Object>(form);

            Resource statusRes = Utils.findResourceByProperty(
                    OSSEVocabulary.Type.Status, OSSEVocabulary.ID, "1");
            form.setProperty(OSSEVocabulary.EpisodeForm.Status, statusRes);

            getSessionBean().addLog("imported last version for form " + form.getFormID() + " (visit " + episode.getName()
                    + ") from " + theLastForm.getFormID() + " (visit " + theLastEpisode.getName() + ") of patient "
                    + ((SessionBean) getSessionBean()).getCurrentPatient().getId());

            save(true);
        }

        if (allOpenForms) {
            //current Form's name, needed to redireckt
            String currentForm = getSessionBean().getCurrentFormName();

            //load current episode's Forms
            episode.loadEpisodeForms();

            // get Forms of the last episode
            List<Entity> lastEpisodeForms = theLastEpisode.getForms();

            // if there are no Forms of the last Episode inform the user; return nothing
            if (lastEpisodeForms == null || lastEpisodeForms.isEmpty()) {
                Utils.addContextMessage(summary_failed, errorNoData);
                return "";
            }

            // import the Forms of the last Episode that are in "open"-State
            episode.importALLPreviousEpisodeForms(lastEpisodeForms);

            //inform the user that the import was successful
            Utils.addContextMessage(summary_succeeded, successImport);

            getSessionBean().addLog("imported last versions of open forms of the " +
                    " (visit " + episode.getName() + ") from last " +
                    " (visit " + theLastEpisode.getName() + ") of patient "
                    + ((SessionBean) getSessionBean()).getCurrentPatient().getId());

            episode.load();
            episode.loadEpisodeForms();

            //redirect to the current form
            form = episode.getForm(currentForm);
            entries = new MyMap<String, Object>(form);
        }
        return null;
    }

    /**
     * Change to new status.
     *
     * @return the string
     */
    public String changeToNewStatus() {
        if (newStatus == null || "".equals(newStatus))
            return "";

        if (form.getFormState().equalsIgnoreCase(newStatus)) {
            if (entries.get("statusChangeComment") != null || !entries.get("statusChangeComment").equals("")) {
                Utils.addContextMessage(Utils.getResourceBundleString("osse_messages", "no_status_change_subject"),
                        Utils.getResourceBundleString("osse_messages", "no_change_no_comment"));
            }
            return "";
        }
        return changeStatus(newStatus);
    }

    /**
     * Action method: tries to change the status of a form.
     *
     * @param newStatus
     *            the new status
     * @return JSF outcome string
     */
    public String changeStatus(String newStatus) {
        if (form == null) {
            Utils.addContextMessage(Utils.getResourceBundleString("summary_reportfailed"),
                    Utils.getResourceBundleString("error_general_noformorvisit"));
            throw new Error("formviewer.reportform: Cancling reportform due to no form or visit");
        }

        String oldStatus = form.getFormState();
        try {
            form.setFormState(newStatus);
        } catch (FormException | DatabaseException e) {
            loadValues();
            // TODO: debug only
            e.printStackTrace();
            Utils.addContextMessage("Changing of status failed", "The state of this form could not be changed.");

            return form.getFormState();
        }

        // save comment
        if (entries.get("statusChangeComment") != null && !"".equals(entries.get("statusChangeComment"))) {
            String comment = (String) entries.get("statusChangeComment");

            FormComment fc = new FormComment(comment,
                    ((SessionBean) getSessionBean()).getCurrentUser().getResourceURI(), new Date().getTime());

            JSONResource commentsResource = null;
            if (form.hasProperty(Vocabulary.Form.formComments)) {
                commentsResource = (JSONResource) form.getProperty(Vocabulary.Form.formComments);
            } else {
                commentsResource = new JSONResource();
            }

            commentsResource.addProperty(Vocabulary.Form.formComment, fc.toJSONResource());

            form.setProperty(Vocabulary.Form.formComments, commentsResource);
            form.getResource().setProperty(Vocabulary.Form.formComments, commentsResource);
            form.getDatabase().save(form.getResource());
        }

        Object[] params = { oldStatus, newStatus };
        String text = Utils.getResourceBundleString("general_statuschange_text", params);

        Utils.addContextMessage(Utils.getResourceBundleString("general_statuschange_successful"), text);

        redirectToForm(sessionBean.getCurrentFormName(), isEpisodeForm);
        
        return newStatus;
    }

    /**
     * Saves the form values to the DB.
     *
     * @param wasImported the was imported
     * @return the string
     * @see AbstractViewBean#save(Boolean)
     */
    @Override
    public String save(Boolean wasImported) {
        String summaryFailed = Utils.getResourceBundleString("summary_savefailed");

        if (form == null) {
            Utils.addContextMessage(summaryFailed, Utils.getResourceBundleString("error_general_noformorvisit"));
            return null;
        }

        // forms that are not open may not be saved
        // TODO: Future workflow generic definitions may need to change this!
        if (!form.isOpen()) {
            if (form.isReported())
                Utils.addContextMessage(summaryFailed, Utils.getResourceBundleString("formstate_reported"));
            else
                Utils.addContextMessage(summaryFailed, Utils.getResourceBundleString("formstate_signed"));

            return null;
        }

        form.getDatabase().beginTransaction();
        form.saveOrUpdate();
        form.getDatabase().commit();

        // update the last Changed values (who and when)
        setLastChanged();

        /** after "import from last visit" the form version might be conflicting,
         *  must be set to the version of the currently displayed form.
         */
       Integer formVersionData = (Integer) Utils.getFormVersionData(getSessionBean().getCurrentFormName())
                .get("version");
        if (!form.getVersion().equals(formVersionData.toString())) {
            Utils.getLogger().debug("Formversion does not match: It was " + form.getVersion() + " it is supposed to be "
                    + formVersionData + " setting it now!");
            form.setVersion(formVersionData);
        }

        // reload the form data, in case there were transformations or
        // conversions done
        form.load();

        // update the form in the parent
        if (episode == null)
            currentCase.replaceChild(OSSEVocabulary.Type.CaseForm, form);
        else
            episode.replaceChild(OSSEVocabulary.Type.EpisodeForm, form);

        // say Yay!
        Utils.addContextMessage(Utils.getResourceBundleString("summary_savesuccess"),
                Utils.getResourceBundleString("success_save"));

        // logbook
        getSessionBean().addLog("saved form " + form.getFormID() + " (visit " + episode + ") of patient "
                + ((SessionBean) getSessionBean()).getCurrentPatient().getId());

        // reload the formslist for navigation, in case status etc changed due
        // to the saveing
        ((SessionBean) getSessionBean()).loadNavigationPatientForms();

        // clear linearModel so it gets redrawn
        // TODO: This is not yet used, it's for future support of forms that may
        // have graphical charts based on form data (e.g. blood pressure chart
        // or similar)
        // linearModel = null;

        return null;
    }

    /**
     * Sets when and by who a form was changed. Also sets this in the
     * patient-context.
     */
    private void setLastChanged() {
        form.storeLastChange();

        // TODO: This has a backend permission rights problem, if the user has
        // no right to write patient-data, and only may
        // write form data of a patient. So disabled this for now
        // getSessionBean().getCurrentPatient().storeLastChange();
    }

    /**
     * Tries to find a lower version of a form within the case (or episode).
     *
     * @param currentFormName            The form we want to find
     * @param isCaseForm            Is it a case form or an episode form?
     * @param myCase the my case
     * @param myEpisode the my episode
     * @return the form
     */
    private Form findLowerVersionForm(String currentFormName, Boolean isCaseForm, Case myCase, Episode myEpisode) {
        HashMap<String, Object> formVersionData = Utils.getFormVersionData(currentFormName);
        Integer formVersion = null;
        String formName = null;

        if (formVersionData != null && formVersionData.get("version") != null && formVersionData.get("name") != null) {
            formVersion = (Integer) formVersionData.get("version");
            formName = (String) formVersionData.get("name");
        }

        if (formVersion != null && formVersion > 1) {
            while (formVersion > 1) {
                formVersion--;
                String toFindForm = formName + "-" + formVersion;

                if (isCaseForm)
                    form = myCase.getForm(toFindForm);
                else
                    form = myEpisode.getForm(toFindForm);

                if (form != null)
                    return form;
            }
        }

        return null;
    }

    /**
     * Initializes the necessary variables. Makes sure we got a case, an episode
     * (if we handle an episodeForm) and a form object. Without any of these, no
     * operation can work.
     *
     * @param wasImported
     *            the was imported
     */
    public void initializeForm(Boolean wasImported) {
        if (!wasImported) {
            // refresh case and visit
            if (!((SessionBean) getSessionBean()).reinitCaseAndEpisode()) {
                System.err.println("FV.initForm: reinitCaseAndVisit returned with error, bailing out");
                episode = null;
                form = null;
                return;
            }
        }

        String currentFormName = getSessionBean().getCurrentFormName();
        if (currentFormName == null || currentFormName.equals("")) {
            return;
        }

        // Get our current case
        currentCase = (Case) ((SessionBean) getSessionBean()).getCurrentCase();
        episode = ((SessionBean) getSessionBean()).getCurrentEpisode();

        if (currentCase == null) {
            return;
        }

        if (!isEpisodeForm || currentFormName.equals(Vocabulary.Development.testFormName)) {
            // caseform or our test form called "test-1", find it, and load it

            if (currentCase.hasChildren(OSSEVocabulary.Type.CaseForm)) {
                form = currentCase.getForm(currentFormName);

                if (form == null) {
                    // We found no form. First check if we got an older version
                    // of the form saved
                    form = findLowerVersionForm(currentFormName, true, currentCase, episode);
                    if (form != null) {
                        // overload the current form setting
                        redirectToForm(form.getFormID(), false);
                    }

                }
            }

            if (form == null) {
                // the form is not yet in the data of the case, so we have to
                // create a new form (if we're allowed to do so)

                if (!((SessionBean) Utils.getSB()).mayWriteCurrentForm()) {
                    return;
                }

                form = new CaseForm(getSessionBean().getDatabase());
                form.setParent(currentCase);
                form.createForm(currentFormName, ((SessionBean) getSessionBean()).getFormVersion());
            }
        } else if (isEpisodeForm) {
            // episode form, find it and load it
            if (currentCase.hasChildren(OSSEVocabulary.Type.Episode)) {
                form = episode.getForm(currentFormName);

                if (form == null) {
                    // We found no form. First check if we got an older version
                    // of the form saved
                    form = findLowerVersionForm(currentFormName, false, currentCase, episode);
                    if (form != null) {
                        // overload the current form setting
                        redirectToForm(form.getFormID(), true);
                    }

                }
            }

            if (form == null) {
                // this form yet does not exist, so we create a new one if we
                // are allowed to

                if (!((SessionBean) Utils.getSB()).mayWriteCurrentForm()) {
                    return;
                }

                form = new EpisodeForm(getSessionBean().getDatabase());
                form.setParent(episode);
                form.createForm(getSessionBean().getCurrentFormName(),
                        ((SessionBean) getSessionBean()).getFormVersion());
            }
        }
        return;
    }

    /**
     * This redirects to a form in its older version, if it was saved as such.
     * So if we got a form "foobar-3", but we stored data in its version
     * "foobar-1", we redirect the user to the "foobar-1" form version instead
     *
     * @param newForm
     *            the new form we want the user to be redirected to
     */
    public void redirectToForm(String newForm) {
        redirectToForm(newForm, false);
    }

    /**
     * This redirects to a form in its older version, if it was saved as such.
     * So if we got a form "foobar-3", but we stored data in its version
     * "foobar-1", we redirect the user to the "foobar-1" form version instead
     *
     * @param newForm
     *            the new form we want the user to be redirected to
     * @param isEpisodeForm
     *            if the form is an episode form
     */
    public void redirectToForm(String newForm, Boolean isEpisodeForm) {
        String oldForm = getSessionBean().getCurrentFormName();
        getSessionBean().setCurrentFormName(newForm);

        String indexEpisode = "index.xhtml";
        if (isEpisodeForm)
            indexEpisode = "episode.xhtml";

        String page = indexEpisode + "?form=" + newForm;
        if(!newForm.equalsIgnoreCase(oldForm))
            page += "&oldform=" + oldForm;
        
        try {
            Utils.redirectToPage(page);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Faces method to load a form by given params. Prepared for sign form
     * display quicklink, not yet used in OSSE.
     *
     * @param patient
     *            PID of Patient
     * @param caseURI
     *            the case backend URI
     * @param episodeName
     *            Label of the visit
     * @param formName
     *            Name of the form
     * @return JSF outcome
     */
    public String showform(String patient, String caseURI, String episodeName, String formName) {
        Patient thePatient = new Patient(getSessionBean().getDatabase(), patient);
        thePatient.load();
        thePatient.loadChildren(OSSEVocabulary.Type.Case, true);

        Case theCase = new Case(getSessionBean().getDatabase(), caseURI);

        theCase.loadChildren(OSSEVocabulary.Type.Episode, false);
        theCase.loadChildren(OSSEVocabulary.Type.CaseForm, false);

        Episode theEpisode = null;
        Form theForm = null;

        if (episodeName == null || "".equals(episodeName)) {
            theForm = theCase.getForm(formName);
        } else {
            theEpisode = (Episode) theCase.getChild(OSSEVocabulary.Type.Episode, episodeName);

            if (theEpisode == null) {
                return "";
            }

            theEpisode.loadEpisodeForms();
            theForm = theEpisode.getForm(formName);
        }

        if (theForm == null) {
            return "";
        }

        ((SessionBean) getSessionBean()).setCurrentCase(theCase);
        ((SessionBean) getSessionBean()).setCurrentEpisode(theEpisode);
        ((SessionBean) getSessionBean()).setCurrentPatient(thePatient);
        getSessionBean().setCurrentFormName(formName);
        ((SessionBean) getSessionBean()).loadNavigationPatientForms();
        form = theForm;
        episode = theEpisode;

        loadValues();
        return "showform";
    }

    /**
     * Returns an Integer array from start year to the current year.
     *
     * @param start
     *            Integer start year
     * @return Array from start year to current year
     */
    public Integer[] selectItemYears(Integer start) {
        Calendar calendar = Calendar.getInstance();
        return Utils.createIntegerArray(start, calendar.get(Calendar.YEAR));
    }

    /**
     * Gets the last changed timestamp.
     *
     * @return the last changed timestamp
     */
    public String getLastChangedTimestamp() {
        if (form == null)
            return "";

        return form.getLastChangedDateAsString();
    }

    /**
     * Gets the LastChangedBy value (user real name).
     *
     * @return the last changed by
     */
    public String getLastChangedBy() {
        if (form == null || form.getLastChangedBy() == null)
            return "null";

        return ((User) form.getLastChangedBy()).getUserRealName();
    }

    /**
     * Is the visit validated?.
     *
     * @return boolean
     */
    @Deprecated
    public Boolean getVisitIsValidated() {
        return episodeIsValidated;
    }

    /**
     * Is the visit reported?.
     *
     * @return boolean
     */
    @Deprecated
    public Boolean getVisitIsReported() {
        return episodeIsReported;
    }

    /**
     * Is the episode validated?.
     *
     * @return boolean
     */
    public Boolean getEpisodeIsValidated() {
        return episodeIsValidated;
    }

    /**
     * Is the episode reported?.
     *
     * @return boolean
     */
    public Boolean getEpisodeIsReported() {
        return episodeIsReported;
    }

    /**
     * Gets the panel heading color, used in a css style class call.
     *
     * @return the panel heading color css class
     */
    @Deprecated
    public String getPanelHeadingColor() {
        if (form == null)
            return "panel-default";

        if (form.isOpen())
            return "panel-default";

        // if (form.isReported() || form.isValidated())
        // return "panel-danger";

        return "panel-default";
    }

    /**
     * A goto method used by JSF action components.
     *
     * @param link
     *            the link
     * @return the string
     */
    public String goTo(String link) {
        if (link.equals("goNewVisitByButton"))
            ((EpisodeBean) Utils.getBackingBean("episodeBean")).goAddEpisode();

        if (link.equals("goEditVisit"))
            ((EpisodeBean) Utils.getBackingBean("episodeBean")).goEditEpisode();

        if (link.equals("deleteVisit"))
            return ((EpisodeBean) Utils.getBackingBean("episodeBean")).deleteVisit();

        if (link.equals("signWholeVisit"))
            return signWholeVisit();

        if (link.equals("reportWholeVisit"))
            return reportWholeVisit();

        if (link.equals("importLastYearData"))
            return importLastYearData(false);

       if (link.equals("importAllEpisodeForms"))
           return importLastYearData(true);

       return "";
    }

    /**
     * Method to add a row to a data table.
     *
     * @param name
     *            the name of the data table to add a row to
     */
    @Override
    public void addRow(String name) {
        DatatableRows stl = null;

        if (!entries.containsKey(name)) {
            stl = new DatatableRows();
        } else {
            stl = (DatatableRows) entries.get(name);
        }

        stl.add(new DatatableRow());
        entries.put(name, stl);
    }

    /**
     * Method to delete a row from a data table.
     *
     * @param name
     *            Name of the data table
     * @param weg
     *            Row to be removed
     */
    @Override
    public void deleteRow(String name, DatatableRow weg) {
        if (!entries.containsKey(name))
            return;

        DatatableRows stl = (DatatableRows) entries.get(name);
        stl.remove(weg);

        entries.put(name, stl);
    }

    /**
     * Method to sort a datatable by row.
     *
     * @param datatable            The datatable to sort
     * @param key            the row key (mdr key)
     * @return the string
     */
    public String sortBy(String datatable, String key) {
        ((DatatableRows) entries.get(datatable)).sortyBy(key);
        return null;
    }

    /**
     * Makes a "linearModel" Chart Model (a 2-dimentional chart) out of a given
     * Datatable and two of it's column fields.
     *
     * @return the linear model
     */
    // public CartesianChartModel getLinearModel(String name, String xName,
    // String yName) {
    //
    // if (linearModel == null) {
    // linearModel = new CartesianChartModel();
    //
    // DatatableRows rows = (DatatableRows) entries.get(name);
    //
    // TreeMap<Object, Number> myRows = new TreeMap<Object, Number>();
    //
    // if (rows != null) {
    // LineChartSeries series1 = new LineChartSeries();
    // series1.setLabel(name);
    //
    // for (DatatableRow column : rows.getRows()) {
    // HashMap<String, Object> colentries = column.getColumns();
    //
    // Object xValue = colentries.get(xName);
    // Object yValue = colentries.get(yName);
    //
    // yValue = new Integer((String) yValue);
    //
    // myRows.put(xValue, (Number) yValue);
    // }
    //
    // for (Object myKey : myRows.keySet()) {
    // Number value = myRows.get(myKey);
    //
    // if (myKey instanceof Date) {
    // myKey = Utils.getDateString((Date) myKey, "dd.MM.yyyy");
    // } else
    // myKey = myKey.toString();
    //
    // series1.set(myKey, value);
    // }
    //
    // linearModel.addSeries(series1);
    // }
    // }
    //
    // return linearModel;
    // }

    /**
     * Gets the form state number.
     *
     * @return the form state number
     */
    public String getFormStateNumber() {
        if (form == null || episode == null) {
            return "1";
        }

        return form.getFormStateNumber();
    }

    /**
     * Gets the form state.
     *
     * @return the form state
     * @see AbstractViewBean#getFormState()
     */
    @Override
    public String getFormState() {
        if (form == null) {
            return "open";
        }

        return form.getFormState();
    }

    /**
     * Prepares the context menu of the current page
     *
     * TODO: This is yet not finished, and needs rework for the future generic
     * workflow setup.
     *
     * @see de.samply.edc.control.AbstractViewBean#makeContextMenu()
     */
    @Override
    public void makeContextMenu() {
        contextMenuList = new LinkedList<ContextMenuEntry>();
        ContextMenuEntry ts = null;

        if (((SessionBean) getSessionBean()).hasCurrentPatient()) {

            if (((SessionBean) getSessionBean()).getMayCreateEpisodes()) {
                ts = new ContextMenuEntry("goNewVisitByButton", "fa-plus",
                        Utils.getResourceBundleString("link_newvisit"), false, null, null, null, null);
                contextMenuList.add(ts);
            }

            if (((SessionBean) getSessionBean()).hasCurrentEpisode()) {

                if (((SessionBean) getSessionBean()).mayWriteCurrentEpisode()) {
                    ts = new ContextMenuEntry("goEditVisit", "fa-edit",
                            Utils.getResourceBundleString("edit_visit_link"), false, null, null, null, null);
                    contextMenuList.add(ts);

                    ts = new ContextMenuEntry("deleteVisit", "fa-eraser",
                            Utils.getResourceBundleString("delete_visit_link"), true,
                            Utils.getResourceBundleString("delete_visit_link"),
                            Utils.getResourceBundleString("delete_visit_text"),
                            Utils.getResourceBundleString("formbutton_delete_visit_yes"),
                            Utils.getResourceBundleString("formbutton_report_no"));
                    contextMenuList.add(ts);
                }

                boolean isMedicalForm = ((SessionBean) getSessionBean()).isCurrentFormMedical();
                boolean mayWriteEpisodeForms = ((SessionBean) getSessionBean()).mayWriteCurrentEpisodeForm();
                boolean isCurrentFormInOpenState = getFormState().equals("open");
                boolean episodeHasAtleastOneOpenForm = !episodeIsReported && !episodeIsValidated;
                Boolean lastEpisodeExists = getTheLastEpisodeByName() != null;

                if (lastEpisodeExists) {
                    if (isMedicalForm && mayWriteEpisodeForms && isEpisodeForm && isCurrentFormInOpenState) {
                        ts = new ContextMenuEntry("importLastYearData", "fa-upload",
                                Utils.getResourceBundleString("formbutton_import"), true,
                                Utils.getResourceBundleString("formbutton_import_title"),
                                Utils.getResourceBundleString("formbutton_import_areyousure"),
                                Utils.getResourceBundleString("formbutton_import_yes"),
                                Utils.getResourceBundleString("formbutton_import_no"));
                        contextMenuList.add(ts);
                    }

                    if (mayWriteEpisodeForms && isEpisodeForm &&  episodeHasAtleastOneOpenForm){
                        ts = new ContextMenuEntry("importAllEpisodeForms", "fa-upload",
                                Utils.getResourceBundleString("formbutton_import_all"), true,
                                Utils.getResourceBundleString("formbutton_import_title_1"),
                                Utils.getResourceBundleString("formbutton_import_all_areyousure"),
                                Utils.getResourceBundleString("formbutton_import_yes"),
                                Utils.getResourceBundleString("formbutton_import_no"));
                        contextMenuList.add(ts);
                    }
                }
            }
        }
    }

    /**
     * May create patient account.
     *
     * @return the boolean
     */
    public Boolean mayCreatePatientAccount() {
        String locationName = ((Case) ((SessionBean) sessionBean).getCurrentCase()).getLocation().getName();
        return ((SessionBean) sessionBean).mayCreatePatientAccount(locationName);
    }
    
    /**
     * May change status of current form.
     *
     * @return the boolean
     */
    public Boolean mayChangeStatusOfCurrentForm() {
        if (((SessionBean) sessionBean).getCurrentCase() == null)
            return false;

        HashMap<String, Boolean> tempMap = ((SessionBean) sessionBean).getMyChangeStatusPermissions()
                .get(((Case) ((SessionBean) sessionBean).getCurrentCase()).getLocation().getName());

        if (tempMap == null)
            return false;

        if (form == null) {
            return false;
        }

        if (tempMap.get(form.getFormStateNumber()) == null)
            return false;

        return tempMap.get(form.getFormStateNumber());
    }

    /**
     * Reports all forms of the current episode.
     *
     * @return JSF outcome
     */
    public String reportWholeVisit() {
        if (episode == null) {
            Utils.addContextMessage(Utils.getResourceBundleString("summary_reportfailed"),
                    Utils.getResourceBundleString("error_general_noformorvisit"));
            throw new Error("formviewer.reportWholeVisit: Cancling reportform due to no currentvisit");
        }

        for (Entity aForm : episode.getChildren(OSSEVocabulary.Type.EpisodeForm)) {
            if (!((Form) aForm).isValidated()) {
                try {
                    ((Form) aForm).reportForm();
                } catch (FormException | DatabaseException e) {
                    e.printStackTrace();
                }
            }
        }

        getSessionBean().addLog("reported whole visit " + episode.getName() + " of patient "
                + ((SessionBean) getSessionBean()).getCurrentPatient().getId());

        ((SessionBean) getSessionBean()).reinitCaseAndEpisode();
        episode = ((SessionBean) getSessionBean()).getCurrentEpisode();
        ((SessionBean) getSessionBean()).loadNavigationPatientForms();
        makeContextMenu();

        episodeIsValidated = episode.allFormsAreValidated();
        episodeIsReported = episode.allFormsAreReported();

        // XXX: Why go to main page?
        getSessionBean().goMainPatientPage();

        Utils.addContextMessage(Utils.getResourceBundleString("summary_reportsuccess"),
                Utils.getResourceBundleString("success_report"));

        return "reported";
    }

    /**
     * Signs all forms of the current episode.
     *
     * @return JSF outcome
     */
    public String signWholeVisit() {
        if (episode == null) {
            Utils.addContextMessage(Utils.getResourceBundleString("summary_signfailed"),
                    Utils.getResourceBundleString("error_general_noformorvisit"));
            throw new Error("formviewer.signformWholeVisit: Cancling reportform due to no currentvisit");
        }

        for (Entity aForm : episode.getChildren(OSSEVocabulary.Type.EpisodeForm)) {
            if (!((Form) aForm).isValidated()) {
                try {
                    ((Form) aForm).signForm();
                } catch (FormException | DatabaseException e) {
                    e.printStackTrace();
                }
            }
        }

        getSessionBean().addLog("validated whole visit " + episode.getName() + " of patient "
                + ((SessionBean) getSessionBean()).getCurrentPatient().getId());

        ((SessionBean) getSessionBean()).reinitCaseAndEpisode();
        episode = ((SessionBean) getSessionBean()).getCurrentEpisode();
        ((SessionBean) getSessionBean()).loadNavigationPatientForms();
        makeContextMenu();

        episodeIsValidated = episode.allFormsAreValidated();
        episodeIsReported = episode.allFormsAreReported();

        // XXX: why go to main page here?
        getSessionBean().goMainPatientPage();

        Utils.addContextMessage(Utils.getResourceBundleString("summary_signsuccess"),
                Utils.getResourceBundleString("success_sign"));
        return "sign";
    }

    /**
     * Gets the data entries of the form.
     *
     * @return the entries
     */
    public MyMap<String, Object> getEntries() {
        if (form == null) {
            return new MyMap<String, Object>(null);
        }
        if (entries == null)
            entries = new MyMap<String, Object>(form);
        return entries;
    }

    /**
     * Sets the data entries of the form.
     *
     * @param entries
     *            the entries
     */
    public void setEntries(MyMap<String, Object> entries) {
        this.entries = entries;
    }

    /**
     * Gets the episode.
     *
     * @return Visit
     */
    public Episode getEpisode() {
        return episode;
    }

    /**
     * Gets the form.
     *
     * @return the form
     */
    public Form getForm() {
        return form;
    }

    /**
     * Gets the new status.
     *
     * @return the newStatus
     */
    public String getNewStatus() {
        return newStatus;
    }

    /**
     * Sets the new status.
     *
     * @param newStatus            the newStatus to set
     */
    public void setNewStatus(String newStatus) {
        this.newStatus = newStatus;
    }

    /**
     * TODO: make this generic/config based.
     *
     * @return the possibleNewStatus
     */
    public List<SelectItem> getPossibleNewStatus() {
        return possibleNewStatus;
    }

    /**
     * Sets the possible new status.
     *
     * @param possibleNewStatus            the possibleNewStatus to set
     */
    public void setPossibleNewStatus(List<SelectItem> possibleNewStatus) {
        this.possibleNewStatus = possibleNewStatus;
    }

    /**
     * Gets the form comments.
     *
     * @return the form comments
     */
    public TreeSet<FormComment> getFormComments() {
        return formComments;
    }

    /**
     * Checks for form comments.
     *
     * @return the boolean
     */
    public Boolean hasFormComments() {
        if (formComments == null || formComments.isEmpty())
            return false;

        return true;
    }

    /**
     * Returns the translated form name.
     *
     * @param formId
     * @param formVersion
     * @param locale ISO 639-1 code of the language to fetch.
     * @return The translated form name (or the default name if no translation available).
     */
    public String getFormTranslatedName(long formId, long formVersion, String locale) {
        String exactResult = null;
        FormDetails formDetails = FormUtils.loadFormFromXML(formId, formVersion);
        FormDetails.FormI18n formI18n = FormUtils.getFormI18n(formDetails, locale);

        if (formI18n instanceof FormDetails.FormI18n) {
            exactResult = formI18n.getName();
        }

        return (exactResult != null)?exactResult:formDetails.getName();
    }

    /**
     * Returns the translated form description.
     *
     * @param formId
     * @param formVersion
     * @param locale ISO 639-1 code of the language to fetch.
     * @return The translated form description (or the default description if no translation available).
     */
    public String getFormTranslatedDescription(long formId, long formVersion, String locale) {
        String exactResult = null;
        FormDetails formDetails = FormUtils.loadFormFromXML(formId, formVersion);
        FormDetails.FormI18n formI18n = FormUtils.getFormI18n(formDetails, locale);

        if (formI18n instanceof FormDetails.FormI18n) {
            exactResult = formI18n.getDescription();
        }

        return (exactResult != null)?exactResult:formDetails.getDescription();
    }

    /**
     * Returns the translated html content of a form item.
     *
     * @param formId       The id of the form which contains the item requested.
     * @param formVersion  The version of the form which contains the item requested.
     * @param itemPosition The numerical position of the item requested.
     * @param locale       The ISO 639-1 code of the content's language to return.
     * @return The translated html content of the item specified (or the default content if no translation available).
     */
    public String getRichTextFormItemTranslated(long formId, long formVersion, int itemPosition, String locale) {
        String exactResult = null;
        FormDetails formDetails = FormUtils.loadFormFromXML(formId, formVersion);
        FormDetails.Item item = FormUtils.getFormItem(formDetails, itemPosition);
        FormDetails.Item.ItemI18n itemI18n = FormUtils.getFormItemI18n(formDetails, itemPosition, locale);

        if (itemI18n instanceof FormDetails.Item.ItemI18n) {
            exactResult = itemI18n.getHtml();
            exactResult = new String(exactResult.getBytes(Charset
                    .forName("UTF-8")), Charset.forName("Windows-1252"));
            exactResult = exactResult.replace("<p><hr>", "<p /><hr />");
            exactResult = exactResult.replace("<p><hr/>", "<p /><hr />");
            exactResult = exactResult.replace("<br>", "<br />");
            exactResult = exactResult.replace("<hr>", "<hr />");
        }

        return (exactResult != null)?exactResult:item.getHtml();
    }

}