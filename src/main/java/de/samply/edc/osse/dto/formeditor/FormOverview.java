/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.osse.dto.formeditor;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * DTO for FormOverview (used in communication with form repository REST).
 *
 * This is basically a copy of de.samply.formeditor.rest.model.FormOverview.
 */
@XmlRootElement
public class FormOverview {

    /** The id. */
    private int id;

    /** The version. */
    private int version;

    /** The name. */
    private String name;

    /** The description. */
    private String description;

    /** The type. */
    private Type type;

    /** The created. */
    private Created created;

    /** The updated. */
    private Updated updated;
    /**
     * Translations of the form.
     */
    private List<I18n> i18ns = new ArrayList<>();

    /**
     * Form creation information.
     */
    public static class Created {
        /**
         * Form date creation.
         */
        private Date createdAt;
        /**
         * Person who created the form.
         */
        private CreatedBy person;

        /**
         * Person who created the form.
         */
        public static class CreatedBy {

            /** The id. */
            private int id;

            /** The name. */
            private String name;

            /**
             * Gets the id.
             *
             * @return the ID of the person who created the form
             */
            public int getId() {
                return id;
            }

            /**
             * Sets the id.
             *
             * @param id
             *            the ID of the person who created the form
             */
            public void setId(int id) {
                this.id = id;
            }

            /**
             * Gets the name.
             *
             * @return the name of the person who created the form
             */
            public String getName() {
                return name;
            }

            /**
             * Sets the name.
             *
             * @param name
             *            the name of the person who created the form
             */
            public void setName(String name) {
                this.name = name;
            }
        }

        /**
         * Gets the created at.
         *
         * @return the form creation date and time
         */
        public Date getCreatedAt() {
            return createdAt;
        }

        /**
         *
         * @param createdAt
         *            the form creation date and time
         */
        public void setCreatedAt(Date createdAt) {
            this.createdAt = createdAt;
        }

        /**
         * Gets the person.
         *
         * @return the person who created the form
         */
        public CreatedBy getPerson() {
            return person;
        }

        /**
         * Sets the person.
         *
         * @param person
         *            the person who created the form
         */
        public void setPerson(CreatedBy person) {
            this.person = person;
        }

    }

    /**
     * Form update information.
     */
    public static class Updated {
        /**
         * Form update date and time.
         */
        private Date updatedAt;
        /**
         * Person who updated the form.
         */
        private UpdatedBy person;

        /**
         * Person who updated the form.
         */
        public static class UpdatedBy {
            /**
             * Person ID.
             */
            private int id;
            /**
             * Person name.
             */
            private String name;

            /**
             * Gets the id.
             *
             * @return the person ID
             */
            public int getId() {
                return id;
            }

            /**
             * Sets the id.
             *
             * @param id
             *            the person ID
             */
            public void setId(int id) {
                this.id = id;
            }

            /**
             * Gets the name.
             *
             * @return the person name
             */
            public String getName() {
                return name;
            }

            /**
             * Sets the name.
             *
             * @param name
             *            the person name
             */
            public void setName(String name) {
                this.name = name;
            }
        }

        /**
         * Gets the updated at.
         *
         * @return the update time and date
         */
        public Date getUpdatedAt() {
            return updatedAt;
        }

        /**
         * Sets the updated at.
         *
         * @param updatedAt
         *            the update time and date
         */
        public void setUpdatedAt(Date updatedAt) {
            this.updatedAt = updatedAt;
        }

        /**
         * Gets the person.
         *
         * @return the person who update the form
         */
        public UpdatedBy getPerson() {
            return person;
        }

        /**
         * Sets the person.
         *
         * @param person
         *            the person who updated the form
         */
        public void setPerson(UpdatedBy person) {
            this.person = person;
        }

    }

    /**
     * A form translation.
     */
    public static class I18n {
        /**
         * The language of the translation in ISO 639-1.
         */
        private String language;
        /**
         * The translated form name.
         */
        private String name;
        /**
         * The translated form description.
         */
        private String description;

        /**
         * @return The translation's language in ISO 639-1.
         */
        public String getLanguage() {
            return language;
        }

        /**
         * @param language ISO 639-1 code of the language
         */
        public void setLanguage(String language) {
            this.language = language;
        }

        /**
         * @return The translated form name.
         */
        public String getName() {
            return name;
        }

        /**
         * @param name The translated form name.
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return The translated form description.
         */
        public String getDescription() {
            return description;
        }

        /**
         * @param description The translated form description.
         */
        public void setDescription(String description) {
            this.description = description;
        }
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the form id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the form version.
     *
     * @return the form version
     */
    public final int getVersion() {
        return version;
    }

    /**
     * Sets the form version.
     *
     * @param version
     *            the form version to set
     */
    public final void setVersion(final int version) {
        this.version = version;
    }

    /**
     * Gets the name.
     *
     * @return the form name
     */
    public final String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the form name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the description.
     *
     * @return the form description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description
     *            the form description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the type.
     *
     * @return the form type
     */
    public Type getType() {
        return type;
    }

    /**
     * Sets the type.
     *
     * @param type
     *            the form type to set
     */
    public void setType(Type type) {
        this.type = type;
    }

    /**
     * Gets the created.
     *
     * @return the form creation information
     */
    public Created getCreated() {
        return created;
    }

    /**
     * Sets the created.
     *
     * @param created
     *            the form creation information
     */
    public void setCreated(Created created) {
        this.created = created;
    }

    /**
     * @return the form update information
     */
    public Updated getUpdated() {
        return updated;
    }

    /**
     * Sets the updated.
     *
     * @param updated
     *            the form update information
     */
    public void setUpdated(Updated updated) {
        this.updated = updated;
    }

    /**
     * @return List of form translations.
     */
    public List<I18n> getI18ns() {
        return i18ns;
    }

    /**
     * @param i18ns List of form translations.
     */
    public void setI18ns(List<I18n> i18ns) {
        this.i18ns = i18ns;
    }

}
