package de.samply.edc.osse.validator;

import de.samply.edc.catalog.Vocabulary;
import de.samply.store.JSONResource;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Created by Jannik on 18.05.2017.
 */
public class FAIRValidator {


    public boolean CheckFDPMetaData(JSONResource osseConfig) {

        boolean validate = true;

        if(osseConfig.getProperty("fair.fdpmetadata.version").getValue().equals("")){

            validate = false;
        }

        if(osseConfig.getProperty("fair.fdpmetadata.url").getValue().equals("")){

            validate = false;
        }

        return validate;



    }

    public boolean CheckCatalogMetaData(JSONResource osseConfig){


        //Version

        boolean validate = true;

        if(osseConfig.getProperty("fair.catalog.version").getValue().equals("")){

            validate = false;
        }


        return validate;

    }

    public boolean CheckDataSetMetaData(JSONResource osseConfig){

        //version
        //theme

        boolean validate = true;

        if(osseConfig.getProperty("fair.dataset.version").getValue().equals("")){

            validate = false;
        }

        if(osseConfig.getProperty("fair.dataset.theme").getValue().equals("")){

            validate = false;
        }



        return validate;


    }

    public boolean CheckDistributionMetaData(JSONResource osseConfig){


        boolean validate = true;

        if(osseConfig.getProperty("fair.distribution.version").getValue().equals("")){

            validate = false;
        }

        return validate;

    }






}
