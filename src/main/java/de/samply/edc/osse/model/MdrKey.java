/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.osse.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.samply.auth.rest.AccessTokenDTO;
import de.samply.common.mdrclient.MdrClient;
import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.common.mdrclient.MdrInvalidResponseException;
import de.samply.common.mdrclient.domain.Definition;
import de.samply.common.mdrclient.domain.Label;
import de.samply.common.mdrclient.domain.Result;
import de.samply.common.mdrclient.domain.Validations;
import de.samply.edc.osse.exceptions.OSSEException;
import de.samply.edc.utils.Utils;

/**
 * MDR Key model.
 */
public class MdrKey implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * possible types are dataelement and record.
     */
    public static enum Type {
        
        /** The dataelement. */
        DATAELEMENT, 
        /** The record. */
        RECORD
    };

    /**  namespace of the mdr key. */
    private String namespace;

    /**  type of the mdr key as string. */
    private String type;

    /**  the mdr key id. */
    private String id;

    /**  the mdr key version. */
    private String version;

    /**  the purified representation of the mdr key. */
    private String purified = null;

    /**  a mdr client to requst data from the MDR. */
    private transient MdrClient mdrClient = null;

    /**  Accesstoken for communication with MDR. */
    private AccessTokenDTO accessToken = null;

    /**  Auth User ID for communication with MDR. */
    private String authUserId = null;

    /**  Labels of this mdr key. */
    private transient List<Label> label = null;

    /**  Members of this mdr record. */
    private transient List<Result> members = null;

    /**  Definition of this mdr key. */
    private transient Definition definition = null;

    /**
     * Instantiates a new mdr key.
     */
    public MdrKey() {
        namespace = "";
        type = "";
        id = "";
        version = "";
    }

    /**
     * Instantiates a new mdr key.
     *
     * @param mdrKey the mdr key
     */
    public MdrKey(String mdrKey) {
        parseString(mdrKey);
    }

    /**
     * Initialize the mdr client.
     *
     * @param mdrClient            The mdr Client
     * @param accessToken            The access token to access the MDR
     * @param authUserId            The auth User ID to access the MDR
     */
    public void initMdrClient(MdrClient mdrClient, AccessTokenDTO accessToken, String authUserId) {
        this.mdrClient = mdrClient;
        this.accessToken = accessToken;
        this.authUserId = authUserId;
    }

    /**
     * Gets the members of a mdr record.
     *
     * @return set of MdrKey
     * @throws MdrConnectionException the mdr connection exception
     * @throws ExecutionException the execution exception
     * @throws OSSEException the OSSE exception
     */
    public Set<MdrKey> getMembersSet() throws MdrConnectionException, ExecutionException, OSSEException {
        Set<MdrKey> membersSet = new HashSet<MdrKey>();

        for (Result member : getMembers()) {
            MdrKey memberKey = new MdrKey(member.getId());
            memberKey.setMdrClient(getMdrClient());
            memberKey.setAccessToken(getAccessToken());
            memberKey.setAuthUserId(getAuthUserId());
            membersSet.add(memberKey);
        }

        return membersSet;
    }

    /**
     * Gets a list of record members.
     *
     * @return list of mdrclient.domain.Result
     * @throws MdrConnectionException the mdr connection exception
     * @throws ExecutionException the execution exception
     * @throws OSSEException the OSSE exception
     */
    public List<Result> getMembers() throws MdrConnectionException, ExecutionException, OSSEException {
        if (members == null) {
            if (mdrClient == null)
                throw new OSSEException("The MDRclient needs to be activated before calling for members.");

            if (isRecord()) {
                members = mdrClient.getRecordMembers(
                        toString(), "en", accessToken.getAccessToken(),
                        authUserId
                        );
            } else {
                throw new OSSEException("The MDRkey " + toString() + " is not a record and thus cannot have members.");
            }
        }
        return members;
    }

    /**
     * Gets a list of mdr key labels.
     *
     * @return the label
     * @throws MdrConnectionException the mdr connection exception
     * @throws ExecutionException the execution exception
     * @throws OSSEException the OSSE exception
     */
    public List<Label> getLabel() throws MdrConnectionException, ExecutionException, OSSEException {
        if (label == null) {
            if (mdrClient == null)
                throw new OSSEException("The MDRclient needs to be activated before calling for labels.");

            if (isRecord()) {
                label = mdrClient.getRecordLabel(toString(),
                        "en", accessToken.getAccessToken(),
                        authUserId
                        );
            } else {
                throw new OSSEException("The MDRkey " + toString()
                        + " is not a record and thus cannot have record labels.");
            }
        }

        return label;
    }

    /**
     * Gets the definition.
     *
     * @return the definition
     * @throws MdrConnectionException the mdr connection exception
     * @throws MdrInvalidResponseException the mdr invalid response exception
     * @throws ExecutionException the execution exception
     * @throws OSSEException the OSSE exception
     */
    public Definition getDefinition() throws MdrConnectionException, MdrInvalidResponseException, ExecutionException,
            OSSEException {
        if (mdrClient == null)
            throw new OSSEException("The MDRclient needs to be activated before calling for definitions.");

        if (definition == null) {
            definition = mdrClient.getDataElementDefinition(toString(), "en", accessToken.getAccessToken(),
                    authUserId);
        }
        return definition;
    }

    /**
     * Gets this mdrkey's first definition as string.
     *
     * @return the mdr definition
     * @throws MdrConnectionException the mdr connection exception
     * @throws ExecutionException the execution exception
     * @throws OSSEException the OSSE exception
     * @throws MdrInvalidResponseException the mdr invalid response exception
     */
    public String getMdrDefinition() throws MdrConnectionException, ExecutionException, OSSEException,
            MdrInvalidResponseException {
        if (isRecord()) {
            return getLabel().get(0).getDefinition();
        } else {
            return getDefinition().getDesignations().get(0).getDefinition();
        }
    }

    /**
     * gets this mdrkey's first designation as string.
     *
     * @return the mdr designation
     * @throws MdrConnectionException the mdr connection exception
     * @throws ExecutionException the execution exception
     * @throws OSSEException the OSSE exception
     * @throws MdrInvalidResponseException the mdr invalid response exception
     */
    public String getMdrDesignation() throws MdrConnectionException, ExecutionException, OSSEException,
            MdrInvalidResponseException {
        if (isRecord()) {
            return getLabel().get(0).getDesignation();
        } else {
            return getDefinition().getDesignations().get(0).getDesignation();
        }
    }

    /**
     * gets this mdrkey's first designation as string.
     *
     * @return the mdr label
     * @throws MdrConnectionException the mdr connection exception
     * @throws ExecutionException the execution exception
     * @throws OSSEException the OSSE exception
     * @throws MdrInvalidResponseException the mdr invalid response exception
     */
    public String getMdrLabel() throws MdrConnectionException, ExecutionException, OSSEException,
            MdrInvalidResponseException {
        return getMdrDesignation();
    }

    /**
     * Parses a string representation of a mdr key.
     *
     * @param mdrKey the mdr key
     * @return boolean if this is a real mdr key
     */
    public Boolean parseString(String mdrKey) {
        Pattern pattern = Pattern.compile("urn:(.*):(.*):(.*):(.*)");
        Matcher matcher = pattern.matcher(mdrKey);

        if (!matcher.find()) {
            return false;
        }

        namespace = matcher.group(1);
        type = matcher.group(2);
        id = matcher.group(3);
        version = matcher.group(4);

        return true;
    }

    /**
     * Returns a string representation of this mdr key.
     *
     * @return the string
     */
    @Override
    public String toString() {
        return "urn:" + namespace + ":" + type + ":" + id + ":" + version;
    }

    /**
     * Returns a version of the MDRkey in which dots are replaced with one
     * underscore and colons are replaced with double underscores colons and
     * dots are illegal characters in maps and JSF params so we need to replace
     * them in a way that our backend saver can recognize and re-replace with
     * the original character.
     *
     * @return the purified mdrkey as string
     */
    public String purify() {
        if (purified == null) {
            String mdrID = toString();
            mdrID = mdrID.replace(":", "__");
            mdrID = mdrID.replace(".", "_");
            purified = mdrID;
        }

        return purified;
    }

    /**
     * Returns the MDR key as we need it for the import/export XMLs.
     *
     * @return the string
     */
    public String forXML() {
        return Utils.upperCaseFirstChar(type) + "_" + id + "_" + version;
    }

    /**
     * gets the namespace part of this mdrkey.
     *
     * @return the string
     */
    public String namespaceForXML() {
        return "urn:" + namespace;
    }

    /**
     * checks if a mdrkey represents a record.
     *
     * @return the boolean
     */
    public Boolean isRecord() {
        return getKeyType() == Type.RECORD;
    }

    /**
     * Gets the key type as enum Type.
     *
     * @return the key type
     */
    public Type getKeyType() {
        if (type == null)
            return null;

        if (type.equalsIgnoreCase("record"))
            return Type.RECORD;
        else
            return Type.DATAELEMENT;
    }

    /**
     * Returns the Validations of a MDRkey.
     *
     * @return the Validations
     * @throws MdrConnectionException the mdr connection exception
     * @throws MdrInvalidResponseException the mdr invalid response exception
     * @throws ExecutionException the execution exception
     * @throws OSSEException the OSSE exception
     */
    public Validations getValidations() throws MdrConnectionException, MdrInvalidResponseException, ExecutionException,
            OSSEException {
        if (mdrClient == null)
            throw new OSSEException("The MDRclient needs to be activated before calling for members.");

        if (isRecord()) {
            throw new OSSEException("The MDRkey " + toString() + " is a record and thus cannot have validators.");
        }
        return mdrClient.getDataElementValidations(toString(), "en", accessToken.getAccessToken(), authUserId);
    }

    /**
     * Gets the namespace.
     *
     * @return the namespace
     */
    public String getNamespace() {
        return namespace;
    }

    /**
     * Sets the namespace.
     *
     * @param namespace            the namespace to set
     */
    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type.
     *
     * @param type            the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id            the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the version.
     *
     * @param version            the version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * Gets the mdr client.
     *
     * @return the mdrClient
     */
    public MdrClient getMdrClient() {
        return mdrClient;
    }

    /**
     * Sets the mdr client.
     *
     * @param mdrClient            the mdrClient to set
     */
    public void setMdrClient(MdrClient mdrClient) {
        this.mdrClient = mdrClient;
    }

    /**
     * Gets the access token.
     *
     * @return the accessToken
     */
    public AccessTokenDTO getAccessToken() {
        return accessToken;
    }

    /**
     * Sets the access token.
     *
     * @param accessToken            the accessToken to set
     */
    public void setAccessToken(AccessTokenDTO accessToken) {
        this.accessToken = accessToken;
    }

    /**
     * Gets the auth user id.
     *
     * @return the authUserId
     */
    public String getAuthUserId() {
        return authUserId;
    }

    /**
     * Sets the auth user id.
     *
     * @param authUserId            the authUserId to set
     */
    public void setAuthUserId(String authUserId) {
        this.authUserId = authUserId;
    }

}
