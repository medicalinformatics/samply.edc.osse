/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.edc.osse.model;

import java.io.Serializable;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.utils.Utils;
import de.samply.store.JSONResource;
import de.samply.store.TimestampLiteral;

/**
 * The Class FormComment.
 */
public class FormComment implements Serializable, Comparable<FormComment> {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The comment. */
    private String comment;
    
    /** The user uri. */
    private String userURI;
    
    /** The user. */
    private User theUser;
    
    /** The timestamp. */
    private long timestamp;

    /**
     * Instantiates a new form comment.
     *
     * @param comment the comment
     * @param userURI the user uri
     * @param timestamp the timestamp
     */
    public FormComment(String comment, String userURI, long timestamp) {
        this.comment = comment;
        this.userURI = userURI;
        this.timestamp = timestamp;
    }

    /**
     * Instantiates a new form comment.
     *
     * @param data the data
     */
    public FormComment(JSONResource data) {
        readJSONResource(data);
    }

    /**
     * Gets the comment.
     *
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the comment.
     *
     * @param comment the new comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * Gets the user real name.
     *
     * @return the user real name
     */
    public String getUserRealName() {
        if(theUser == null) {
            theUser = new User(Utils.getDatabase(), userURI);
            theUser.load(false, false);
        }
        
        return theUser.getUserRealName();
    }
    
    /**
     * Gets the timestamp.
     *
     * @return the timestamp
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the timestamp.
     *
     * @param timestamp the new timestamp
     */
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Read json resource.
     *
     * @param data the data
     */
    public void readJSONResource(JSONResource data) {
        if (data == null)
            return;

        this.comment = data.getProperty(Vocabulary.Comment.commentText).getValue();
        this.timestamp = data.getProperty(Vocabulary.Comment.commentTimestamp).asLong();
        this.userURI = data.getProperty(Vocabulary.Comment.commentUserURI).getValue();
    }

    /**
     * To json resource.
     *
     * @return the JSON resource
     */
    public JSONResource toJSONResource() {
        JSONResource commentResource = new JSONResource();
        commentResource.addProperty(Vocabulary.Comment.commentTimestamp, new TimestampLiteral(timestamp));
        commentResource.addProperty(Vocabulary.Comment.commentUserURI, userURI);
        commentResource.addProperty(Vocabulary.Comment.commentText, comment);

        return commentResource;
    }

    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(FormComment other) {
        if (other.getTimestamp() > getTimestamp())
            return -1;

        if (other.getTimestamp() < getTimestamp())
            return 1;

        return 0;
    }
}
