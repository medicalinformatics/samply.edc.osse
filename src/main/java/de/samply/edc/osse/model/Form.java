/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.osse.model;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.control.AbstractDatabase;
import de.samply.edc.exceptions.FormException;
import de.samply.edc.model.Entity;
import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.utils.Utils;
import de.samply.store.Resource;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.osse.OSSEVocabulary;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

/**
 * Model class for forms.
 */
public abstract class Form extends Entity {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new form.
     *
     * @param database
     *            the database
     * @param formType
     *            the OSSEVocabulary.Type.* form type
     * @param formURI
     *            the backend URI of the form
     */
    public Form(AbstractDatabase<?> database, String formType, String formURI) {
        super(database, formType, formURI);
    }

    /**
     * Instantiates a new form.
     *
     * @param database
     *            the database
     * @param formType
     *            the OSSEVocabulary.Type.* form type
     * @param formRes
     *            the backend resource of the form
     */
    public Form(AbstractDatabase<?> database, String formType, Resource formRes) {
        super(database, formType, formRes);
    }

    /**
     * Instantiates a new form.
     *
     * @param database
     *            the database
     * @param formType
     *            the OSSEVocabulary.Type.* form type
     */
    public Form(AbstractDatabase<?> database, String formType) {
        super(database, formType);
    }

    /**
     * Sets the form status to a given status, only legal statuses will be
     * accepted.
     *
     * @param state            the new form status
     * @throws FormException             if an illegal status is provided
     * @throws DatabaseException the database exception
     */
    public void setFormState(String state) throws FormException, DatabaseException {
        if (!((ApplicationBean) Utils.getAB()).getFormStatusReverse()
                .containsKey(state)) {
            throw new FormException(
                    "form.setFormState called with illegal state "
                            + state
                            + " available are only: "
                            + ((ApplicationBean) Utils.getAB()).getFormStatus()
                                    .values());
        }

        Resource statusResource = Utils.findResourceByProperty(
                OSSEVocabulary.Type.Status, "name", state);
        if (statusResource == null) {
            throw new FormException(
                    "form.setFormState called with illegal state " + state
                            + ": no Resource found!");
        }

        getDatabase().beginTransaction();
        getDatabase().executeFormAction(getResource(),
                statusResource);
        getDatabase().commit();
    }

    /**
     * Sets a form state to 'reported'.
     *
     * @throws FormException the form exception
     * @throws DatabaseException the database exception
     */
    public void reportForm() throws FormException, DatabaseException {
        setFormState("reported");
    }

    /**
     * Sets a form status to 'validated'.
     *
     * @throws FormException the form exception
     * @throws DatabaseException the database exception
     */
    public void signForm() throws FormException, DatabaseException {
        setFormState("validated");
    }

    /**
     * Sets a form status to 'refused'.
     *
     * @throws FormException the form exception
     * @throws DatabaseException the database exception
     */
    public void refuseForm() throws FormException, DatabaseException {
        setFormState("open");
    }

    /**
     * Sets a form status to 'open' (used in action "refuse form").
     *
     * @throws FormException the form exception
     * @throws DatabaseException the database exception
     */
    public void reportTakeBackForm() throws FormException, DatabaseException {
        setFormState("open");
    }

    /**
     * Sets a form status to 'open' (used in action "break sign").
     *
     * @throws FormException the form exception
     * @throws DatabaseException the database exception
     */
    public void signTakeBackForm() throws FormException, DatabaseException {
        setFormState("open");
    }

    /**
     * Sets the version.
     *
     * @param formVersion
     *            the new version
     */
    public void setVersion(Integer formVersion) {
        String versionParam = OSSEVocabulary.CaseForm.Version;
        if (getType() == null
                || getType().equals(OSSEVocabulary.Type.EpisodeForm))
            versionParam = OSSEVocabulary.EpisodeForm.Version;

        getResource().setProperty(versionParam, formVersion);
        setProperty(versionParam, formVersion);
        getDatabase().save(getResource());
    }

    /**
     * Creates a new form and sets its initial status to the one with the
     * backend ID "1" (which in the original system is "open").
     *
     * @param formName            the form name
     * @param formVersion            the form version
     */
    public void createForm(String formName, Integer formVersion) {
        String formNameParam = OSSEVocabulary.CaseForm.Name;
        String formStateParam = OSSEVocabulary.CaseForm.Status;
        String formVersionParam = OSSEVocabulary.CaseForm.Version;
        String formParentParam = OSSEVocabulary.CaseForm.Case;
        String parentType = OSSEVocabulary.Type.Case;

        if (getType() == null
                || getType().equals(OSSEVocabulary.Type.EpisodeForm)) {
            formNameParam = OSSEVocabulary.EpisodeForm.Name;
            formStateParam = OSSEVocabulary.EpisodeForm.Status;
            formVersionParam = OSSEVocabulary.EpisodeForm.Version;
            formParentParam = OSSEVocabulary.EpisodeForm.Episode;
            parentType = OSSEVocabulary.Type.Episode;
        }

        setProperty(formNameParam, formName);
        Resource statusRes = Utils.findResourceByProperty(
                OSSEVocabulary.Type.Status, OSSEVocabulary.ID, "1");
        setProperty(formStateParam, statusRes);
        setProperty(formVersionParam, formVersion);
        setProperty(formParentParam, getParent(parentType).getResource());

        // define if it is a "patientform" or an ordinary registry form
        if(((ApplicationBean) Utils.getAB()).isPatientForm(formName)) {
            setProperty(Vocabulary.Form.formType, Vocabulary.FormTypes.PATIENTFORM);
        } else {
            setProperty(Vocabulary.Form.formType, Vocabulary.FormTypes.DEFAULT);
        }
        
        getDatabase().beginTransaction();
        saveOrUpdate();
        getDatabase().commit();

        getParent(parentType).addChild(getType(), this);
    }

    /**
     * Deletes any medical data (i.e. the key is a MDRKey) from the form properties
     */
    public void deleteFormMedicalData() {
        Iterator<Entry<String, Object>> allKeysIterator = getProperties().entrySet().iterator();
        while(allKeysIterator.hasNext()) {
            Entry<String, Object> entry = allKeysIterator.next();
            String key = entry.getKey();
            String fkey = Utils.fixMDRkeyForSave(key);
            MdrKey mdrKey = new MdrKey(fkey);
            if(mdrKey.getKeyType() == null) {
                continue;
            }
            
            allKeysIterator.remove();
            addRemovedProperty(fkey);
        }
    }

    /**
     * Copies the Data of a given previous Episode's Form
     *
     * @param previousForm
     */
    public void importPreviousFormData(Form previousForm) {
        String summary = Utils.getResourceBundleString("summary_importfailed");
        String errorFormNotOpen = Utils.getResourceBundleString("error_formnotopen");

        HashMap<String, Boolean> notCopyableDataKeys = new HashMap<String, Boolean>();
        notCopyableDataKeys.put(OSSEVocabulary.ID, true);
        notCopyableDataKeys.put("lastChangedDate", true);
        notCopyableDataKeys.put("lastChangedBy", true);
        notCopyableDataKeys.put("transaction_id", true);
        notCopyableDataKeys.put("activePermissions", true);
        notCopyableDataKeys.put(OSSEVocabulary.EpisodeForm.Name, true);
        notCopyableDataKeys.put(OSSEVocabulary.EpisodeForm.Status, true);
        notCopyableDataKeys.put(OSSEVocabulary.EpisodeForm.Version, true);
        notCopyableDataKeys.put(OSSEVocabulary.EpisodeForm.Episode, true);

            if(isOpen()){
                for (String key : previousForm.getProperties().keySet()) {
                    if (notCopyableDataKeys.get(key) != null) {
                    continue;
                    }

                    setProperty(key, previousForm.getProperty(key));
                }
            }

        getDatabase().beginTransaction();
        saveOrUpdate();
        getDatabase().commit();
        storeLastChange();
    }

    
    /**
     * Checks if form is in the status 'validated'.
     *
     * @return the boolean
     */
    public Boolean isValidated() {
        if (getFormState().equals("validated"))
            return true;

        return false;
    }

    /**
     * Checks if form is in the status 'reported'.
     *
     * @return the boolean
     */
    public Boolean isReported() {
        if (getFormState().equals("reported"))
            return true;

        return false;
    }

    /**
     * Checks if form is in the status 'open'.
     *
     * @return the boolean
     */
    public Boolean isOpen() {
        if (getFormState().equals("open"))
            return true;

        return false;
    }

    /**
     * Gets the form state number (which is basically the ID of the backend
     * resource of the state).
     *
     *
     * @return the form status number.
     */
    public String getFormStateNumber() {
        String statusNumber = ((ApplicationBean) Utils.getAB()).getFormStandardStatusNumber();

        // no type? return the standard status
        if(getType() == null)
        	return statusNumber;

        if(getType().equals(OSSEVocabulary.Type.EpisodeForm)) {
        	Object statusRes = getProperty(OSSEVocabulary.EpisodeForm.Status);
        	if(statusRes != null) {
        		if(statusRes instanceof Resource) {
        			return ""+((Resource) statusRes).getId();
        		}
        		statusNumber = ((String) statusRes).split(":")[1];;
        	}
        } else if(getType().equals(OSSEVocabulary.Type.CaseForm)) {
        	Object statusRes = getProperty(OSSEVocabulary.CaseForm.Status);
        	if(statusRes != null) {
        		if(statusRes instanceof Resource) {
        			return ""+((Resource) statusRes).getId();
        		}
        		statusNumber = ((String) statusRes).split(":")[1];;
        	}
        }

        return statusNumber;
    }

    /**
     * Gets the form status name (open, reported, validated, refused).

     *
     * @return the form status
     */
    public String getFormState() {
        String formState = ((ApplicationBean) Utils.getAB()).getFormStatus()
                .get(Integer.parseInt(getFormStateNumber()));

        if (formState == null)
            return "open";

        return formState;
    }

    /**
     * Gets the version of the form.
     *
     * @return the version
     */
    public String getVersion() {
        if (getType() == null
                || getType().equals(OSSEVocabulary.Type.EpisodeForm))
            return getProperty(OSSEVocabulary.EpisodeForm.Version).toString();
        return getProperty(OSSEVocabulary.CaseForm.Version).toString();
    }

    /**
     * Gets the form id/name.
     *
     * @return the form id
     */
    public String getFormID() {
        if (getType() == null
                || getType().equals(OSSEVocabulary.Type.EpisodeForm))
            return (String) getProperty(OSSEVocabulary.EpisodeForm.Name);
        return (String) getProperty(OSSEVocabulary.CaseForm.Name);
    }

    /**
     * Prints out the properties of the form.
     *
     * @return the string
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String text = "";

        text += getURI();

        for (String key : this.getProperties().keySet()) {
            text = new StringBuilder()
                    .append(text).append("K: ").append(key).append(" V: ").append(getProperty(key)).append("\n\n")
                    .toString();
        }

        return text;
    }

    /**
     * returns the page title name of a form.
     *
     * @return String the page title name
     */
    public String getFormRealname() {

        String formid = getFormID();
        String[] moo = formid.split("-");
        if (moo.length > 0) {
            return Utils.getAB().getPageTitleName(formid, Utils.getLocale());
        }

        return "";
    }
}
