/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.osse.bridgehead;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import javax.xml.parsers.ParserConfigurationException;

import org.codehaus.jettison.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.thoughtworks.xstream.XStream;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.model.DatatableRow;
import de.samply.edc.model.DatatableRows;
import de.samply.edc.model.Entity;
import de.samply.edc.osse.bridgehead.exception.ImportException;
import de.samply.edc.osse.control.Database;
import de.samply.edc.osse.model.Case;
import de.samply.edc.osse.model.CaseForm;
import de.samply.edc.osse.model.Episode;
import de.samply.edc.osse.model.EpisodeForm;
import de.samply.edc.osse.model.Form;
import de.samply.edc.osse.model.Location;
import de.samply.edc.osse.model.OSSEMessage;
import de.samply.edc.osse.model.Patient;
import de.samply.edc.osse.model.SessionsKillerSingleton;
import de.samply.edc.osse.model.User;
import de.samply.edc.osse.utils.MapEntryConverter;
import de.samply.edc.osse.utils.XMLHelper;
import de.samply.edc.utils.Utils;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.Value;
import de.samply.store.osse.OSSEVocabulary;

/**
 * REST interface for bridgehead functions.
 */
@Path("/api/bridgehead")
public class BridgeheadInterface {
    
    /** The error messages. */
    private OSSEMessage errorMessages;
    
    /** The episode and case form name. */
    private String caseFormName, episodeFormName;
    
    /** This is a bridgehead. */
    private Boolean isBridgehead;
    
    /** The mdr Namespace. */
    private String mdrNS;
    
    /** The status one resource. */
    private Resource statusOneResource = null;

    /**
     * Import patients.
     *
     * @param requestContext the request context
     * @param context the context
     * @param headers the headers
     * @param patientsXML the patients xml
     * @return the response
     */
    @POST
    @Path("/patients")
    @Consumes(MediaType.APPLICATION_XML)
    @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    public Response importPatients(@Context HttpServletRequest requestContext, @Context SecurityContext context,
            @Context HttpHeaders headers, String patientsXML) {
        if (!allowREST())
            return Response.status(Status.FORBIDDEN).build();

        errorMessages = new OSSEMessage();

        Database database = new Database(true);
        JSONResource config = database.getConfig("osse");
        isBridgehead = Utils.isBridgehead(config);

        // check for apikey
        String apikey = Utils.getStringOfJSONResource(config, Vocabulary.Config.Importer.apikey);

        if (apikey != null && !"".equals(apikey)) {
            String givenApiKey = headers.getRequestHeader("apikey") != null ? headers.getRequestHeader("apikey").get(0)
                    : null;
            if (!apikey.equalsIgnoreCase(givenApiKey)) {
                return Response.status(Status.UNAUTHORIZED).build();
            }
        }

        // Fetch the XSD for the import
        JSONResource osseXSDStorage = database.getConfig("osse.xsd.storage");
        String xsd = Utils.getStringOfJSONResource(osseXSDStorage, Vocabulary.Config.specificXSD);
        if (xsd == null) {
            errorMessages.setFinalMessage("No XSD found!");
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(errorMessages).build();
        }

        // validate the xml and create a Document from it
        XMLHelper xmlHelper = new XMLHelper(errorMessages);

        Document dom = xmlHelper.validateXML(xsd, patientsXML);
        errorMessages = xmlHelper.getErrorMessages();

        // validation failed, bail out
        if (dom == null || errorMessages.hasErrorMessages())
            return Response.status(Status.BAD_REQUEST).entity(errorMessages).build();

        // We need some information if this is a bridgehead and the episode and
        // case form names
        caseFormName = config.getProperty(
                Vocabulary.Config.Form.patientStandard).getValue();
        episodeFormName = config.getProperty(
                Vocabulary.Config.Form.visitStandard).getValue();

        Element docEle = dom.getDocumentElement();

        // Read out MDR Namespace
        Element mdr = (Element) docEle.getElementsByTagName("Mdr").item(0);
        mdrNS = mdr.getElementsByTagName("Namespace").item(0).getTextContent();

        NodeList patientNodeList = docEle.getElementsByTagName("BHPatient");
        Integer returnCode = 0;

        Integer counterOverwritten = 0;
        Integer counterNewPatients = 0;
        Integer counterAddPatientData = 0;

        database.beginTransaction();

        for (int i = 0; i < patientNodeList.getLength(); i++) {
            Node nNode = patientNodeList.item(i);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element patient = (Element) nNode;
                Patient patientEntity = new Patient(database);

                String pid = patient.getElementsByTagName("Identifier").item(0).getTextContent();
                Utils.getLogger().debug("Patient PID = " + pid);

                NodeList locations = patient.getElementsByTagName("Location");

                if (locations == null || locations.getLength() == 0) {
                    continue;
                }

                for (int l = 0; l < locations.getLength(); l++) {

                    nNode = locations.item(l);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element location = (Element) nNode;
                        String locationName = location.getAttribute("name");
                        Utils.getLogger().debug("Location " + locationName);

                        Location patientLocation = new Location(database);

                        // on the first run, create the patient and add the
                        // location as the patient's own location
                        if (l == 0) {
                            patientLocation.createOrLoadByName(locationName);

                            // In the bridgehead we want to delete the patient first and
                            // then add in the data
                            // In a registry, we do not want to delete the data but only
                            // overwrite the form data (or add new forms)
                            returnCode = patientEntity.importPatient(pid, patientLocation, isBridgehead);
                        } else {
                            patientLocation = new Location(database);
                            patientLocation.createOrLoadByName(locationName);
                        }

                        // Prepare the case
                        Entity myCase = patientEntity.getCaseOfLocation(patientLocation);
                        if (myCase == null) {
                            if(isBridgehead)
                                myCase = patientEntity.addCaseOfLocation(patientLocation);
                            else {
                                errorMessages.setFinalMessage("Could not find a location with the name '"+locationName+"'.");
                                return Response.status(Status.BAD_REQUEST).entity(errorMessages).build();
                            }
                        }

                        NodeList basicData = location.getElementsByTagName("BasicData");
                        if (basicData == null || basicData.getLength() == 0) {
                            Utils.getLogger().debug("No basic data");
                        } else {
                            int counter = -1;
                            boolean amDone = false;
                            do {
                                String counterText = "";
                                counter++;
                                if(counter>0)
                                    counterText = counter+"";
                                Utils.getLogger().debug("importing form Form"+counterText);
                                amDone = readCaseForms(database, (Case) myCase, (Element) basicData.item(0), counterText);
                            } while(amDone || counter <= 0);
                        }

                        // episodes
                        NodeList episodes = location.getElementsByTagName("Episode");
                        if (episodes == null || episodes.getLength() == 0) {
                            Utils.getLogger().debug("No episodes");
                        }

                        for (int e = 0; e < episodes.getLength(); e++) {
                            Element episode = (Element) episodes.item(e);
                            String episodeName = episode.getAttribute("name");
                            Utils.getLogger().debug("Episode: " + episodeName);

                            Episode episodeEntity = new Episode(database);
                            episodeEntity.createOrLoadByName(episodeName, (Case) myCase);

                            NodeList longitudinalData = episode.getElementsByTagName("LogitudinalData");
                            if (longitudinalData == null || longitudinalData.getLength() == 0) {
                                Utils.getLogger().debug("No longitudinal data");
                            } else {
                                int counter = -1;
                                boolean amDone = false;
                                do {
                                    String counterText = "";
                                    counter++;
                                    if(counter>0)
                                        counterText = counter+"";
                                    Utils.getLogger().debug("importing form Form"+counterText);
                                    amDone = readEpisodeForms(database, episodeEntity, (Element) longitudinalData.item(0), counterText);
                                } while(amDone || counter <= 0);
                                
                            }
                        }
                    }
                }

                if (errorMessages.hasErrorMessages()) {
                    database.rollback();
                    errorMessages.setPatientID(pid);
                    errorMessages
                            .setFinalMessage("The import was interrupted due to validation errors. The following errors occured in the patient "
                                    + pid + ":");
                    return Response.status(Status.BAD_REQUEST).entity(errorMessages).build();
                }

                if (returnCode == -1)
                    counterOverwritten++;
                else if (returnCode == 1)
                    counterNewPatients++;
                else if (returnCode == 0)
                    counterAddPatientData++;
            }
        }

        database.commit();

        // put upload date into DB
        config.setProperty(Vocabulary.Config.uploadStats, new Date().getTime());
        database.saveConfig("osse", config);

        if (counterNewPatients > 0) {
            errorMessages.setFinalMessage(counterNewPatients
                    + " patients were imported, "
                    + counterOverwritten
                    + " patients were overwritten, and "
                    + counterAddPatientData
                    + " patients had data added or forms overwritten.");
            return Response.status(Status.CREATED).entity(errorMessages).build();
        }

        Utils.getLogger().debug("Kill all current user sessions!");
        SessionsKillerSingleton.instance.setDoKillAllSessions(true);

        errorMessages.setFinalMessage(counterOverwritten + " patients were overwritten, and "  
                                    + counterAddPatientData  
                                    + " patients had data added or forms overwritten.");
        return Response.status(Status.OK).entity(errorMessages).build();
    }

    /**
     * Puts a dataelement into a given form and returns the form.
     *
     * @param database the database
     * @param theForm the the form
     * @param mdrKey the mdr key
     * @param entry the entry
     * @return the form
     */
    private Form readDataelement(Database database, Form theForm, String mdrKey, Element entry) {
        Boolean isCaseForm = (theForm.getType() == OSSEVocabulary.Type.CaseForm);

        NodeList entryValues = entry.getElementsByTagName("Value");
        if (entryValues != null && entryValues.getLength() > 0) {
            // repeatable Field

            DatatableRows rows = new DatatableRows();

            if (entryValues != null && entryValues.getLength() > 0) {
                for (int ev = 0; ev < entryValues.getLength(); ev++) {
                    Node nNode = entryValues.item(ev);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element repValue = (Element) nNode;
                        String value = repValue.getTextContent();

                        Utils.getLogger().debug("- " + mdrKey + " = " + value);

                        if (value == null || value.equals("")) {
                            Utils.getLogger().debug("Value is nix, weiter");
                            continue;
                        }

                        try {
                            Object theValue = ImportValidator.convertAndValidate(database, isCaseForm, mdrKey, value,
                                    null);
                            HashMap<String, Object> columns = new HashMap<>();
                            DatatableRow row = new DatatableRow();
                            columns.put(mdrKey, theValue);
                            row.setColumns(columns);
                            rows.add(row);
                        } catch (ImportException e) {
                            errorMessages.addErrorMessage(mdrKey, value, e.getMessage());
                        }
                    }
                }

                theForm.addProperty(mdrKey, rows);
            }
        } else {
            // field
            String value = entry.getTextContent();
            Utils.getLogger().debug(mdrKey + " = " + value);

            try {
                Object theValue = ImportValidator.convertAndValidate(database, isCaseForm, mdrKey, value, null);
                theForm.addProperty(mdrKey, theValue);
            } catch (ImportException e) {
                errorMessages.addErrorMessage(mdrKey, value, e.getMessage());
            }
        }

        return theForm;
    }

    /**
     * Puts a record into a form and returns the form.
     *
     * @param database the database
     * @param theForm the the form
     * @param mdrKey the mdr key
     * @param entry the entry
     * @return the form
     */
    private Form readRecord(Database database, Form theForm, String mdrKey, Element entry) {
        Utils.getLogger().debug("Record " + mdrKey);
        NodeList recordRows = entry.getElementsByTagName("Row");
        if (recordRows == null || recordRows.getLength() == 0) {
            // non repeatable record
            Utils.getLogger().debug("===SINGLE===");
            NodeList recordEntries = entry.getChildNodes();
            for (int re = 0; re < recordEntries.getLength(); re++) {
                Node nNode = recordEntries.item(re);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element recordEntry = (Element) nNode;
                    String recordEntryMdrKey = recordEntry.getNodeName();
                    recordEntryMdrKey = fixMdrKey(mdrNS, recordEntryMdrKey);
                    String value = recordEntry.getTextContent();

                    Utils.getLogger().debug("- " + recordEntryMdrKey + " = " + value);

                    try {
                        Object theValue = ImportValidator.convertAndValidate(database,
                                true, recordEntryMdrKey, value, mdrKey);
                        Utils.getLogger().debug("Field: " + recordEntryMdrKey + " Value: "
                                + value);

                        String finalKey = mdrKey + "/" + recordEntryMdrKey;
                        theForm.addProperty(finalKey, theValue);
                    } catch (ImportException e) {
                        errorMessages.addErrorMessage(mdrKey + " -> " + recordEntryMdrKey, value, e.getMessage());
                    }
                }
            }
        } else {
            // repeatable record
            DatatableRows rows = new DatatableRows();

            for (int rr = 0; rr < recordRows.getLength(); rr++) {
                Utils.getLogger().debug("===ROW===");
                DatatableRow row = new DatatableRow();
                HashMap<String, Object> columns = new HashMap<>();

                Node nNode = recordRows.item(rr);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element aRow = (Element) nNode;
                    NodeList recordEntries = aRow.getChildNodes();

                    for (int re = 0; re < recordEntries.getLength(); re++) {
                        nNode = recordEntries.item(re);

                        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element recordEntry = (Element) nNode;
                            String recordEntryMdrKey = recordEntry.getNodeName();
                            recordEntryMdrKey = fixMdrKey(mdrNS, recordEntryMdrKey);

                            String value = recordEntry.getTextContent();

                            Utils.getLogger().debug("- " + recordEntryMdrKey + " = " + value);

                            try {
                                Object theValue = ImportValidator.convertAndValidate(
                                        database, true, recordEntryMdrKey,
                                        value, mdrKey);
                                Utils.getLogger().debug("Field: " + recordEntryMdrKey
                                        + " Value: " + value);
                                columns.put(recordEntryMdrKey, theValue);
                            } catch (ImportException e) {
                                Utils.getLogger().debug("ERRORVALUE = '" + value + "'");
                                errorMessages.addErrorMessage(mdrKey + " -> " + recordEntryMdrKey, value,
                                        e.getMessage());
                            }
                        }
                    }
                }
                row.setColumns(columns);
                rows.add(row);
            }

            theForm.addProperty(mdrKey, rows);
        }

        return theForm;
    }

    /**
     * fills a form and saves it to the backend.
     *
     * @param database the database
     * @param theForm the the form
     * @param formEntries the form entries
     */
    private void readForm(Database database, Form theForm, NodeList formEntries) {
        for (int fe = 0; fe < formEntries.getLength(); fe++) {
            Node nNode = formEntries.item(fe);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element entry = (Element) nNode;
                String mdrKey = entry.getNodeName();

                mdrKey = fixMdrKey(mdrNS, mdrKey);

                if (mdrKey.contains("dataelement")) {
                    // dataelement
                    theForm = readDataelement(database, theForm, mdrKey, entry);

                } else {

                    theForm = readRecord(database, theForm, mdrKey, entry);
                }
            }
        }
        theForm.saveOrUpdate();
    }

    /**
     * fills an episode form with values and saves it to the backend.
     *
     * @param database the database
     * @param myEpisode the my episode
     * @param dataElement the data element
     * @param counterText the counter text
     * @return true, if successful
     */
    private boolean readEpisodeForms(Database database, Episode myEpisode, Element dataElement, String counterText) {
        NodeList forms = dataElement.getElementsByTagName("Form"+counterText);
        
        if(forms == null || forms.getLength() == 0)
            return false;
        
        for (int f = 0; f < forms.getLength(); f++) {
            Node nNode = forms.item(f);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element form = (Element) nNode;
                String formName = form.getAttribute("name");
                Utils.getLogger().debug("Form " + formName);

                if (isBridgehead) {
                    // The bridgehead has only one fixed episode form, so all
                    // data will be put into it
                    // no matter what form names the xml brings along
                    Utils.getLogger().debug(
                            "XML has episode form name " + formName + ". We are a bridgehead, so putting it into "
                                    + episodeFormName);
                    formName = episodeFormName;
                }

                EpisodeForm episodeForm = (EpisodeForm) myEpisode.getForm(formName);
                if(episodeForm == null) {
                    episodeForm = new EpisodeForm(database);
                } else {
                    episodeForm.load();
                    episodeForm.deleteFormMedicalData();
                }
                
                // Prepare the caseForm
                episodeForm.setProperty(OSSEVocabulary.EpisodeForm.Name, formName);
                Resource statusRes = getStatusOneResource(database);
                episodeForm.setProperty(OSSEVocabulary.EpisodeForm.Status, statusRes);
                episodeForm.setProperty(OSSEVocabulary.EpisodeForm.Version, 1);
                episodeForm.setProperty(OSSEVocabulary.EpisodeForm.Episode, myEpisode.getResource());

                Date lastChangedTime = Calendar.getInstance().getTime();
                User systemUser = new User(database, "user:1");
                systemUser.load(false, true);
                episodeForm.setProperty(Vocabulary.attributes.lastChangedBy, systemUser.getResource());
                episodeForm.setProperty(Vocabulary.attributes.lastChangedDate, lastChangedTime);

                NodeList formEntries = form.getChildNodes();

                readForm(database, episodeForm, formEntries);

                if (errorMessages.hasErrorMessages()) {
                    return true;
                }
            }
        }
        
        return true;
    }

    /**
     * fills a case form with values and saves it to the backend.
     *
     * @param database the database
     * @param myCase the my case
     * @param dataElement the data element
     * @param counterText the counter text
     * @return true, if successful
     */
    private boolean readCaseForms(Database database, Case myCase, Element dataElement, String counterText) {
        NodeList forms = dataElement.getElementsByTagName("Form"+counterText);
        if(forms == null || forms.getLength() == 0)
            return false;
        
        for (int f = 0; f < forms.getLength(); f++) {
            Node nNode = forms.item(f);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element form = (Element) nNode;
                String formName = form.getAttribute("name");
                Utils.getLogger().debug("Form " + formName);

                if (isBridgehead) {
                    // The bridgehead has only one fixed case form, so all data
                    // will be put into it
                    // no matter what form names the xml brings along
                    Utils.getLogger().debug(
                            "XML has case form name " + formName + ". We are a bridgehead, so putting it into "
                                    + caseFormName);
                    formName = caseFormName;
                }

                CaseForm caseForm = (CaseForm) myCase.getForm(formName);
                if(caseForm == null) {
                    caseForm = new CaseForm(database);
                } else {
                    caseForm.load();
                    caseForm.deleteFormMedicalData();
                }
                
                // Prepare the caseForm
                caseForm.setProperty(OSSEVocabulary.CaseForm.Name, formName);
                Resource statusRes = getStatusOneResource(database);
                caseForm.setProperty(OSSEVocabulary.CaseForm.Status, statusRes);
                caseForm.setProperty(OSSEVocabulary.CaseForm.Version, 1);
                caseForm.setProperty(OSSEVocabulary.CaseForm.Case, myCase.getResource());

                Date lastChangedTime = Calendar.getInstance().getTime();
                User systemUser = new User(database, "user:1");
                systemUser.load(false, true);
                caseForm.setProperty(Vocabulary.attributes.lastChangedBy, systemUser.getResource());
                caseForm.setProperty(Vocabulary.attributes.lastChangedDate, lastChangedTime);
                
                NodeList formEntries = form.getChildNodes();

                readForm(database, caseForm, formEntries);

                if (errorMessages.hasErrorMessages()) {
                    return true;
                }
            }
        }
        
        return true;
    }

    /**
     * Gets the resource for "status = 1 = open".
     *
     * @param database the database
     * @return the status one resource
     */
    private Resource getStatusOneResource(Database database) {
        if (statusOneResource == null) {
            statusOneResource = Utils.findResourceByPropertyInDatabase(
                    database, OSSEVocabulary.Type.Status, OSSEVocabulary.ID,
                    "1");
        }

        return statusOneResource;
    }

    /**
     * REST interface to completely delete a patient (this is a true delete).
     *
     * @param pseudonym the pseudonym
     * @return the response
     */
    @DELETE
    @Path("/patients/{pseudonym}")
    public Response deletePatient(@PathParam("pseudonym") String pseudonym) {
        if (!allowREST())
            return Response.status(Status.FORBIDDEN).build();

        Database database = new Database(true);
        Patient patientEntity = new Patient(database);
        Resource patientResource = patientEntity.entityExistsByProperty("patientID", pseudonym);

        if (patientResource != null) {
            patientEntity.setResource(patientResource);

            database.beginTransaction();
            patientEntity.wipeoutPatient();
            database.commit();

            return Response.ok().build();
        }
        else {
            errorMessages = new OSSEMessage();
            errorMessages.setFinalMessage("The patient with the ID " + pseudonym + " could not be deleted.");
            return Response.status(Status.BAD_REQUEST).entity(errorMessages).build();
        }
    }

    /**
     * Reads status information of the bridgehead.
     *
     * @return A map with status information on software version.
     */
    private Map<String, String> getStatusInformation() {
        Database database = new Database(true);
        JSONResource config = database.getConfig("osse");

        Map<String, String> out = new HashMap<String, String>();

        Value version = config.getProperty(Vocabulary.Config.version);
        if (version == null)
            out.put("version", "1.0.0");
        else
            out.put("version", version.getValue());

        Value uploadStats = config.getProperty(Vocabulary.Config.uploadStats);
        if (uploadStats != null) {
            Date date = new Date(uploadStats.asLong());
            // we need a date format like: Tue, 15 Nov 1994 08:12:31 GMT
            String datePattern = "EEE, dd MMM yyyy HH:mm:ss zzz";
            SimpleDateFormat sdf = new SimpleDateFormat(datePattern);
            out.put("uploadStats", sdf.format(date));
        }
        else {
            out.put("uploadStats", "No upload yet");
        }

        return out;
    }

    /**
     * Output status information as JSON.
     *
     * @return A JSON object with the following members:
     *         <ul>
     *         <li>version: Software version of this Bridgehead instance.</li>
     *         </ul>
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JSONObject helloJSON() {
        Map<String, String> versionMap = new HashMap<>();
        versionMap.put("version", getStatusInformation().get("version"));
        return new JSONObject(versionMap);
    }

    /**
     * Output status information as XML.
     *
     * @return A XML containing the software version of this Bridgehead
     *         instance. Example: <root> <version>1.1.2</version> </root>
     * @throws ParserConfigurationException the parser configuration exception
     */
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public String helloXML() throws ParserConfigurationException {
        Map<String, String> versionMap = new HashMap<>();
        versionMap.put("version", getStatusInformation().get("version"));

        XStream magicApi = new XStream();
        magicApi.registerConverter(new MapEntryConverter());
        magicApi.alias("root", Map.class);

        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        xml += magicApi.toXML(versionMap);

        return xml;
    }

    /**
     * Output status information as text.
     *
     * @return A status message containing the software version of this
     *         Bridgehead instance.
     */
    @GET
    @Produces({ MediaType.TEXT_HTML, MediaType.TEXT_PLAIN })
    public String helloHTML() {
        return String.format("This is OSSE-Bridgehead running version %s.",
                getStatusInformation().get("version"));
    }

    /**
     * Get the timestamp for the last successful upload to determine which
     * patients have to be uploaded. Output as JSON.
     *
     * @return A JSON object with the following members:
     *         <ul>
     *         <li>LastUploadTimestamp: Timestamp of the last upload</li>
     *         </ul>
     */
    @GET
    @Path("/uploadStats")
    @Produces(MediaType.APPLICATION_JSON)
    public JSONObject getUploadStatsJSON() {
        Map<String, String> versionMap = new HashMap<>();
        versionMap.put("LastUploadTimestamp", getStatusInformation().get("uploadStats"));
        return new JSONObject(versionMap);
    }

    /**
     * Get the timestamp for the last successful upload to determine which
     * patients have to be uploaded. Output as XML.
     *
     * @return A XML containing the software version of this Bridgehead
     *         instance. Example: <Uploadstats> <LastUploadTimestamp>Tue, 15 Nov
     *         1994 08:12:31 GMT</LastUploadTimestamp> </Uploadstats>
     * @throws ParserConfigurationException the parser configuration exception
     */
    @GET
    @Path("/uploadStats")
    @Produces(MediaType.APPLICATION_XML)
    public String getUploadStatsXML() throws ParserConfigurationException {
        Map<String, String> versionMap = new HashMap<>();
        versionMap.put("LastUploadTimestamp", getStatusInformation().get("uploadStats"));

        XStream magicApi = new XStream();
        magicApi.registerConverter(new MapEntryConverter());
        magicApi.alias("Uploadstats", Map.class);

        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        xml += magicApi.toXML(versionMap);

        return xml;
    }

    /**
     * Transforms XML-MDRKey fragments with underscores to MDRKeys.
     *
     * @param mdrNS            the MDR Namespace
     * @param mdrKey            the XML-MDRKey fragment
     * @return the string
     */
    private String fixMdrKey(String mdrNS, String mdrKey) {
        mdrKey = mdrKey.replace("_", ":");
        mdrKey = Utils.lowerCaseFirstChar(mdrKey);

        mdrKey = mdrNS + ":" + mdrKey;

        return mdrKey;
    }

    /**
     * Checks if this REST interface shall allow access currently it's only for
     * the OSSE Bridgehead.
     *
     * @return the boolean
     */
    protected Boolean allowREST() {
        Database database = new Database(true);
        JSONResource config = database.getConfig("osse");

        if (!Utils.isBridgehead(config))
            return false;

        return true;
    }
}
