/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.osse.bridgehead;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.samply.common.mdrclient.domain.EnumValidationType;
import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.osse.bridgehead.exception.ImportException;
import de.samply.edc.osse.control.Database;
import de.samply.edc.utils.DateAndTimePatterns;
import de.samply.store.JSONResource;
import de.samply.store.StringLiteral;
import de.samply.store.Value;

/**
 * Validates a data import, if the values are valid for the given mdr keys.
 */
public class ImportValidator implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**  special range regexp used in mdr. */
    public static final String FLOAT_RANGE_REGEX = "(?:(.+)<=)?x(?:<=(.+))?";

    /**
     * Checks if a value is an integer, and if it's between a min and max if
     * given.
     *
     * @param value            The value to check
     * @param min            min value|null
     * @param max            max value|null
     * @return boolean
     */
    public static Boolean validInteger(String value, String min, String max) {
        try {
            Integer floatValue = Integer.valueOf(String.valueOf(value));

            if (min == null)
                return true;

            Integer minFloat = Integer.valueOf(min);
            Integer maxFloat = Integer.valueOf(max);

            if (floatValue >= minFloat && floatValue <= maxFloat) {
                return true;
            }
        } catch (NumberFormatException e) {
            return false;
        }

        return false;
    }

    /**
     * Checks if a value is a float, and if it's between a min and max if given.
     *
     * @param value            The value to check
     * @param min            min value|null
     * @param max            max value|null
     * @return boolean
     */
    public static Boolean validFloat(String value, String min, String max) {
        try {
            Float floatValue = Float.valueOf(String.valueOf(value));

            if (min == null)
                return true;

            Integer minFloat = Integer.valueOf(min);
            Integer maxFloat = Integer.valueOf(max);

            if (floatValue >= minFloat && floatValue <= maxFloat) {
                return true;
            }
        } catch (NumberFormatException e) {
            return false;
        }

        return false;
    }

    /**
     * Checks if a value is a boolean.
     *
     * @param value the value
     * @return the boolean
     */
    public static Boolean validBoolean(String value) {
        if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false"))
            return true;

        return false;
    }

    /**
     * Validates the value and converts it into the according class.
     *
     * @param database            database wrapper
     * @param isCaseForm            If or not it is supposed to be in a case form
     * @param mdrKey            the MDRKey
     * @param value            the value
     * @param recordMdrKey the record mdr key
     * @return returns the value as string, integer, float, or boolean. Dates
     *         are yet returned as string until the databinding is fixed in the
     *         MDRfaces
     * @throws ImportException the import exception
     */
    @SuppressWarnings("unused")
    public static Object convertAndValidate(Database database, Boolean isCaseForm, String mdrKey, String value,
            String recordMdrKey) throws ImportException {
        // TODO: This also has to check for the formname

        database.getConfig("osse");

        JSONResource mdrEntityHasValidation = (JSONResource) database.getConfig("osse").getProperty(
                Vocabulary.Config.mdrEntityHasValidation);

        // existance check of the entry
        JSONResource mdrEntityIsInForm = (JSONResource) database.getConfig("osse").getProperty(
                Vocabulary.Config.mdrEntityIsInForm);

        Value doesExist = mdrEntityIsInForm.getProperty(mdrKey);
        String formType = null;

        if (recordMdrKey == null)
        {
            // not part of a record
            if (doesExist == null) {
                throw new ImportException("The MDR key " + mdrKey + " does not exist in any form.");
            }
            formType = doesExist.asJSONResource().getProperty("formType").getValue();

        } else {
            // part of a record
            doesExist = mdrEntityIsInForm.getProperty(recordMdrKey);

            if (doesExist == null) {
                throw new ImportException("The MDR record " + recordMdrKey + " does not exist in any form.");
            }
            formType = doesExist.asJSONResource().getProperty("formType").getValue();
        }

        if (isCaseForm && !"CASE".equalsIgnoreCase(formType))
        {
            throw new ImportException("The MDR key " + mdrKey + " is not expected to be in a case form.");
        }

        if (!isCaseForm && !"EPISODE".equalsIgnoreCase(formType))
        {
            throw new ImportException("The MDR key " + mdrKey + " is not expected to be in an episode form.");
        }

        Value validationData = mdrEntityHasValidation.getProperty(mdrKey);
        if (validationData != null && validationData instanceof JSONResource) {
            String validationType = ((JSONResource) validationData).getProperty("type").getValue();

            if (validationType.equalsIgnoreCase("permissibleValues")) {
                if (((JSONResource) validationData).getProperties("permissibleValue")
                        .contains(new StringLiteral(value))) {
                    return value;
                } else {
                    throw new ImportException("The given value is non-permissible.");
                }
            }
            else if (validationType.equalsIgnoreCase(EnumValidationType.INTEGERRANGE.name())) {
                String min = ((JSONResource) validationData).getProperty("min").getValue();
                String max = ((JSONResource) validationData).getProperty("max").getValue();
                if (!validInteger(value, min, max))
                    throw new ImportException(((JSONResource) validationData).getProperty("error").getValue());
                else
                    return Integer.parseInt(value);
            } else if (validationType.equalsIgnoreCase(EnumValidationType.FLOATRANGE.name())) {
                String min = ((JSONResource) validationData).getProperty("min").getValue();
                String max = ((JSONResource) validationData).getProperty("max").getValue();
                if (!validFloat(value, min, max))
                    throw new ImportException(((JSONResource) validationData).getProperty("error").getValue());
                else
                    return Float.parseFloat(value);
            } else if (validationType.equals(EnumValidationType.REGEX.name())) {
                String regexp = ((JSONResource) validationData).getProperty("regexp").getValue();
                Pattern pattern = Pattern.compile(regexp);
                Matcher matcher = pattern.matcher(value.toString());
                if (!matcher.matches()) {
                    throw new ImportException(((JSONResource) validationData).getProperty("error").getValue());
                } else
                    return value;
            } else if (validationType.equalsIgnoreCase(EnumValidationType.INTEGER.name())) {
                if (!validInteger(value, null, null))
                    throw new ImportException(((JSONResource) validationData).getProperty("error").getValue());
                else
                    return Integer.parseInt(value);
            } else if (validationType.equalsIgnoreCase(EnumValidationType.FLOAT.name())) {
                if (!validFloat(value, null, null))
                    throw new ImportException(((JSONResource) validationData).getProperty("error").getValue());
                else
                    return Float.parseFloat(value);
            } else if (validationType.equalsIgnoreCase(EnumValidationType.BOOLEAN.name())) {
                if (!validBoolean(value))
                    throw new ImportException(((JSONResource) validationData).getProperty("error").getValue());
                else
                    return Boolean.parseBoolean(value);
            } else if (validationType.equalsIgnoreCase(EnumValidationType.DATE.name())) {
                String enumDateFormat = ((JSONResource) validationData).getProperty("enumDateFormat").getValue();
                String datePattern = DateAndTimePatterns.getDatePattern(enumDateFormat);
                SimpleDateFormat sdf = new SimpleDateFormat(datePattern);
                sdf.setLenient(false);
                try {
                    // if not valid, it will throw ParseException
                    Date date = sdf.parse(value.toString());
                    return value;
                } catch (ParseException e) {
                    throw new ImportException(((JSONResource) validationData).getProperty("error").getValue());
                }
            } else if (validationType.equalsIgnoreCase(EnumValidationType.TIME.name())) {
                String enumTimeFormat = ((JSONResource) validationData).getProperty("enumTimeFormat").getValue();
                String datePattern = DateAndTimePatterns.getTimePattern(enumTimeFormat);
                SimpleDateFormat sdf = new SimpleDateFormat(datePattern);
                sdf.setLenient(false);
                try {
                    // if not valid, it will throw ParseException
                    Date date = sdf.parse(value.toString());
                    return value;
                } catch (ParseException e) {
                    throw new ImportException(((JSONResource) validationData).getProperty("error").getValue());
                }
            } else if (validationType.equalsIgnoreCase(EnumValidationType.DATETIME.name())) {
                String enumTimeFormat = ((JSONResource) validationData).getProperty("enumTimeFormat").getValue();
                String enumDateFormat = ((JSONResource) validationData).getProperty("enumDateFormat").getValue();
                String dateTimePattern = DateAndTimePatterns.getDateTimePattern(enumDateFormat, enumTimeFormat);
                SimpleDateFormat sdf = new SimpleDateFormat(dateTimePattern);
                sdf.setLenient(false);

                try {
                    // if not valid, it will throw ParseException
                    Date date = sdf.parse(value.toString());
                    return value;
                } catch (ParseException e) {
                    throw new ImportException(((JSONResource) validationData).getProperty("error").getValue());
                }
            }
        }

        return value;
    }

}
