/*
 * Copyright (c) 2017 MIG Frankfurt
 */
package de.samply.edc.osse.upgrade;

import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.osse.upgrade.dto.Upgrades.Upgrade;
import de.samply.edc.utils.VersionNumber;
import de.samply.store.JSONResource;

/**
 * 1.4.2 -> 1.4.3
 *
 * See CHANGELOG.md for details.
 */
public class UpgradeExecution16 extends UpgradeExecution {

    /**
     * Instantiates a new upgrade execution.
     *
     * @param upgradeData
     *            the upgrade data
     * @param currentOSSEConfig
     *            the current osse config
     * @param applicationBean
     *            the application bean
     */
    public UpgradeExecution16(Upgrade upgradeData, JSONResource currentOSSEConfig, ApplicationBean applicationBean) {
        super(upgradeData, currentOSSEConfig, applicationBean);

        fromVersion = new VersionNumber("1.4.2");
        toVersion = new VersionNumber("1.4.3");
    }

    /**
     * Instantiates a new upgrade execution.
     *
     * @param currentOSSEConfig
     *            the current osse config
     * @param applicationBean
     *            the application bean
     */
    public UpgradeExecution16(JSONResource currentOSSEConfig, ApplicationBean applicationBean) {
        super(currentOSSEConfig, applicationBean);

        fromVersion = new VersionNumber("1.4.2");
        toVersion = new VersionNumber("1.4.3");
    }

    /**
     * Do pre upgrade.
     *
     * @return the boolean
     * @see UpgradeExecution#doPreUpgrade()
     */
    @Override
    public Boolean doPreUpgrade() {
        return true;
    }

    @Override
    public void doPostUpgrade() {
    }
}
