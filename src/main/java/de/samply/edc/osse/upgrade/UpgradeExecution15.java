/*
 * Copyright (c) 2017 MIG Frankfurt
 */
package de.samply.edc.osse.upgrade;

import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.osse.upgrade.dto.Upgrades.Upgrade;
import de.samply.edc.utils.VersionNumber;
import de.samply.store.JSONResource;

/**
 * 1.4.1 -> 1.4.2
 *
 * See CHANGELOG.md for details.
 */
public class UpgradeExecution15 extends UpgradeExecution {

    /**
     * Instantiates a new upgrade execution.
     *
     * @param upgradeData
     *            the upgrade data
     * @param currentOSSEConfig
     *            the current osse config
     * @param applicationBean
     *            the application bean
     */
    public UpgradeExecution15(Upgrade upgradeData, JSONResource currentOSSEConfig, ApplicationBean applicationBean) {
        super(upgradeData, currentOSSEConfig, applicationBean);

        fromVersion = new VersionNumber("1.4.1");
        toVersion = new VersionNumber("1.4.2");
    }

    /**
     * Instantiates a new upgrade execution.
     *
     * @param currentOSSEConfig
     *            the current osse config
     * @param applicationBean
     *            the application bean
     */
    public UpgradeExecution15(JSONResource currentOSSEConfig, ApplicationBean applicationBean) {
        super(currentOSSEConfig, applicationBean);

        fromVersion = new VersionNumber("1.4.1");
        toVersion = new VersionNumber("1.4.2");
    }

    /**
     * Do pre upgrade.
     *
     * @return the boolean
     * @see UpgradeExecution#doPreUpgrade()
     */
    @Override
    public Boolean doPreUpgrade() {
        return true;
    }

    @Override
    public void doPostUpgrade() {
    }
}
