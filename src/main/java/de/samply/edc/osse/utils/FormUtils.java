/*
 * Copyright (c) 2017 MIG Frankfurt
 */
package de.samply.edc.osse.utils;

import de.samply.edc.osse.dto.formeditor.FormDetails;
import de.samply.edc.utils.Utils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.Serializable;

/**
 * Various supporting methods for handling forms in OSSE.
 */
public class FormUtils implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Construct a full form xml file path for a specific form.
     *
     * @param formId      The id of the form to load.
     * @param formVersion The version number of the form to load.
     * @return Full path of the form's xml file.
     */
    public static String getFormXmlFilePath(long formId, long formVersion) {
        return Utils.getRealPath("/forms/xml_" + formId + "_ver-" + formVersion + ".xml");
    }

    /**
     * Loads the xml file of a specific form and returns the form's definition.
     *
     * @param formId      The id of the form to load.
     * @param formVersion The version number of the form to load.
     * @return The form's full definition stored in the xml file.
     */
    public static FormDetails loadFormFromXML(long formId, long formVersion) {
        FormDetails formDetails = null;

        String fileName = FormUtils.getFormXmlFilePath(formId, formVersion);
        File file = new File(fileName);
        if (file.exists()) {
            try {
                JAXBContext jaxbContext = JAXBContext.newInstance(FormDetails.class);
                Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                formDetails = (FormDetails) unmarshaller.unmarshal(file);
            } catch (JAXBException e) {
                Utils.getLogger().error("Loading form XML failed. Form ID: " + formId + " Form version: " + formVersion, e);
            }
        } else {
            Utils.getLogger().error("Loading form XML failed, file does not exist. Form ID: " + formId + " Form version: " + formVersion);
        }

        return formDetails;
    }

    /**
     * Retrieves a specific translation from a form object.
     *
     * @param formDetails The form definition.
     * @param locale      The ISO 639-1 code of the requested language.
     * @return The form translation.
     */
    public static FormDetails.FormI18n getFormI18n(FormDetails formDetails, String locale) {
        FormDetails.FormI18n result = null;

        for (FormDetails.FormI18n formI18n : formDetails.getI18ns()) {
            if (formI18n.getLanguage().equals(locale)) {
                result = formI18n;
                break;
            }
        }

        return result;
    }

    /**
     * Retrieves a specific item from a form definition.
     *
     * @param formDetails  The form definition.
     * @param itemPosition The numeric position of the item within the form to fetch.
     * @return The form item definition.
     */
    public static FormDetails.Item getFormItem(FormDetails formDetails, int itemPosition) {
        FormDetails.Item result = null;

        for (FormDetails.Item item : formDetails.getItems()) {
            if (item.getPosition() == itemPosition) {
                result = item;
                break;
            }
        }

        return result;
    }

    /**
     *
     *
     * @param formDetails  The form definition.
     * @param itemPosition The numeric position of the item within the form to fetch.
     * @param locale       The ISO 639-1 code of the requested language.
     * @return The form item translation object.
     */
    public static FormDetails.Item.ItemI18n getFormItemI18n(FormDetails formDetails, int itemPosition, String locale) {
        FormDetails.Item.ItemI18n result = null;

        for (FormDetails.Item.ItemI18n itemI18n : getFormItem(formDetails, itemPosition).getI18ns()) {
            if (itemI18n.getLanguage().equals(locale)) {
                result = itemI18n;
                break;
            }
        }

        return result;
    }

}
