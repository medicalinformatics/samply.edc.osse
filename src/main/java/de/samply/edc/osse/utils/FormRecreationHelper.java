/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.osse.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sun.jersey.api.client.Client;

import de.samply.auth.client.jwt.JWTException;
import de.samply.auth.rest.AccessTokenDTO;
import de.samply.common.http.HttpConnector43;
import de.samply.common.http.HttpConnectorException;
import de.samply.common.mdrclient.MdrClient;
import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.osse.auth.Auth;
import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.osse.control.Database;
import de.samply.edc.osse.exceptions.OSSEException;
import de.samply.edc.osse.model.MdrKeyUsageData;
import de.samply.edc.utils.Utils;
import de.samply.store.JSONResource;
import de.samply.store.exceptions.DatabaseException;

/**
 * Helper for a complete forms recreation.
 */
public class FormRecreationHelper {

    /** The application bean. */
    private ApplicationBean applicationBean;
    /** The database config file name. */
    protected String databaseConfigFileName = "backend.xml";

    /** The database config file with full path. */
    protected String databaseConfigFile;

    /**  Helper for imports. */
    private FormImportHelper fih;

    /**  The access token to access the MDR. */
    private AccessTokenDTO accessToken;

    /**  jersey clients for form repository and mdr communication. */
    private Client frJerseyClient, mdrJerseyClient;

    /**  The form storage. */
    private JSONResource formStorage = new JSONResource();

    /**
     * Instantiates a new form recreation helper.
     *
     * @param applicationBean the application bean
     */
    public FormRecreationHelper(ApplicationBean applicationBean) {
        this.applicationBean = applicationBean;
        databaseConfigFile = Utils.findConfigurationFile(databaseConfigFileName);

        fih = new FormImportHelper(applicationBean.getConfig());
    }

    /**
     * Recreates a certain form, old versions will be deleted and replaced.
     *
     * @param formName            the name of the form
     * @param episodeForm the episode form
     * @param isArchivedForm the is archived form
     * @return true, if successful
     * @throws FileNotFoundException the file not found exception
     */
    private boolean doRecreateForm(String formName, Boolean episodeForm, Boolean isArchivedForm) throws FileNotFoundException {
        String saveFileName = "/forms/" + formName + ".xhtml";
        String saveFileNamePure = saveFileName;
        saveFileName = Utils.getRealPath(saveFileName);
        File file = new File(saveFileName);

        if (formStorage.getProperty(saveFileNamePure) != null) {
            Utils.getLogger().debug("This form was stored, removing it.");
            formStorage.removeProperties(saveFileNamePure);
        }

        if (file.exists()) {
            Utils.getLogger().debug("Form " + formName + " exists as form in " + saveFileName + ". Deleting it.");
            file.delete();
        }

        Pattern pattern = Pattern.compile("^form_([^-]+)_ver-([^-]+)$");
        Matcher matcher = pattern.matcher(formName);
        if (!matcher.find()) {
            Utils.getLogger().debug("Formname " + formName + " does not match pattern.");
            return false;
        }

        String mdrURL = (String) fih.getConfiguration().getProperty(Vocabulary.Config.MDR.REST);
        MdrClient mdrClient = new MdrClient(mdrURL, mdrJerseyClient);

        formStorage = fih.doImport(frJerseyClient, mdrClient, accessToken, matcher.group(1), matcher.group(2), episodeForm,
                formStorage, true, isArchivedForm);

        return true;
    }

    /**
     * Reworks all forms and updates already saved data to the new
     * non-repeatable saving strategy.
     *
     * @return boolean
     * @throws DatabaseException the database exception
     */
    public Boolean recreateForms() throws DatabaseException {
        Database database = new Database(true);
        try {
            // We call this to verify the system role was selectable and is
            // valid.
            // If not, a null pointer exception is thrown
            database.getDatabaseModel();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        JSONResource osseFormStorage = applicationBean.getOSSEFormStorage();

        if (osseFormStorage.getProperty(Vocabulary.Config.Form.storage) != null) {
            formStorage = osseFormStorage.getProperty(Vocabulary.Config.Form.storage).asJSONResource();
        }

        fih.clearData();

        if (osseFormStorage.getProperty(Vocabulary.Config.Form.formNameMatrix) != null) {
            fih.setFormNameMatrix(osseFormStorage.getProperty(Vocabulary.Config.Form.formNameMatrix).asJSONResource());
        }

        HttpConnector43 httpConnector = null;
        Client client = null;

        try {
            httpConnector = new HttpConnector43(applicationBean.getConfig());
            // Get a HTTPS jersey client (AUTH is always https)
            client = httpConnector.getJerseyClientForHTTPS(false);
            accessToken = Auth.getAccessToken(client, applicationBean.getConfig());
            client.destroy();
        } catch (HttpConnectorException e1) {
            e1.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        } catch (JWTException e) {
            e.printStackTrace();
        }

        if (httpConnector == null) {
            Utils.getLogger().error("HTTPclient could not be started");
            return false;
        }

        if (accessToken == null) {
            Utils.getLogger().error("No access token could be gained");
            return false;
        }

        // Get a Jersey client for the FormRepository
        String urlFR = applicationBean.getConfig().getProperty(Vocabulary.Config.FormEditor.formEditorBASE) + "";
        frJerseyClient = httpConnector.getJerseyClient(urlFR, false);

        // Get a Jersey client for the MDR
        String urlMDR = applicationBean.getConfig().getProperty(Vocabulary.Config.MDR.REST) + "";
        mdrJerseyClient = httpConnector.getJerseyClient(urlMDR, false);

        // 1) rework all forms
        try {
            for (String formName : applicationBean.getArchivedFormulars().get("en").keySet()) {
                if (!doRecreateForm(formName, false, true))
                    return false;
            }

            for (String formName : applicationBean.getFormulars().get("en").keySet()) {
                if (!doRecreateForm(formName, false, false))
                    return false;
            }

            for (String formName : applicationBean.getArchivedVisitFormulars().get("en").keySet()) {
                if (!doRecreateForm(formName, true, true))
                    return false;
            }

            for (String formName : applicationBean.getVisitFormulars().get("en").keySet()) {
                if (!doRecreateForm(formName, true, false))
                    return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        osseFormStorage.setProperty(Vocabulary.Config.Form.storage, formStorage);

        database.saveConfig("osse.form.storage", osseFormStorage);

        JSONResource osseConfig = applicationBean.getOSSEConfig();

        osseConfig.setProperty(Vocabulary.Config.mdrEntityIsInForm, fih.getMdrKeyUsageStore().asJSONResource());

        JSONResource recordHasMdrEntities = new JSONResource();
        for (String key : fih.getRecordHasMdrEntities().keySet()) {
            LinkedHashSet<String> moo = fih.getRecordHasMdrEntities().get(key);
            for (String me : moo)
                recordHasMdrEntities.addProperty(key, me);
        }
        osseConfig.setProperty(Vocabulary.Config.recordHasMdrEntities, recordHasMdrEntities);

        JSONResource mdrs = new JSONResource();
        for (String key : fih.getWarnDoubleUsage().keySet()) {
            Set<MdrKeyUsageData> moo = fih.getWarnDoubleUsage().get(key);
            for (MdrKeyUsageData me : moo)
                mdrs.addProperty(key, me.asJSONResource());
        }
        osseConfig.setProperty(Vocabulary.Config.warnDoubleMDRKeyUsage, mdrs);

        MdrClient mdrClient = new MdrClient(urlMDR, mdrJerseyClient);
        try {
            fih.fillMdrEntityHasValidation(mdrClient, accessToken);
        } catch (OSSEException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        osseConfig.setProperty(Vocabulary.Config.mdrEntityHasValidation, fih.getMdrEntityHasValidation());

        database.saveConfig("osse", osseConfig);

        JSONResource osseXSDStorage = applicationBean.getOSSEXSDStorage();
        if (osseXSDStorage == null)
            osseXSDStorage = new JSONResource();
        try {
            String specificXSD = fih.makeV2XSD(mdrClient, accessToken);
            osseXSDStorage.setProperty(Vocabulary.Config.specificXSD, specificXSD);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        database.saveConfig("osse.xsd.storage", osseXSDStorage);
        
        Utils.getLogger().debug("Forms redone.");

        return true;
    }
}
