/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.osse.utils;

import java.io.Serializable;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import com.sun.jersey.api.client.Client;

import de.samply.auth.client.jwt.JWTException;
import de.samply.auth.rest.AccessTokenDTO;
import de.samply.common.http.HttpConnector43;
import de.samply.common.http.HttpConnectorException;
import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.osse.auth.Auth;
import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.osse.control.Database;
import de.samply.edc.utils.Utils;
import de.samply.jsf.JsfUtils;
import de.samply.jsf.LoggedUser;
import de.samply.store.Resource;
import de.samply.store.osse.OSSEVocabulary;

/**
 * General Utils class with OSSE helper methods.
 */
public abstract class OSSEUtils implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Gets the callback URL to the registry, used by Mainzelliste callbacks. In
     * case the Mainzelliste has to use an localhost port that is different from
     * the way a user accesses the registry
     *
     * @return the callback url to registry
     */
    public static String getCallbackURLToRegistry() {
        String webPath = Utils.generateUrlToWebservice();
        String port = ((ApplicationBean) Utils.getAB())
                .getInternalMainzellistePort();
        String path = "";

        if (port == null || "".equals(port))
            path = webPath;
        else
            path = "http://localhost:"
                    + ((ApplicationBean) Utils.getAB())
                            .getInternalMainzellistePort()
                    + FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();

        return path;
    }

    /**
     * Gets a list of location as select items.
     *
     * @param database the database
     * @return the locations as select items
     */
    public static List<SelectItem> getLocationsAsSelectItems(Database database) {
        List<SelectItem> locationsSelectItems = new ArrayList<>();

        List<Resource> groupList = database.getLocations(null);

        SelectItem item;

        for (Resource group : groupList) {
            item = new SelectItem();

            item.setLabel(group.getProperty(OSSEVocabulary.Location.Name)
                    .getValue());
            item.setValue(group.getProperty(OSSEVocabulary.Location.Name)
                    .getValue());
            locationsSelectItems.add(item);
        }

        return locationsSelectItems;
    }

    /**
     * Prepares data in the session used by the MDRFaces components.
     *
     * @return true: all ok, false: could not get an accessToken
     * @throws InvalidKeyException
     *             the invalid key exception
     * @throws NoSuchAlgorithmException
     *             the no such algorithm exception
     * @throws SignatureException
     *             the signature exception
     * @throws JWTException
     *             the JWT exception
     */
    public static Boolean mdrFacesClientStuff() throws InvalidKeyException,
            NoSuchAlgorithmException, SignatureException, JWTException {
        HttpConnector43 hc = null;
        try {
            hc = new HttpConnector43(Utils.getAB().getConfig());
        } catch (HttpConnectorException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        LoggedUser lu = new LoggedUser();

        if (hc == null) {
            Utils.setSessionValue(JsfUtils.SESSION_USER, lu);
            return false;
        }

        Client client = hc.getJerseyClientForHTTPS(false);
        AccessTokenDTO accessToken;
        accessToken = Auth.getAccessToken(client, Utils.getAB().getConfig());
        if (accessToken != null) {
            lu.setAccessToken(accessToken.getAccessToken());
            Utils.setSessionValue(JsfUtils.SESSION_USER, lu);
            client.destroy();
            hc.closeClients();
            return true;
        } else {
            Utils.setSessionValue(JsfUtils.SESSION_USER, lu);
            client.destroy();
            hc.closeClients();
            return false;
        }
    }

    /**
     * Gets the URL to the Mainzelliste REST interface.
     *
     * @return the mainzelliste resturl
     */
    public static String getMainzellisteRESTURL() {
        // If a mainzelliste is configured, we use that URL
        String mainzellisteRESTURL = Utils.getAB().getConfig()
                .getString(Vocabulary.Config.Mainzelliste.REST);

        if (mainzellisteRESTURL == null || "".equals(mainzellisteRESTURL)) {
            // else we grab what the user has typed into his browser to
            // access the registry and use that as URL-base
            String currentHostURL = Utils.generateUrlToWebservice();
            mainzellisteRESTURL = currentHostURL + "/mainzelliste";
        }

        return mainzellisteRESTURL;
    }

    /**
     * Gets the URL to the Mainzelliste REST interface via localhost-HTTP
     * connection If no REST URL for the Mainzelliste is set, then the
     * mainzelliste runs on the same machine. In that case, the connection from
     * registry to mainzelliste can be done by a simple HTTP request to
     * localhost, if the port to it is set
     *
     * @return the mainzelliste internal resturl
     */
    public static String getMainzellisteInternalRESTURL() {
        // If a mainzelliste is configured, we use that URL
        String mainzellisteRESTURL = Utils.getAB().getConfig()
                .getString(Vocabulary.Config.Mainzelliste.REST);

        if (mainzellisteRESTURL == null || "".equals(mainzellisteRESTURL)) {
            String RESTLocalhostToMainzellistePort = Utils.getAB().getConfig()
                    .getString(Vocabulary.Config.Mainzelliste.RESTInternalMainzellistePort);

            if (RESTLocalhostToMainzellistePort != null && !"".equals(RESTLocalhostToMainzellistePort)) {
                String currentHostURL = "http://localhost:" + RESTLocalhostToMainzellistePort;
                mainzellisteRESTURL = currentHostURL + "/mainzelliste";
            } else {
                return getMainzellisteRESTURL();
            }
        }

        return mainzellisteRESTURL;
    }

    /**
     * Gets the URL to the teiler REST interface.
     *
     * @return the teiler resturl
     */
    public static String getTeilerRESTURL() {
        // If a mainzelliste is configured, we use that URL
        String teilerRESTURL = Utils.getAB().getConfig()
                .getString(Vocabulary.Config.Teiler.REST);

        if (teilerRESTURL == null || "".equals(teilerRESTURL)) {
            // else we grab what the user has typed into his browser to
            // access the registry and use that as URL-base
            String currentHostURL = Utils.generateUrlToWebservice();
            teilerRESTURL = currentHostURL + "/osse-share";
        }

        return teilerRESTURL;
    }

}
