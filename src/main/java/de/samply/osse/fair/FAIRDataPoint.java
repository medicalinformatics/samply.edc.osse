package de.samply.osse.fair;
import com.hp.hpl.jena.vocabulary.DCTerms;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.RDF;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.vocabulary.RDFS;
import de.samply.common.mdrclient.domain.ResultList;
import de.samply.store.JSONResource;
import de.samply.store.exceptions.DatabaseException;
import java.io.StringWriter;


/**
 * Metadata Service to get Information of the FAIR-Data Point
 * The FAIR Data Point has 5 levels:
 * 1. FDP Data Repository: Information about the FDP as a data repository
 * 2. Catalog: Information about the catalog of datasets offered
 * 3. Dataset: Information about each of the offered datasets e.g. publisher, theme
 * 4. Distribution: Information about how the dataset is distributed e.g acessURL, format, mediatype
 * 5. Data Record: Information about the actual data, types, identifiers etc.
 */


@Path("")
@Produces(MediaType.TEXT_XML)
public class FAIRDataPoint {

    @Produces(MediaType.TEXT_XML)
    @GET
    @Path("/FDPMetaData")


/**
 * Returns FDPMetaData of the Fair Data Point.
 */
    public String getFDPMetaData() throws Exception {


        //Stringwriter to write RDF Document
        StringWriter writer = new StringWriter();
        FDPHelper helper = new FDPHelper();

        JSONResource osseConfig = helper.getOSSEConfig();

        FDPMetaData fdpMetaData = null;
        fdpMetaData = helper.getFDPMetaDataFromConfig(osseConfig);

        if(helper.checkFDPActivation(osseConfig) == false){ //Check if FDP is deactivated by the user
            return "FAIR Data Point is currently deactivated. Please contact the Registry Operator";
        }

        String ns = fdpMetaData.getRegistryURL() + "/fdp/";

        //Create a RDF Model
        Model model = ModelFactory.createDefaultModel();

        //set prefix for supported ontologies we need to use for the FDP
        model.setNsPrefix("dcat", "http://www.w3.org/ns/dcat#");
        model.setNsPrefix("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        model.setNsPrefix("dct", "http://purl.org/dc/terms/");
        model.setNsPrefix("r3d", "http://www.re3data.org/schema/3-0#");
        model.setNsPrefix("fdp-o", "http://rdf.biosemantics.org/ontologies/fdp-o#");
        model.setNsPrefix("lang", "http://id.loc.gov/vocabulary/iso639-1/");

        final Resource firstDescription = model.createResource(ns);
        //get the data from the FDPMetaData Model
        //rdf rdftype
        firstDescription.addProperty(RDF.type, fdpMetaData.getRDFType());
        //DcTerms title
        firstDescription.addProperty(DCTerms.title, "FDP of " + fdpMetaData.getTitle());
        //DCTerms identifier
        firstDescription.addProperty(DCTerms.identifier, fdpMetaData.getIdentifier());
        //DCTerms hasVersion
        firstDescription.addProperty(DCTerms.hasVersion, fdpMetaData.getVersion());
        //DCTerms description
        firstDescription.addProperty(DCTerms.description, fdpMetaData.getDescription());
        //DCTerms publisher
        firstDescription.addProperty(DCTerms.publisher, fdpMetaData.getPublisher());
        //DCTerms language
        firstDescription.addProperty(DCTerms.language, fdpMetaData.getLanguage());
        //RDFs label
        firstDescription.addProperty(RDFS.label, "FDP of: " + fdpMetaData.getTitle());
        //DCTerms issued
        firstDescription.addProperty(DCTerms.issued, fdpMetaData.getIssued());
        //DCTerms modified
        firstDescription.addProperty(DCTerms.modified, fdpMetaData.getModified());

        //need to create properties manually which are not directly supported by Apache Jena
        String catalogString = "http://www.re3data.org/schema/3-0#";
        Property relatedCatalog = model.createProperty(catalogString, "catalog"); //r3d cata,
        firstDescription.addProperty(relatedCatalog, fdpMetaData.getRegistryURL() + "/fdp/catalog/" + fdpMetaData.getIdentifier()); //catalog

        //write the model via the Stringwriter in RDF/XML Output. Other possibilites
        model.write(writer, "RDF/XML-ABBREV");


        return writer.toString();
    }

    /***
     * Retrieves Information about the catalog of the registry
     * @param id
     * @return
     * @throws Exception
     */

    @Produces(MediaType.APPLICATION_XML)
    @GET
    @Path("/catalog/{id}")
    public String getCatalog(@PathParam("id") String id) throws DatabaseException {


        FDPHelper helper = new FDPHelper();
        JSONResource osseConfig = helper.getOSSEConfig();
        CatalogMetaData catalogMetaData = null;
        catalogMetaData = helper.getCatalogMetaDataFromConfig(osseConfig);

        if(helper.checkFDPActivation(osseConfig) == false){
            return "FAIR Data Point is currently deactivated. Please contact the Registry Operator";
        }

        FDPMetaData fdpMetaData = null;
        fdpMetaData = helper.getFDPMetaDataFromConfig(osseConfig);
        StringWriter writer = new StringWriter();


        if (fdpMetaData.getIdentifier().equals(id)) {

            String ns = fdpMetaData.getRegistryURL() + "/fdp/catalog/" + id;
            Model model = ModelFactory.createDefaultModel();

            final Resource firstDescription = model.createResource(ns); //First Description
            model.setNsPrefix("dcat", "http://www.w3.org/ns/dcat#");
            model.setNsPrefix("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
            model.setNsPrefix("dct", "http://purl.org/dc/terms/");
            model.setNsPrefix("r3d", "http://www.re3data.org/schema/3-0#");
            model.setNsPrefix("fdp-o", "http://rdf.biosemantics.org/ontologies/fdp-o#");
            model.setNsPrefix("lang", "http://id.loc.gov/vocabulary/iso639-1/");
            model.setNsPrefix("foaf", "http://xmlns.com/foaf/0.1/");

            //dcterms title
            firstDescription.addProperty(DCTerms.title, "Catalog of " + catalogMetaData.getTitle());
            //dcterms identifier
            firstDescription.addProperty(DCTerms.identifier, "catalog-" + fdpMetaData.getIdentifier());
            //dctterms hasVersion
            firstDescription.addProperty(DCTerms.hasVersion, catalogMetaData.getVersion());
            //dcterms description
            firstDescription.addProperty(DCTerms.description, catalogMetaData.getDescription());
            //dcterms publisher
            firstDescription.addProperty(DCTerms.publisher, fdpMetaData.getPublisher());
            //dcterms language
            firstDescription.addProperty(DCTerms.language, fdpMetaData.getLanguage());
            //dcterms license
            firstDescription.addProperty(DCTerms.license, catalogMetaData.getLicense());
            //rdfs label
            firstDescription.addProperty(RDFS.label, catalogMetaData.getTitle() + " catalog"); //rdfs label
            //dcterms issued
            firstDescription.addProperty(DCTerms.issued, catalogMetaData.getIssued());
            //dcterms modified
            firstDescription.addProperty(DCTerms.modified, catalogMetaData.getModified());

            //foaf:homepage
            String homepageString = "http://xmlns.com/foaf/0.1/"; //"http://www.w3.org/ns/dcat#distribution";
            Property relatedHomepage = model.createProperty(homepageString, "homepage"); //r3d cata,
            firstDescription.addProperty(relatedHomepage, fdpMetaData.getWebsite()); //catalog


            //dcat dataset
            String dataSetString = "http://www.w3.org/ns/dcat#";
            Property relatedDataSet = model.createProperty(dataSetString, "dataset");
            firstDescription.addProperty(relatedDataSet, fdpMetaData.getRegistryURL() + "/fdp"  + "/dataset/" + fdpMetaData.getIdentifier()); //catalog

            //dcat:themeTaxonomy

            String themeTaxonmyString = "http://www.w3.org/ns/dcat#";
            Property relatedThemeTaxonmyString = model.createProperty(themeTaxonmyString, "themeTaxonmy");
            firstDescription.addProperty(relatedThemeTaxonmyString, catalogMetaData.getThemeTaxonomy()); //catalog

            model.write(writer, "RDF/XML-ABBREV");

        }
        return writer.toString();

    }

    @Produces(MediaType.APPLICATION_XML)
    @GET
    @Path("/dataset/{id}")

    /***
     * Retrieves information about the dataset of the registry
     */

    public String getDataSet(@PathParam("id") String id) throws Exception {

        FDPHelper fdpHelper = new FDPHelper();
        FDPMetaData fdpMetaData = null;
        JSONResource osseConfig = fdpHelper.getOSSEConfig();
        fdpMetaData = fdpHelper.getFDPMetaDataFromConfig(osseConfig);
        if(fdpHelper.checkFDPActivation(osseConfig) == false){
            return "FAIR Data Point is currently deactivated. Please contact the Registry Operator";
        }

        StringWriter writer = new StringWriter();

        DatasetMetadata datasetMetadata = null;
        datasetMetadata = fdpHelper.getDatasetMetaDataFromConfig(osseConfig);

        if (fdpMetaData.getIdentifier().equals(id)) {


            String ns = fdpMetaData.getRegistryURL() + "/fdp/dataset/" + id;
            Model model = ModelFactory.createDefaultModel();


            final Resource firstDescription = model.createResource(ns); //First Description
            model.setNsPrefix("dcat", "http://www.w3.org/ns/dcat#");
            model.setNsPrefix("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
            model.setNsPrefix("dct", "http://purl.org/dc/terms/");
            model.setNsPrefix("r3d", "http://www.re3data.org/schema/3-0#");
            model.setNsPrefix("fdp-o", "http://rdf.biosemantics.org/ontologies/fdp-o#");
            model.setNsPrefix("lang", "http://id.loc.gov/vocabulary/iso639-1/");
            model.setNsPrefix("foaf", "http://xmlns.com/foaf/0.1/");

            //dcterms: title
            firstDescription.addProperty(DCTerms.title, "Dataset of " + datasetMetadata.getTitle());
            //dcterms: identifier
            firstDescription.addProperty(DCTerms.identifier, "dataset-" + fdpMetaData.getIdentifier());
            //dcterms: hasVersion
            firstDescription.addProperty(DCTerms.hasVersion, datasetMetadata.getVersion());
            //dcterms: description
            firstDescription.addProperty(DCTerms.description, datasetMetadata.getDescription());
            //dcterms: publisher
            firstDescription.addProperty(DCTerms.publisher, fdpMetaData.getPublisher());
            //dcterms: language
            firstDescription.addProperty(DCTerms.language, fdpMetaData.getLanguage());
            //rdfs: label
            firstDescription.addProperty(RDFS.label, datasetMetadata.getTitle() + " dataset");

            firstDescription.addProperty(DCTerms.issued, datasetMetadata.getIssued());
            firstDescription.addProperty(DCTerms.modified, datasetMetadata.getModified());
            //dcat: contactPoint
            String ContactPoint = "http://www.w3.org/ns/dcat#";
            Property relatedContactPoint = model.createProperty(ContactPoint, "contactPoint");
            firstDescription.addProperty(relatedContactPoint, datasetMetadata.getContactPoint()); //catalog

            //dcat: keyword
            String keyWord = "http://www.w3.org/ns/dcat#";
            Property relatedKeyWord = model.createProperty(keyWord, "keyword");
            firstDescription.addProperty(relatedKeyWord, datasetMetadata.getKeyword()); //catalog

            //dcat: distribution
            String distribution = "http://www.w3.org/ns/dcat#";
            Property relatedDistribution = model.createProperty(distribution, "distribution");
            firstDescription.addProperty(relatedDistribution, fdpMetaData.getRegistryURL() + "/fdp/" + "distribution/"+ fdpMetaData.getIdentifier()); //catalog

            //dcat: theme
            String theme = "http://www.w3.org/ns/dcat#";
            Property relatedTheme = model.createProperty(theme, "theme");
            firstDescription.addProperty(relatedTheme, datasetMetadata.getTheme()); //catalog

            model.write(writer, "RDF/XML-ABBREV");

        }

        return writer.toString();


    }

    @Produces(MediaType.APPLICATION_XML)
    @GET
    @Path("/distribution/{id}")

    /***
     * Retrieves information about the distribution of the registry
     */

    public String getDistribution(@PathParam("id") String id) throws Exception {

        FDPHelper fdpHelper = new FDPHelper();
        FDPMetaData fdpMetaData = null;
        JSONResource osseConfig = fdpHelper.getOSSEConfig();
        fdpMetaData = fdpHelper.getFDPMetaDataFromConfig(osseConfig);

        StringWriter writer = new StringWriter();

        if(fdpHelper.checkFDPActivation(osseConfig) == false){
            return "FAIR Data Point is currently deactivated. Please contact the Registry Operator";
        }

        DistributionMetadata distributionMetadata = null;
        distributionMetadata = fdpHelper.getDistributionMetaDataFromConfig(osseConfig);


        String ns = fdpMetaData.getRegistryURL() + "/fdp/distribution/" + id;
        Model model = ModelFactory.createDefaultModel();

        if (fdpMetaData.getIdentifier().equals(id)) {

            final Resource firstDescription = model.createResource(ns); //First Description
            model.setNsPrefix("dcat", "http://www.w3.org/ns/dcat#");
            model.setNsPrefix("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
            model.setNsPrefix("dct", "http://purl.org/dc/terms/");
            model.setNsPrefix("r3d", "http://www.re3data.org/schema/3-0#");
            model.setNsPrefix("fdp-o", "http://rdf.biosemantics.org/ontologies/fdp-o#");
            model.setNsPrefix("lang", "http://id.loc.gov/vocabulary/iso639-1/");
            model.setNsPrefix("foaf", "http://xmlns.com/foaf/0.1/");

            //dcterms: title

            firstDescription.addProperty(DCTerms.title, "Distribution of " + distributionMetadata.getTitle());
            //dctterms:identifier
            firstDescription.addProperty(DCTerms.identifier, "distribution-" + fdpMetaData.getIdentifier());
            //dcterms: hasVersion
            firstDescription.addProperty(DCTerms.hasVersion, distributionMetadata.getVersion());
            //dcterms: description
            firstDescription.addProperty(DCTerms.description, distributionMetadata.getDescription());
            //dctterms: publisher
            firstDescription.addProperty(DCTerms.publisher, fdpMetaData.getPublisher());
            //dcterms: language
            firstDescription.addProperty(DCTerms.language, fdpMetaData.getLanguage());
            //rdfs: label
            firstDescription.addProperty(RDFS.label, distributionMetadata.getTitle() + " distribution");
            firstDescription.addProperty(DCTerms.issued, distributionMetadata.getIssued());
            firstDescription.addProperty(DCTerms.modified, distributionMetadata.getModified());

            //dcat:mediaType
            String mediaType = "http://www.w3.org/ns/dcat#";
            Property relatedMediaType = model.createProperty(mediaType, "mediaType");
            firstDescription.addProperty(relatedMediaType, "text/json"); //catalog

            //dcat:mediaType
            String license = "http://www.w3.org/ns/dcat#";
            Property relatedLicense = model.createProperty(mediaType, "license");
            firstDescription.addProperty(relatedLicense, distributionMetadata.getLicense()); //catalog

            //dcat: accessUrl
            String accessUrl = "http://www.w3.org/ns/dcat#";
            Property relatedaccessUrl = model.createProperty(accessUrl, "accessURL");
            firstDescription.addProperty(relatedaccessUrl, fdpMetaData.getRegistryURL()+"/fdp" + "/access/"+fdpMetaData.getIdentifier()); //catalog

            model.write(writer, "RDF/XML-ABBREV");


        }

        return writer.toString();

    }

    @Produces(MediaType.APPLICATION_JSON)
    @GET
    @Path("/access/{id}")

    /***
     * Access the data of the MDR
     */

    public ResultList getDataRecords(@PathParam("id") String id) throws Exception {

        ResultList resultList = null;
        FDPHelper fdpHelper = new FDPHelper();
        FDPMetaData fdpMetaData = null;
        JSONResource osseConfig = fdpHelper.getOSSEConfig();
        fdpMetaData = fdpHelper.getFDPMetaDataFromConfig(osseConfig);

        if (fdpMetaData.getIdentifier().equals(id)) {

            //use MDRHelper to get ResultList of the Data elements
            MDRHelper mdrHelper = new MDRHelper();
            resultList = mdrHelper.getResultListMDR(osseConfig);

        }

        return resultList;

    }


}
