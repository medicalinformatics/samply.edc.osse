package de.samply.osse.fair;

/**
 * The Dataset Metadata can have a number of Distribution Metadata. The
 * Dataset Metadata Retrieval function lead to the distribution Metadata Retrieval function by containing the URIs of
 * the Distribution Metadata in the Dataset Metadata content. Also, the Dataset Metadata can have a Data Record Metadata. The Dataset Metadata
 * retrieval function can lead to the Data Record Metadata Retrieval function by appending the URIs of the Datarecord Metadata in the
 * Dataset Metadata content
 */
public class DatasetMetadata {

    private String title; //title of the dataset
    private String identifier; //
    private String version;
    private String description;
    private String language = "http://id.loc.gov/vocabulary/iso639-1/en";
    private String RDFLabel;
    private String contactPoint;
    private String keyword;
    private String distribution;
    private String modified;
    private String issued;

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getIssued() {
        return issued;
    }

    public void setIssued(String issued) {
        this.issued = issued;
    }


    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    private String theme;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getRDFLabel() {
        return RDFLabel;
    }

    public void setRDFLabel(String RDFLabel) {
        this.RDFLabel = RDFLabel;
    }

    public String getContactPoint() {
        return contactPoint;
    }

    public void setContactPoint(String contactPoint) {
        this.contactPoint = contactPoint;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getDistribution() {
        return distribution;
    }

    public void setDistribution(String distribution) {
        this.distribution = distribution;
    }
}
