package de.samply.osse.fair;

import com.sun.jersey.api.client.Client;
import de.samply.auth.rest.AccessTokenDTO;
import de.samply.common.http.HttpConnector43;
import de.samply.common.http.HttpConnectorException;
import de.samply.common.mdrclient.MdrClient;
import de.samply.common.mdrclient.domain.Result;
import de.samply.common.mdrclient.domain.ResultList;
import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.osse.auth.Auth;
import de.samply.edc.osse.model.MdrKey;
import de.samply.edc.utils.Utils;
import de.samply.store.JSONResource;
import org.apache.commons.configuration.Configuration;

import java.util.List;

/**
 * Helper Class for FAIR-Data point to get directly informations from the MDR
 */
public class MDRHelper {


    /***
     * //Returns the Datalements from the MDR
     *
     * @param osseConfig
     * @return
     * @throws Exception
     */
    public ResultList getResultListMDR(JSONResource osseConfig) throws Exception {
        Configuration config = Utils.getConfig();
        ResultList resultList = new ResultList();

        //set Proxy settings
        String[] proxySettings = {Vocabulary.Config.Standard.proxyHTTPHost,
                Vocabulary.Config.Standard.proxyHTTPSHost,
                Vocabulary.Config.Standard.proxyHTTPPort,
                Vocabulary.Config.Standard.proxyHTTPSPort,
                Vocabulary.Config.Standard.proxyHTTPUsername,
                Vocabulary.Config.Standard.proxyHTTPPassword,
                Vocabulary.Config.Standard.proxyHTTPSUsername,
                Vocabulary.Config.Standard.proxyHTTPSPassword,
                Vocabulary.Config.Standard.proxyRealm};

        // inject proxy settings, so they dont get overloaded in the next step
        for (String pSet : proxySettings) {
            osseConfig.setProperty(pSet, config.getString(pSet));
        }

        // inject OSSEConfig into Config, so both ways of configuration are
        // returning the same values
        for (String key : osseConfig.getDefinedProperties()) {
            config.setProperty(key, osseConfig.getProperty(key).getValue());
        }

        //get AuthUserID from Config
        String authUserId = osseConfig.getProperty(Vocabulary.Config.Auth.UserId).getValue();
        AccessTokenDTO accessToken;
        HttpConnector43 httpConnector = null;
        try {
            httpConnector = new HttpConnector43(config);
        } catch (HttpConnectorException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (httpConnector != null) {
            Client client = httpConnector.getJerseyClientForHTTPS(false);
            accessToken = Auth.getAccessToken(client, config);

            Client mdrJerseyClient = httpConnector.getJerseyClient(osseConfig.getProperty(
                    Vocabulary.Config.MDR.REST).getValue(), false);

            // Init MdrFacesClient
            MdrClient mdrClient = new MdrClient(osseConfig.getProperty(
                    Vocabulary.Config.MDR.REST).getValue(), mdrJerseyClient);

            //Create mdr key to Init MDR-Client

            MdrKey key = new MdrKey();
            key.initMdrClient(mdrClient, accessToken, authUserId);

            //get all data elements
            resultList = key.getMdrClient().getUserRootElements("en", accessToken.getAccessToken(), authUserId);
        } else {
            Utils.getLogger().error("Couldn't establish http connection.");
        }

        return resultList;

    }

    /***
     * Returns the namespace of the registry off the MDR
     * @param osseConfig
     * @return namespace
     */

    public String getNameSpace(JSONResource osseConfig) {

        String namespace = "";
        try {
            ResultList resultList = getResultListMDR(osseConfig);
            List<Result> results = resultList.getResults();
            String id = results.get(0).getId();
            MdrKey mdrKey = new MdrKey(id);
            namespace = mdrKey.getNamespace();

        } catch (Exception e) {

        }
        return namespace;

    }


}
