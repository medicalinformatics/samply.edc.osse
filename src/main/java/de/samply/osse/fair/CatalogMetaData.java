package de.samply.osse.fair;

/**
 * The catalog is composed by a number of Dataset Metadata. The Catalog Metadata Retrieval function
 * lead to the Dataset Metadata function by containing the URIs of the Dataset Metadata in the Catalog
 * Metadata content
 */
public class CatalogMetaData {

    private String title; // The name of the Catalog
    private String identifier; //A unambigous and persistent identifier to the catalog
    private String version; //version
    private String description; //short description
    private String language = "http://id.loc.gov/vocabulary/iso639-1/en";
    private String license = "http://purl.org/NET/rdflicense/MIT1.0";
    private String RDFLabel; //
    private String website; //website of your registry
    private String dataset; //URI of the dataset
    private String themeTaxonomy = "http://dbpedia.org/resource/Disease_registry";
    private String issued; //date of issue
    private String modified; //date of modify

    public String getIssued() {
        return issued;
    }
    public void setIssued(String issued) {
        this.issued = issued;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getRDFLabel() {
        return RDFLabel;
    }

    public void setRDFLabel(String RDFLabel) {
        this.RDFLabel = RDFLabel;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getDataset() {
        return dataset;
    }

    public void setDataset(String dataset) {
        this.dataset = dataset;
    }

    public String getThemeTaxonomy() {
        return themeTaxonomy;
    }

    public void setThemeTaxonomy(String themeTaxonomy) {
        this.themeTaxonomy = themeTaxonomy;
    }


}
