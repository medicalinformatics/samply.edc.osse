package de.samply.osse.fair;

/**
 * Retrieves the FDP Metadata. The FDP Metadta is composed by a number of Catalog metadata. FDP Metadata Retrieval
 * function lead to the Catalog Metadata Retrieval Function by appending the URIs of the Catalog Metadata at the end of the FDP Metadata
 * content
 */
public class FDPMetaData {

    private String RDFType = "http://www.w3.org/ns/ldp#Container";
    private String title;
    private String identifier;
    private String description;
    private String publisher;
    private String language;
    private String catalog;
    private String version;
    private String registryURL;
    private String website;
    private String modified;
    private String issued;


    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getIssued() {
        return issued;
    }

    public void setIssued(String issued) {
        this.issued = issued;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }


    public String getRegistryURL() {
        return registryURL;
    }

    public void setRegistryURL(String registryURL) {
        this.registryURL = registryURL;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getRDFType() {
        return RDFType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCatalog() {
        return catalog;
    }

    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }
}
